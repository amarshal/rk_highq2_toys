import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import scipy.stats as stats
import math
import __main__


def plot_x_y_yerr(data, weights=None, bins=50, x_range=None, color='k', density=False, label=None,fmt=' ', marker='s',capsize=3,linewidth=0.75, markersize=4, alpha=1.):
	if weights is None: 
		weights = np.ones(np.shape(data))
	hist_i = np.histogram(data, bins=bins, range=x_range, weights=weights, density=density)
	x_points = hist_i[1][:-1] + (hist_i[1][1]-hist_i[1][0])/2.
	y_points = hist_i[0]
	yerr_points = np.sqrt(np.histogram(data, bins=bins, range=[np.amin(hist_i[1]),np.amax(hist_i[1])], weights=weights*weights)[0])

	if density == True:
		scale = np.sum(hist_i[0])/np.sum(np.histogram(data, bins=bins, range=x_range, weights=weights, density=False)[0])
		yerr_points = yerr_points*scale

	plt.errorbar(x_points, y_points, yerr=yerr_points,color=color,marker=marker,fmt=fmt,capsize=capsize,linewidth=linewidth, markersize=markersize, label=label, alpha=alpha)

	return x_points, y_points, yerr_points, [np.amin(hist_i[1]),np.amax(hist_i[1])]



def pulls(config, list_of_parameters, dict_of_results, extra_string='', onlyConvergedFits=False):

	plot_dims = np.ceil(np.sqrt(len(list_of_parameters[config])+2))

	plt.figure(figsize=(plot_dims*4., plot_dims*4.))

	pull_plot_errors = {}

	converged = dict_of_results[config]["converged"]
	valid = dict_of_results[config]["valid"]

	if onlyConvergedFits:
		where = np.where((converged==True)&(valid==True))
	else:
		where = np.where((converged==True)|(converged==False))


	for plot_idx, param in enumerate(list_of_parameters[config]+['RK_minos']+['mean_pull_plot']):

		try:
			if param == 'RK_minos':

				fit = dict_of_results[config]["RK"][where]
				fit_err_lower = dict_of_results[config]['RK_lower'][where]
				fit_err_upper = dict_of_results[config]['RK_upper'][where]

				fit_pull = np.empty(np.shape(fit))

				for i in range(0, np.shape(fit)[0]):

					if fit[i] > 1.:
						fit_pull[i] = (fit[i]-__main__.RK)/(-1.*fit_err_lower[i])
					else:
						fit_pull[i] = (fit[i]-__main__.RK)/fit_err_upper[i]
				
				list_of_parameters[config].append("RK_minos")
			else:
				fit = dict_of_results[config][param][where]
				fit_err = dict_of_results[config][param+'_err'][where]
				fit_pull = dict_of_results[config][param+'_pull'][where]

			ax = plt.subplot(int(plot_dims),int(plot_dims),plot_idx+1)

			mu = 0
			variance = 1
			sigma = math.sqrt(variance)
			x = np.linspace(mu - 6*sigma, mu + 6*sigma, 50)
			plt.plot(x, stats.norm.pdf(x, mu, sigma), c='tab:red',linewidth=2)

			plt.hist(fit_pull, bins=25, histtype='step', density=True, color='tab:blue',alpha=0.5)
			plot_x_y_yerr(fit_pull, bins=25, density=True, color='tab:blue')

			mean_pull = np.mean(fit_pull)
			pull_width = np.std(fit_pull)

			plt.axvline(x=mean_pull, c='k')
			plt.axvline(x=0., c='r')
			plt.ylim(ymin=0)
			plt.yticks([],[])

			plt.text(0.97, 0.97, '%s'%param, horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, fontsize=10)

			plt.text(0.97, 0.87, ' mean: %.2f'%mean_pull, horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, fontsize=15)
			plt.text(0.97, 0.78, 'width: %.2f'%pull_width, horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, fontsize=15)

			n = np.shape(fit_pull)[0]
			pull_plot_errors[param] = [mean_pull, pull_width/np.sqrt(n), pull_width, np.sqrt(pull_width/(2*n))]
		except:
			pass

		if param == 'mean_pull_plot':

			ax = plt.subplot(int(plot_dims),int(plot_dims),plot_idx+1)

			scatter = np.empty((0,4))
			y_labels = []
			n = len(list(pull_plot_errors.keys()))

			for key in list(pull_plot_errors.keys()):

				y_labels.append(key)

				scatter = np.append(scatter,[pull_plot_errors[key]], axis=0)

			plt.axvline(x=0, c='k')
			plt.axvline(x=1, c='k')
			ax.yaxis.tick_right()
			plt.errorbar(scatter[:,0],np.arange(0,n), xerr=scatter[:,1], c='r',fmt=' ', marker='s',capsize=2,linewidth=0.75, markersize=2, alpha=1.)
			plt.errorbar(scatter[:,2],np.arange(0,n), xerr=scatter[:,3], c='b',fmt=' ', marker='s',capsize=2,linewidth=0.75, markersize=2, alpha=1.)
			plt.xlim(-0.3,1.3)
			plt.yticks(np.arange(0,n), y_labels)


	plt.savefig('%s/pulls_%s%s.pdf'%(__main__.plotting_directory,config,extra_string),bbox_inches='tight')
	plt.close('all')

	plt.figure(figsize=(10., 5.))
	for plot_idx, param in enumerate(['RK','mean_pull_plot']):

		try:
			if param == 'RK_minos':

				fit = dict_of_results[config]["RK"][where]
				fit_err_lower = dict_of_results[config]['RK_lower'][where]
				fit_err_upper = dict_of_results[config]['RK_upper'][where]

				fit_pull = np.empty(np.shape(fit))

				for i in range(0, np.shape(fit)[0]):

					if fit[i] > 1.:
						fit_pull[i] = (fit[i]-__main__.RK)/(-1.*fit_err_lower[i])
					else:
						fit_pull[i] = (fit[i]-__main__.RK)/fit_err_upper[i]
				
				list_of_parameters[config].append("RK_minos")
			else:
				fit = dict_of_results[config][param][where]
				fit_err = dict_of_results[config][param+'_err'][where]
				fit_pull = dict_of_results[config][param+'_pull'][where]

			ax = plt.subplot(1,2,1)

			mu = 0
			variance = 1
			sigma = math.sqrt(variance)
			x = np.linspace(mu - 6*sigma, mu + 6*sigma, 50)
			plt.plot(x, stats.norm.pdf(x, mu, sigma), c='k',linewidth=2,alpha=0.5)


			fit = dict_of_results[config][param][where]
			fit_err = dict_of_results[config][param+'_err'][where]
			fit_pull = dict_of_results[config][param+'_pull'][where]

			plt.hist(fit_pull, bins=25, histtype='step', density=True, color='tab:blue',alpha=0.5)
			plot_x_y_yerr(fit_pull, bins=25, density=True, color='tab:blue')

			plt.text(0.97, 0.97, '%s (blue)'%param, horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, fontsize=10)

			mean_pull = np.mean(fit_pull)
			pull_width = np.std(fit_pull)

			plt.text(0.97, 0.87, 'mean: %.2f'%mean_pull, horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, fontsize=15)
			plt.text(0.97, 0.78, 'width: %.2f'%pull_width, horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, fontsize=15)

			plt.axvline(x=mean_pull, c='tab:blue')

			fit = dict_of_results[config]["RK"][where]
			fit_err_lower = dict_of_results[config]['RK_lower'][where]
			fit_err_upper = dict_of_results[config]['RK_upper'][where]

			fit_pull = np.empty(np.shape(fit))

			for i in range(0, np.shape(fit)[0]):

				if fit[i] > 1.:
					fit_pull[i] = (fit[i]-__main__.RK)/(-1.*fit_err_lower[i])
				else:
					fit_pull[i] = (fit[i]-__main__.RK)/fit_err_upper[i]

			plt.hist(fit_pull, bins=25, histtype='step', density=True, color='tab:orange',alpha=0.5)
			plot_x_y_yerr(fit_pull, bins=25, density=True, color='tab:orange',alpha=0.5)

			plt.text(0.97, 0.68, '%s (orange)'%'RK_minos', horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, fontsize=10)
			mean_pull = np.mean(fit_pull)
			pull_width = np.std(fit_pull)
			plt.text(0.97, 0.57, 'mean: %.2f'%mean_pull, horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, fontsize=15)
			plt.text(0.97, 0.48, 'width: %.2f'%pull_width, horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, fontsize=15)


			RMS = sensitivity(config, list_of_parameters, dict_of_results, extra_string=extra_string, onlyConvergedFits=True, RK=__main__.RK)

			n = np.shape(fit_pull)[0]
			RMS_err = np.sqrt(RMS/(2*n))

			plt.text(0.02, 0.97, 'RMS: %.4f+-%.5f'%(RMS,RMS_err), horizontalalignment='left',verticalalignment='top', transform=ax.transAxes, fontsize=17)

			plt.axvline(x=mean_pull, c='tab:orange')
			plt.axvline(x=0., c='k',alpha=0.5)
			plt.ylim(ymin=0)
			plt.yticks([],[])



			
		except:
			pass

		if param == 'mean_pull_plot':

			ax = plt.subplot(1,2,2)

			scatter = np.empty((0,4))
			y_labels = []
			n = len(list(pull_plot_errors.keys()))

			for key in list(pull_plot_errors.keys()):

				y_labels.append(key)

				scatter = np.append(scatter,[pull_plot_errors[key]], axis=0)

			plt.axvline(x=0, c='k')
			plt.axvline(x=1, c='k')
			ax.yaxis.tick_right()
			plt.errorbar(scatter[:,0],np.arange(0,n), xerr=scatter[:,1], c='r',fmt=' ', marker='s',capsize=2,linewidth=0.75, markersize=2, alpha=1.)
			plt.errorbar(scatter[:,2],np.arange(0,n), xerr=scatter[:,3], c='b',fmt=' ', marker='s',capsize=2,linewidth=0.75, markersize=2, alpha=1.)
			plt.xlim(-0.3,1.3)
			plt.yticks(np.arange(0,n), y_labels)


	plt.savefig('%s/pullsRK_%s%s.pdf'%(__main__.plotting_directory,config,extra_string),bbox_inches='tight')
	plt.close('all')




def values(config, list_of_parameters, dict_of_results, extra_string='', onlyConvergedFits=False):

	plot_dims = np.ceil(np.sqrt(len(list_of_parameters[config])+2))

	plt.figure(figsize=(plot_dims*4., plot_dims*4.))

	pull_plot_errors = {}

	converged = dict_of_results[config]["converged"]
	valid = dict_of_results[config]["valid"]

	if onlyConvergedFits:
		where = np.where((converged==True)&(valid==True))
	else:
		where = np.where((converged==True)|(converged==False))

	plot_dims = np.ceil(np.sqrt(len(list_of_parameters[config])))

	plt.figure(figsize=(plot_dims*4., plot_dims*4.))

	for plot_idx, param in enumerate(list_of_parameters[config]):
		
		try:

			fit = dict_of_results[config][param][where]
			fit_err = dict_of_results[config][param+'_err'][where]
			fit_pull = dict_of_results[config][param+'_pull'][where]


			ax = plt.subplot(int(plot_dims),int(plot_dims),plot_idx+1)


			mu = np.mean(fit)
			sigma = np.std(fit)
			x = np.linspace(mu - 6*sigma, mu + 6*sigma, 50)
			plt.plot(x, stats.norm.pdf(x, mu, sigma), c='tab:red',linewidth=2)


			plt.hist(fit, bins=25, histtype='step', density=True, color='tab:blue',alpha=0.5)
			plot_x_y_yerr(fit, bins=25, density=True, color='tab:blue')

			mean_pull = np.mean(fit)
			pull_width = np.std(fit)

			plt.axvline(x=mean_pull, c='k')
			plt.ylim(ymin=0)
			plt.yticks([],[])

			true_value = fit[0] - fit_pull[0]*fit_err[0]

			plt.text(0.97, 0.97, '%s'%param, horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, fontsize=10)

			plt.text(0.97, 0.87, ' mean: %.4f'%mean_pull, horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, fontsize=15)
			plt.text(0.97, 0.77, ' true: %.4f'%true_value, horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, fontsize=15,color='r')
			plt.axvline(x=true_value, c='r')

		except:
			pass

	plt.savefig('%s/values_%s%s.pdf'%(__main__.plotting_directory,config,extra_string),bbox_inches='tight')
	plt.close('all')



def convergence_success(config, list_of_parameters, dict_of_results, extra_string='', plot=False):

	if plot:
		plt.figure(figsize=(4., 4.))

		converged = dict_of_results[config]["converged"]
		valid = dict_of_results[config]["valid"]

		convergence_results = np.asarray([[0.,np.shape(converged)[0]],[1.,np.shape(np.where((converged==True)))[1]],[2.,np.shape(np.where((converged==True)&(valid==True)))[1]]])

		labels = ['All','Converged','Converged+Valid']

		plt.figure(figsize=(4,4))
		ax = plt.subplot(1,1,1)

		for i in range(0, np.shape(convergence_results)[0]):
			plt.axhline(y=convergence_results[i][1],c='tab:red')

		plt.scatter(convergence_results[:,0], convergence_results[:,1], marker='x', color='k')

		plt.xticks(np.linspace(0,len(labels)-1,len(labels)), labels, rotation=90, fontsize=10)

		ax.tick_params(axis=u'both', which=u'both',length=0)

		frac = convergence_results[2][1]/convergence_results[0][1]

		plt.ylim(ymin=0)

		plt.text(0.95, 0.05, 'frac of toys converged+valid: %.3f'%frac, horizontalalignment='right',verticalalignment='bottom', transform=ax.transAxes)
		plt.savefig('%s/convergence_success_%s%s.pdf'%(__main__.plotting_directory,config,extra_string),bbox_inches='tight')
		plt.close('all')

	else:
		converged = dict_of_results[config]["converged"]
		valid = dict_of_results[config]["valid"]

		convergence_results = np.asarray([[0.,np.shape(converged)[0]],[1.,np.shape(np.where((converged==True)))[1]],[2.,np.shape(np.where((converged==True)&(valid==True)))[1]]])
		frac = convergence_results[2][1]/convergence_results[0][1]
	return frac



def mean_correlations(config, list_of_parameters, dict_of_results, extra_string='', onlyConvergedFits=False):

	list_of_floating_params = dict_of_results[config]["list_of_floating_params"][0]
	Correlation_matrix = np.mean(dict_of_results[config]["Correlation_matrix"],axis=0)

	plt.figure(figsize=(10,8))
	plt.title("mean of %d toys"%np.shape(dict_of_results[config]["Correlation_matrix"])[0])
	ax = plt.subplot(1,1,1)
	plt.imshow(Correlation_matrix,vmin=-1., vmax=1.)
	plt.xticks(np.linspace(0,len(list_of_floating_params)-1,len(list_of_floating_params)), list_of_floating_params, rotation=90, fontsize=10)
	plt.yticks(np.linspace(0,len(list_of_floating_params)-1,len(list_of_floating_params)), list_of_floating_params, fontsize=10)
	ax.tick_params(axis=u'both', which=u'both',length=0)
	plt.colorbar()
	plt.savefig('%s/mean_correlations_%s%s.pdf'%(__main__.plotting_directory,config,extra_string),bbox_inches='tight')
	plt.close('all')



def sensitivity(config, list_of_parameters, dict_of_results, extra_string='', onlyConvergedFits=False, plot=False, RK=1.):

	converged = dict_of_results[config]["converged"]
	valid = dict_of_results[config]["valid"]

	if onlyConvergedFits:
		where = np.where((converged==True)&(valid==True))
	else:
		where = np.where((converged==True)|(converged==False))

	RK_values = dict_of_results[config]["RK"][where]
	RK_err_values = dict_of_results[config]["RK_err"][where]
	RK_err_LOWER_values = dict_of_results[config]["RK_lower"][where]
	RK_err_UPPER_values = dict_of_results[config]["RK_upper"][where]

	RK_residuals = np.abs(RK_values-RK*np.ones(np.shape(RK_values)))

	if plot:
		plt.figure(figsize=(12,8))

		ax = plt.subplot(2,3,1)
		# plt.hist(RK_values, bins=75, histtype='step')
		plot_x_y_yerr(RK_values, bins=25, density=True, color='tab:blue')
		plt.text(0.95, 0.95, 'RK values', horizontalalignment='right',verticalalignment='top', transform=ax.transAxes)
		plt.axvline(np.mean(RK_values),c='tab:red')
		plt.axvline(np.mean(RK_values)+np.std(RK_values)/np.sqrt(np.shape(RK_values)[0]),c='tab:red',linestyle='--',alpha=0.5)
		plt.axvline(np.mean(RK_values)-np.std(RK_values)/np.sqrt(np.shape(RK_values)[0]),c='tab:red',linestyle='--',alpha=0.5)
		plt.text(0.95, 0.90, '%.5f+-%.5f'%(np.mean(RK_values),np.std(RK_values)/np.sqrt(np.shape(RK_values)[0])), horizontalalignment='right',verticalalignment='top', transform=ax.transAxes)

		ax = plt.subplot(2,3,2)
		# plt.hist(RK_err_values, bins=75, histtype='step')
		plot_x_y_yerr(RK_err_values, bins=25, density=True, color='tab:blue')
		plt.text(0.95, 0.95, 'RK errors (hesse)', horizontalalignment='right',verticalalignment='top', transform=ax.transAxes)
		plt.axvline(np.mean(RK_err_values),c='tab:red')
		plt.axvline(np.mean(RK_err_values)+np.std(RK_err_values)/np.sqrt(np.shape(RK_err_values)[0]),c='tab:red',linestyle='--',alpha=0.5)
		plt.axvline(np.mean(RK_err_values)-np.std(RK_err_values)/np.sqrt(np.shape(RK_err_values)[0]),c='tab:red',linestyle='--',alpha=0.5)
		plt.text(0.95, 0.90, '%.5f+-%.5f'%(np.mean(RK_err_values),np.std(RK_err_values)/np.sqrt(np.shape(RK_err_values)[0])), horizontalalignment='right',verticalalignment='top', transform=ax.transAxes)

		
		ax = plt.subplot(2,3,3)
		# plt.hist(RK_residuals, bins=75, histtype='step')
		plot_x_y_yerr(RK_residuals, bins=25, density=True, color='tab:blue')
		plt.text(0.95, 0.95, 'abs(RK residuals)', horizontalalignment='right',verticalalignment='top', transform=ax.transAxes)
		plt.axvline(np.mean(RK_residuals),c='tab:red')
		plt.axvline(np.mean(RK_residuals)+np.std(RK_residuals)/np.sqrt(np.shape(RK_residuals)[0]),c='tab:red',linestyle='--',alpha=0.5)
		plt.axvline(np.mean(RK_residuals)-np.std(RK_residuals)/np.sqrt(np.shape(RK_residuals)[0]),c='tab:red',linestyle='--',alpha=0.5)
		plt.text(0.95, 0.90, '%.5f+-%.5f'%(np.mean(RK_residuals),np.std(RK_residuals)/np.sqrt(np.shape(RK_residuals)[0])), horizontalalignment='right',verticalalignment='top', transform=ax.transAxes)
		plt.text(0.95, 0.85, 'RMS %.5f'%(np.sqrt(np.mean(RK_residuals**2))), horizontalalignment='right',verticalalignment='top', transform=ax.transAxes)


		plt.savefig('%s/sensitivity_%s%s.pdf'%(__main__.plotting_directory,config,extra_string),bbox_inches='tight')
		plt.close('all')

		plt.figure(figsize=(6,4))
		ax = plt.subplot(1,1,1)
		plot_x_y_yerr(RK_values, bins=25, density=True, color='tab:blue')
		plt.axvline(x=1.,c='k')
		plt.text(0.95, 0.85, 'RMS of residuals %.5f'%(np.sqrt(np.mean(RK_residuals**2))), horizontalalignment='right',verticalalignment='top', transform=ax.transAxes)
		plt.text(0.95, 0.8, 'RMS of residuals <0.2 %.5f'%(np.sqrt(np.mean(RK_residuals[np.where(RK_residuals<0.2)]**2))), horizontalalignment='right',verticalalignment='top', transform=ax.transAxes)
		plt.savefig('%s/sensitivityRMS_%s%s.pdf'%(__main__.plotting_directory,config,extra_string),bbox_inches='tight')
		plt.close('all')


	else:
		
		return (np.sqrt(np.mean(RK_residuals**2)))



	
