import glob
import numpy as np
import pickle

def loader(file_locs, toy_configurations, quiet=False):

	files = glob.glob(file_locs)

	dict_of_results = {}
	list_of_parameters = {}
	for config in toy_configurations:
		dict_of_results[config] = {}
		list_of_parameters[config] = []
	for file_idx, file in enumerate(files):
		if file_idx % 25 == 0: 
			if quiet == False: 
				print(file_idx, '/', np.shape(files)[0])

		current_results = pickle.load( open( file, "rb" ) )[1] # Throw away custom cuts array [0].

		list_of_toys = list(current_results.keys())

		for toy_idx, toy in enumerate(list_of_toys):
			toy_results = current_results[toy]
			for config in toy_configurations:
				try:
					toy_result_i = toy_results[config]
					for item in list(toy_result_i.keys()):
						if item not in list_of_parameters[config] and 'err' not in item and 'pull' not in item:
							if item+'_err' in list(toy_result_i.keys()): # only save the names of parameters with errors and pulls.
								list_of_parameters[config].append(item)
						try:
							dict_of_results[config][item] = np.concatenate((dict_of_results[config][item], np.expand_dims(toy_result_i[item],0)), axis=0)
						except:
							dict_of_results[config][item] = np.expand_dims(toy_result_i[item],0)

				except:
					if quiet == False: print('Toy %d failed in'%toy_idx,file)
					pass

	return dict_of_results, list_of_parameters




def load_RK_values(file_locs, toy_configurations):

	files = glob.glob(file_locs)

	RK_values = np.empty((0,4))




	for file_idx, file in enumerate(files):
		if file_idx % 25 == 0: print(file_idx, '/', np.shape(files)[0])

		current_results = pickle.load( open( file, "rb" ) )[1] # Throw away custom cuts array [0].

		list_of_toys = list(current_results.keys())

		for toy_idx, toy in enumerate(list_of_toys):

			toy_results = current_results[toy]

			append = False

			try:
				values = np.empty(4)
				for config_idx, config in enumerate(toy_configurations):

					toy_result_i = toy_results[config]

					values[config_idx] = toy_result_i['RK']

					if values[config_idx] > 1.:
						values[config_idx+2] = toy_result_i['RK_lower']
					else:
						values[config_idx+2] = toy_result_i['RK_upper']


					print(toy_result_i['converged'], toy_result_i['valid'])
					if toy_result_i['converged'] != True:
						quit()
					if toy_result_i['valid'] != True:
						quit()

				append = True

			except:
				print('non-convergence')
				append = False

			if append:
				RK_values = np.append(RK_values,[values],axis=0)


				# for item in list(toy_result_i.keys()):
				# 	if item not in list_of_parameters[config] and 'err' not in item and 'pull' not in item:
				# 		if item+'_err' in list(toy_result_i.keys()): # only save the names of parameters with errors and pulls.
				# 			list_of_parameters[config].append(item)
				# 	try:
				# 		dict_of_results[config][item] = np.concatenate((dict_of_results[config][item], np.expand_dims(toy_result_i[item],0)), axis=0)
				# 	except:
				# 		dict_of_results[config][item] = np.expand_dims(toy_result_i[item],0)

		

	return RK_values




def load_BFKee_values(file_locs, toy_configurations):

	files = glob.glob(file_locs)

	BFKee_values = np.empty((0,2))




	for file_idx, file in enumerate(files):
		if file_idx % 25 == 0: print(file_idx, '/', np.shape(files)[0])

		current_results = pickle.load( open( file, "rb" ) )[1] # Throw away custom cuts array [0].

		list_of_toys = list(current_results.keys())

		for toy_idx, toy in enumerate(list_of_toys):

			toy_results = current_results[toy]

			append = False

			try:
				values = np.empty(2)
				for config_idx, config in enumerate(toy_configurations):

					toy_result_i = toy_results[config]

					values[config_idx] = toy_result_i['BFKee']

					# print(config, toy_result_i['BFKee'], toy_result_i['converged'], toy_result_i['valid'])
					if toy_result_i['converged'] != True:
						quit()
					if toy_result_i['valid'] != True:
						quit()

				append = True

			except:
				print('non-convergence')
				append = False

			if append:
				BFKee_values = np.append(BFKee_values,[values],axis=0)



	return BFKee_values







	