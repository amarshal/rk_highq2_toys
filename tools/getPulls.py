import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import pickle
import glob
import scipy.stats as stats
import math
import utils.loader as loader
import utils.plotting as plotting

print(' ')

plotting_directory = '/afs/cern.ch/user/m/marshall/RK/RK_toys/plotdir/' # Called from utils.plotting

base_output_location = '/eos/lhcb/user/m/marshall/RK/zfit/RK_jobs/'

def file_locs(folder):
	return base_output_location+'%s/fit_results*.pickle'%folder

toy_configurations =[
'combined_eTOS',
'separate_eTOS',
# 'combined_combined',
# 'combined_separate',
# 'separate_combined',
# 'separate_separate'
]

configuration_labels =[
'combined years, eTOS',
'separate years, eTOS',
# 'combined, combined',
# 'combined years, separate triggers',
# 'separate years, combined triggers',
# 'separate, separate'
]

def custom_plot(dict_of_results):


	RK1 = dict_of_results['combined_eTOS']['RK'][np.where((dict_of_results['combined_eTOS']["converged"]==True)&(dict_of_results['combined_eTOS']["valid"]==True))]
	RK2 = dict_of_results['separate_eTOS']['RK'][np.where((dict_of_results['separate_eTOS']["converged"]==True)&(dict_of_results['separate_eTOS']["valid"]==True))]

	RK1_err = dict_of_results['combined_eTOS']['RK_err'][np.where((dict_of_results['combined_eTOS']["converged"]==True)&(dict_of_results['combined_eTOS']["valid"]==True))]
	RK2_err = dict_of_results['separate_eTOS']['RK_err'][np.where((dict_of_results['separate_eTOS']["converged"]==True)&(dict_of_results['separate_eTOS']["valid"]==True))]

	plt.figure(figsize=(8,8))
	plt.subplot(2,2,1)
	plt.scatter(RK1, RK2,s=5)
	plt.xlabel('combined')
	plt.ylabel('separate')

	ax = plt.subplot(2,2,2)
	plt.scatter(RK1_err, RK2_err,s=5)

	lims = [
			    np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
			    np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
					]

	# now plot both limits against eachother
	ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
	ax.set_aspect('equal')
	ax.set_xlim(lims)
	ax.set_ylim(lims)

	plt.xlabel('combined')
	plt.ylabel('separate')


	ax = plt.subplot(2,2,3)
	plt.scatter(np.abs(np.ones(np.shape(RK1))-RK1), np.abs(np.ones(np.shape(RK2))-RK2),s=5)

	lims = [
			    np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
			    np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
					]

	plt.axhline(y=np.mean(np.abs(np.ones(np.shape(RK2))-RK2)))
	plt.axvline(x=np.mean(np.abs(np.ones(np.shape(RK1))-RK1)))
	print('combined',np.mean(np.abs(np.ones(np.shape(RK1))-RK1)))
	print('separate',np.mean(np.abs(np.ones(np.shape(RK2))-RK2)))


	plt.axhline(y=np.mean(np.abs(np.ones(np.shape(RK2[np.where(np.abs(np.ones(np.shape(RK2))-RK2)<0.1)]))-RK2[np.where(np.abs(np.ones(np.shape(RK2))-RK2)<0.1)])),c='r')
	plt.axvline(x=np.mean(np.abs(np.ones(np.shape(RK1[np.where(np.abs(np.ones(np.shape(RK1))-RK1)<0.1)]))-RK1[np.where(np.abs(np.ones(np.shape(RK1))-RK1)<0.1)])),c='r')
	print('combined',np.mean(np.abs(np.ones(np.shape(RK1[np.where(np.abs(np.ones(np.shape(RK1))-RK1)<0.1)]))-RK1[np.where(np.abs(np.ones(np.shape(RK1))-RK1)<0.1)])))
	print('separate',np.mean(np.abs(np.ones(np.shape(RK2[np.where(np.abs(np.ones(np.shape(RK2))-RK2)<0.1)]))-RK2[np.where(np.abs(np.ones(np.shape(RK2))-RK2)<0.1)])))


	# # now plot both limits against eachother
	ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
	ax.set_aspect('equal')
	ax.set_xlim(lims)
	ax.set_ylim(lims)

	plt.xlabel('combined')
	plt.ylabel('separate')



	plt.savefig('%s/custom.pdf'%(plotting_directory),bbox_inches='tight')
	plt.close('all')

RK = 1

for floatArgus in ['true','false']:
	for usemklcut in ['true','false']:
		for floatarguslambda in ['true','false']:
			print(' ')
			additional_options = {}

			result_location = 'KeeYieldLARGE_floatArgus_%s_mklcut_%s_floatlambda_%s'%(floatArgus,usemklcut,floatarguslambda)

			dict_of_results, list_of_parameters = loader.loader(file_locs(result_location), toy_configurations, quiet=True)

			for config in toy_configurations:

				try:
					frac = plotting.convergence_success(config, list_of_parameters, dict_of_results, extra_string=result_location+config)
					print(result_location, config, '%.3f'%frac)

					RMS = plotting.sensitivity(config, list_of_parameters, dict_of_results, extra_string=result_location+config+'_converged', onlyConvergedFits=True)
					print(result_location, config, '%.5f'%RMS)
					plotting.pulls(config, list_of_parameters, dict_of_results, extra_string=result_location+config+'_converged', onlyConvergedFits=True)
					plotting.mean_correlations(config, list_of_parameters, dict_of_results, extra_string=result_location+config+'_converged', onlyConvergedFits=True)
				except:
					pass
		# 	print('mean combi yield', dict_of_results['combined_eTOS']['RK'])

		# quit()

for result_location in ['KeeYieldLARGE_floatArgus_true_mklcut_false_floatlambda_false_fixCascades_true']:
	dict_of_results, list_of_parameters = loader.loader(file_locs(result_location), toy_configurations, quiet=True)
	for config in toy_configurations:
		print(' ')
		try:
			frac = plotting.convergence_success(config, list_of_parameters, dict_of_results, extra_string=result_location+config)
			print(result_location, config, '%.3f'%frac)

			RMS = plotting.sensitivity(config, list_of_parameters, dict_of_results, extra_string=result_location+config+'_converged', onlyConvergedFits=True)
			print(result_location, config, '%.5f'%RMS)
			plotting.pulls(config, list_of_parameters, dict_of_results, extra_string=result_location+config+'_converged', onlyConvergedFits=True)
			plotting.mean_correlations(config, list_of_parameters, dict_of_results, extra_string=result_location+config+'_converged', onlyConvergedFits=True)
		except:
			pass

RK = 0.846

for result_location in ['KeeYieldLARGE_floatArgus_true_mklcut_true_floatlambda_false_RK_0846', 'KeeYieldLARGE_floatArgus_true_mklcut_true_floatlambda_false_RK_0846_BOOST']:
	dict_of_results, list_of_parameters = loader.loader(file_locs(result_location), toy_configurations, quiet=True)
	for config in toy_configurations:

		try:
			print(' ')
			frac = plotting.convergence_success(config, list_of_parameters, dict_of_results, extra_string=result_location+config)
			print(result_location, config, '%.3f'%frac)

			RMS = plotting.sensitivity(config, list_of_parameters, dict_of_results, extra_string=result_location+config+'_converged', onlyConvergedFits=True, RK=0.846)
			print(result_location, config, '%.5f'%RMS)
			plotting.pulls(config, list_of_parameters, dict_of_results, extra_string=result_location+config+'_converged', onlyConvergedFits=True)
			plotting.mean_correlations(config, list_of_parameters, dict_of_results, extra_string=result_location+config+'_converged', onlyConvergedFits=True)
		except:
			pass
quit()

# for result_location in ['floatArgus_true_mklcut_true', 'floatArgus_false_mklcut_true', 'floatArgus_true_mklcut_false', 'floatArgus_false_mklcut_false','floatArgus_true_jonas_mklcut_true', 'floatArgus_false_jonas_mklcut_true', 'floatArgus_true_jonas_mklcut_false', 'floatArgus_false_jonas_mklcut_false']: # Jonas 
# for result_location in ['testing_years']: 
# for result_location in ['years_fixing_constraints']: 
# for result_location in ['fitrange']: 
# for result_location in ['attempt2_fixing']: 
# for result_location in ['invRK2']: 
# for result_location in ['fixCascades_false','fixCascades_true']: 

RK = 0.846
# for result_location in ['KeeYield_floatArgus_false_mklcut_false_floatlambda_false']:
for result_location in ['KeeYieldLARGE_floatArgus_true_mklcut_true_floatlambda_false_RK_0846']:
# KeeYieldLARGE_floatArgus_true_mklcut_true_floatlambda_false
# KeeYieldLARGE_floatArgus_true_mklcut_true_floatlambda_false_RK_0846
	# print(result_location)
	dict_of_results, list_of_parameters = loader.loader(file_locs(result_location), toy_configurations)

	# custom_plot(dict_of_results)

	for config in toy_configurations:


		# try:
			plotting.convergence_success(config, list_of_parameters, dict_of_results, extra_string=result_location+config)

			# plotting.pulls(config, list_of_parameters, dict_of_results, extra_string=result_location)
			plotting.pulls(config, list_of_parameters, dict_of_results, extra_string=result_location+config+'_converged', onlyConvergedFits=True)

			# plotting.values(config, list_of_parameters, dict_of_results, extra_string=result_location)
			plotting.values(config, list_of_parameters, dict_of_results, extra_string=result_location+config+'_converged', onlyConvergedFits=True)

			# plotting.mean_correlations(config, list_of_parameters, dict_of_results, extra_string=result_location)
			plotting.mean_correlations(config, list_of_parameters, dict_of_results, extra_string=result_location+config+'_converged', onlyConvergedFits=True)

			# plotting.sensitivity(config, list_of_parameters, dict_of_results, extra_string=result_location+config)
			plotting.sensitivity(config, list_of_parameters, dict_of_results, extra_string=result_location+config+'_converged', onlyConvergedFits=True)
		# except:
		# 	print('FAILED')

quit()




for result_location in ['attempt2_fixing']: 
# for result_location in ['inverseRK']: 
# for result_location in ['invRK2']: 


	RK_values = loader.load_RK_values(file_locs(result_location), toy_configurations)

	print(RK_values, np.shape(RK_values))

	RK_combined = RK_values[:,0]
	RK_separate = RK_values[:,1]

	RK_combined_err = RK_values[:,2]
	RK_separate_err = RK_values[:,3]

	RK_combined_residuals = np.abs(np.ones(np.shape(RK_combined))-RK_combined)
	RK_separate_residuals = np.abs(np.ones(np.shape(RK_separate))-RK_separate)

	print('comb',np.sqrt(np.mean(RK_combined_residuals**2))/np.mean(RK_combined))
	print('sep',np.sqrt(np.mean(RK_separate_residuals**2))/np.mean(RK_separate))

	quit()

	# RK_combined_residuals_noabs = np.ones(np.shape(RK_combined))-RK_combined
	# RK_separate_residuals_noabs = np.ones(np.shape(RK_separate))-RK_separate

	# print('comb',np.sqrt(np.mean(RK_combined_residuals_noabs**2)))
	# print('sep',np.sqrt(np.mean(RK_separate_residuals_noabs**2)))


	xdims = 5
	ydims = 3
	plt.figure(figsize=(4*xdims,4*ydims))
	plt.subplot(ydims,xdims,1)
	plt.title('values')
	plt.hist([RK_combined, RK_separate], bins=100, label=['comb','sep'], histtype='step')
	plt.axvline(x=1,c='k')
	plt.legend()

	plt.subplot(ydims,xdims,2)
	plt.title('residuals')
	plt.hist([np.ones(np.shape(RK_combined))-RK_combined, np.ones(np.shape(RK_separate))-RK_separate], bins=100, label=['comb','sep'], histtype='step')
	plt.axvline(x=0,c='k')
	plt.legend()

	plt.subplot(ydims,xdims,3)
	plt.title('abs residuals')
	plt.hist([RK_combined_residuals, RK_separate_residuals], bins=100, label=['comb','sep'], histtype='step')
	plt.legend()

	ax = plt.subplot(ydims,xdims,4)
	plt.title('diff residuals')
	plt.hist([RK_combined_residuals-RK_separate_residuals], bins=100, label=['comb-sep'], histtype='step')
	plt.legend()
	plt.axvline(x=0,c='k')
	plt.text(0.97, 0.87, 'mean %.7f'%np.mean(RK_combined_residuals-RK_separate_residuals), horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, fontsize=10)


	ax = plt.subplot(ydims,xdims,5)
	plt.title('residuals')
	plt.scatter(np.ones(np.shape(RK_combined))-RK_combined, np.ones(np.shape(RK_separate))-RK_separate, s=5)
	plt.xlabel('comb')
	plt.ylabel('sep')
	lims = [
	    np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
	    np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
	]

	# now plot both limits against eachother
	ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
	ax.set_aspect('equal')
	ax.set_xlim(lims)
	ax.set_ylim(lims)
	plt.axvline(x=0,c='k')
	plt.axhline(y=0,c='k')


	ax = plt.subplot(ydims,xdims,6)
	plt.title('abs residuals')
	plt.scatter(RK_combined_residuals, RK_separate_residuals, s=5)
	plt.xlabel('comb')
	plt.ylabel('sep')
	lims = [
	    np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
	    np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
	]

	# now plot both limits against eachother
	ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
	ax.set_aspect('equal')
	ax.set_xlim(lims)
	ax.set_ylim(lims)

	plt.subplot(ydims,xdims,7)
	plt.title('abs residuals**2')
	plt.hist([RK_combined_residuals**2, RK_separate_residuals**2], bins=25, label=['comb','sep'], histtype='step')
	plt.yscale('log')
	plt.legend()

	ax= plt.subplot(ydims,xdims,8)
	plt.title('combined RK vs diff res')
	plt.scatter(RK_combined, RK_combined_residuals-RK_separate_residuals, s=5)
	plt.axhline(y=0,c='k')
	plt.axvline(x=1,c='k')
	lims = [
	    -np.max([ax.get_ylim(), ax.get_ylim()]),  # min of both axes
	    np.max([ax.get_ylim(), ax.get_ylim()]),  # max of both axes
	]
	ax.set_ylim(lims)

	ax=plt.subplot(ydims,xdims,9)
	plt.title('separate RK vs diff res')
	plt.scatter(RK_separate, RK_combined_residuals-RK_separate_residuals, s=5)
	plt.axhline(y=0,c='k')	
	plt.axvline(x=1,c='k')
	lims = [
	    -np.max([ax.get_ylim(), ax.get_ylim()]),  # min of both axes
	    np.max([ax.get_ylim(), ax.get_ylim()]),  # max of both axes
	]
	ax.set_ylim(lims)



	ax=plt.subplot(ydims,xdims,10)
	plt.title('separate RK vs combined RK')
	plt.scatter(RK_combined, RK_separate, s=5)
	plt.xlabel('comb')
	plt.ylabel('sep')
	lims = [
	    np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
	    np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
	]

	# now plot both limits against eachother
	ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
	ax.set_aspect('equal')
	ax.set_xlim(lims)
	ax.set_ylim(lims)


	ordered_RK_values = RK_values[RK_values[:, 0].argsort()]

	ordered_RK_combined_residuals = np.ones(np.shape(ordered_RK_values[:,0]))-ordered_RK_values[:,0]
	ordered_RK_separate_residuals = np.ones(np.shape(ordered_RK_values[:,1]))-ordered_RK_values[:,1]

	# print(ordered_RK_values[:,0])

	ax=plt.subplot(ydims,xdims,11)
	plt.title('ordered combined residuals')
	plt.scatter(np.arange(np.shape(ordered_RK_combined_residuals)[0]),ordered_RK_combined_residuals, label='comb', s=5)
	plt.axhline(y=0,c='k')	
	lims = [
	    -np.max([ax.get_ylim(), ax.get_ylim()]),  # min of both axes
	    np.max([ax.get_ylim(), ax.get_ylim()]),  # max of both axes
	]
	ax.set_ylim(lims)
	plt.xlabel('low RK on left')

	ax=plt.subplot(ydims,xdims,12)
	plt.title('ordered separate residuals')
	plt.scatter(np.arange(np.shape(ordered_RK_separate_residuals)[0]),ordered_RK_separate_residuals, label='sep', s=5)
	plt.axhline(y=0,c='k')	
	lims = [
	    -np.max([ax.get_ylim(), ax.get_ylim()]),  # min of both axes
	    np.max([ax.get_ylim(), ax.get_ylim()]),  # max of both axes
	]
	ax.set_ylim(lims)


	ax=plt.subplot(ydims,xdims,13)
	plt.title('ordered diff residuals')
	plt.scatter(np.arange(np.shape(ordered_RK_separate_residuals)[0]),np.abs(ordered_RK_combined_residuals)-np.abs(ordered_RK_separate_residuals), label='sep', s=5)
	plt.axhline(y=0,c='k')	
	lims = [
	    -np.max([ax.get_ylim(), ax.get_ylim()]),  # min of both axes
	    np.max([ax.get_ylim(), ax.get_ylim()]),  # max of both axes
	]
	ax.set_ylim(lims)



	plt.savefig('%s/custom.pdf'%(plotting_directory),bbox_inches='tight')
	plt.close('all')



	quit()











# for result_location in ['BFKee']: 
for result_location in ['test_constraints']: 


	BFKee_values = loader.load_BFKee_values(file_locs(result_location), toy_configurations)

	print(BFKee_values, np.shape(BFKee_values))

	# for i in range(100):

	# 	print(RK_values[i], RK_values[i][1]- RK_values[i][0])


	RK_combined = BFKee_values[:,0]
	RK_separate = BFKee_values[:,1]

	RK_combined_residuals = np.abs(np.ones(np.shape(RK_combined))*4.8784e-7-RK_combined)
	RK_separate_residuals = np.abs(np.ones(np.shape(RK_separate))*4.8784e-7-RK_separate)

	print('comb',np.sqrt(np.mean(RK_combined_residuals**2)))
	print('sep',np.sqrt(np.mean(RK_separate_residuals**2)))







