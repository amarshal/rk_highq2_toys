import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import pickle
import glob
import scipy.stats as stats
import math

directory = '0'
file = 'plotdir/%s/fit_results_%sprocess-99.pickle'%(directory,directory)

toy_configurations =[
'combined_eTOS',
# 'separate_eTOS',
# 'combined_combined',
# 'combined_separate',
# 'separate_combined',
# 'separate_separate'
]

configuration_labels =[
'combined years, eTOS',
# 'separate years, eTOS',
# 'combined, combined',
# 'combined years, separate triggers',
# 'separate years, combined triggers',
# 'separate, separate'
]

##############################
dict_of_results = {}
list_of_parameters = {}
for config in toy_configurations:
	dict_of_results[config] = {}
	list_of_parameters[config] = []

current_results = pickle.load( open( file, "rb" ) )[1]
list_of_toys = list(current_results.keys())
for toy_idx, toy in enumerate(list_of_toys):
	toy_results = current_results[toy]
	for config in toy_configurations:
		try:
			toy_result_i = toy_results[config]
			if toy_result_i['converged'] != True or toy_result_i['valid'] != True:
				continue
			for item in list(toy_result_i.keys()):
				if item not in list_of_parameters[config] and 'err' not in item and 'pull' not in item:
					if item+'_err' in list(toy_result_i.keys()): # only save the names of parameters with errors and pulls.
						list_of_parameters[config].append(item)
				try:
					dict_of_results[config][item] = np.append(dict_of_results[config][item], toy_result_i[item])
				except:
					dict_of_results[config][item] = np.asarray([toy_result_i[item]])
		except:
			print('Toy %d failed in'%toy_idx,file)
			pass
##############################


for config in toy_configurations:
	print(' ')
	print('***********************')
	print(config)
	print('***********************')
	print(' ')
	for result in list(dict_of_results[config].keys()):
		print(result, dict_of_results[config][result])

















