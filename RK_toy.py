import os
os.environ["ZFIT_DISABLE_TF_WARNINGS"] = "1"
import uproot as ur
import zfit
import zfit_physics as zphys
from zfit import z
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import pylab
import mplhep
mplhep.style.use('LHCb2')
import pickle
import argparse
import time
import zfit.z.numpy as znp

print("zfit.__version__", zfit.__version__)

####################################################################################################################################################
# Initiate scripts ...
####################################################################################################################################################

print('\n\n*****************************************************************')
print('\n\t\tRK high q2 sensitivity toys \n')
print('*****************************************************************')
print('\n\n')

from utils.utilities import *
from utils.plotFunctions import *
from utils.getPreselection import *
from utils.config import *
from utils.definitions import *
from utils.PDFs import *

if seed != -1:
	print('Setting seed to %d'%seed)
	zfit.settings.set_seed(seed)

if boost_stats != 1.:
	boost_eTOS = boost_stats
	boost_TIS = boost_stats

print('\n\n')
print("*********************************")


####################################################################################################################################################
# Set some options ...
####################################################################################################################################################

# load_toy_from_pickle = True
load_toy_from_pickle = False

if gitlab_CI: load_toy_from_pickle = True

add_constraints = True

fix_the_corrections = True

combi_option = 'ArgusExponential' # 'Exponential'




####################################################################################################################################################
# Sort fitting options ...
####################################################################################################################################################

trigger_list_Kuu = ['uTOS']
trigger_list = [trigger_cat_names[trigger] for trigger in use_trigger_categories]

if len(trigger_list) == 1:
	trigger_options_to_fit = ['eTOS']
else:
	trigger_options_to_fit = triggers

years_options_to_fit = years

year_list = ['2012', '2016', '2017']
if 'combined' in years_options_to_fit:
	year_list.append('combined')

trigger_options_to_fit_loop = trigger_options_to_fit
if len(trigger_list) > 1:
	trigger_list_all = trigger_list + ['all']
else:
	trigger_list_all = trigger_list
	trigger_options_to_fit_loop = ['eTOS']





####################################################################################################################################################
# Set some variable and define some dictionaries ...
####################################################################################################################################################

pars = {}
list_of_constraints = {}

if load_toy_from_pickle:
	save_shapes_and_parameters = False
	load_shapes_and_parameters = True
else:
	save_shapes_and_parameters = True
	load_shapes_and_parameters = False

fit_ee_mode = True
fit_uu_mode = True


####################################################################################################################################################
# Set constants ...
####################################################################################################################################################

pars['value_for_corrections'] = zfit.Parameter("value_for_corrections", 0.3, floating=False)# in eTOS - if using base_Kee_yield_off_2018_Kuu this is overwritten downstairs
frac_error_for_corrections = 0.05

BFKee=4.8784e-7*(1./RK)

def RK_fn(BF):
	return 4.8784e-7/BF
if fit_BFKee:
	pars["BFKee"] = zfit.Parameter("BFKee", 4.8784e-7, 4.8784e-9, 4.8784e-5)
	pars["RK"] = zfit.ComposedParameter("RK",RK_fn,params=[pars["BFKee"]])
else:
	pars["RK"] = zfit.Parameter("RK", RK, 0, 10.)
print('\n\nToy running with an RK value of',RK,'\n')

BFKstee=(((1./3.)*0.904*1e-6)+((2./3.)*0.904*1e-6)) #https://arxiv.org/pdf/1606.04731.pdf 
BFPsi2SKee = 7.89e-3*6.21e-4
effStrip=0.13846# pg 166 of RK ana note
NKee_bf_Run1 = (295e-6*3e15*2*0.4*BFKee)*0.1785 #sigma_bbX*lumi*fragfrac*BF # 72e-6 and 144e-6 from https://arxiv.org/pdf/1612.05140.pdf pg 7
NKee_bf_Run2 = (560e-6*6e15*2*0.4*BFKee)*0.1785 #sigma_bbX*lumi*fragfrac*BF
NKee_bf=NKee_bf_Run1+NKee_bf_Run2
decProdSignal=0.16 # more like this: 0.1756
decProdCascade1=0.1852326287*0.00189 # decProdCascade1=0.0055 #Old RK MC
decProdCascade2=0.027777
decProdCascade3=0.1887987989*0.09319 # decProdCascade3=0.0621 #Old RK MC
BFMisID = 50.e-6;
BFcas1=9.45e-4
BFcas2=1.06e-3
BFcas3=1.5e-4
BFcas12=1.7e-4
BFcas13=1.3e-5

Nsim = {'Kee':2E6,'Cas1':2E6,'Cas2':2E6,'Cas3':2E6,'Kpipi':2E7,'Kstee':8E6,'Cas12':2E6,'Cas13':2E6} 
DecProd = {'Kee':0.17562,'Cas1':0.1852326287*0.00189,'Cas2':0.027777,'Cas3':0.1887987989*0.09319,'Kpipi':0.14778,'Kstee':0.17378,'Cas12':0.188*0.087062,'Cas13':0.188*0.129314} 

combi_frac_in_kuu = 1196./1459.

expected_fracs_of_run_periods = {'2012':0.27,'2016':0.23,'2017':0.5,'combined':1.,'':1.} # Ballpark 

if use_presel_tuples: 
	extra_file_string = '_presel_BDT'
	presel_eff = {'Kee':0.0630906, 'K*ee':0.0407758, 'Cas1':0.0758727, 'Cas2':0.032914, 'Cas3':0.326685, 'Kpipi':0.450701, 'Cas12':0.275331, 'Cas13':0.263258}
else: 
	extra_file_string = ''
	presel_eff = {'Kee':1., 'K*ee':1., 'Cas1':1., 'Cas2':1., 'Cas3':1., 'Kpipi':1.}

use_kpipi_PIDcalib_weights = True

if not load_shapes_and_parameters:
	####################################################################################################################################################
	print('\nOpening files...\n')
	####################################################################################################################################################

	PATH_MC='/eos/lhcb/user/m/marshall/RK/pythonBDT/tuples/' 
	PATH_Data='/eos/lhcb/user/m/marshall/RK/tuples_from_uzh/' 

	year=2018

	tupleMCName='Kee_truthed_'+str(year)+'_PIDWeight%s.root'%extra_file_string
	fileMC = ur.open(PATH_MC+tupleMCName)

	tupleMCNKpipiame='B2Kpipi_MC_'+str(year)+'_truthed_CORR_PIDWeight%s_PIDcalibeTOSWeights_individualPID.root'%extra_file_string
	fileMCKpipi = ur.open(PATH_MC+tupleMCNKpipiame)

	tupleMCKstName='Kstee'+str(year)+'_truthed_PIDWeight%s_LASS.root'%extra_file_string
	fileMCKst = ur.open(PATH_MC+tupleMCKstName)

	tupleMCCas1Name='Bu2D0Kenuenu_'+str(year)+'_truthed_PIDWeight%s.root'%extra_file_string
	fileMCCas1 = ur.open(PATH_MC+tupleMCCas1Name)
	tupleMCCas2Name='Bu2D0KPienu_'+str(year)+'_truthed_PIDWeight%s.root'%extra_file_string
	fileMCCas2 = ur.open(PATH_MC+tupleMCCas2Name)
	tupleMCCas3Name='Bu2D0KenuPi_'+str(year)+'_truthed_PIDWeight%s.root'%extra_file_string
	fileMCCas3 = ur.open(PATH_MC+tupleMCCas3Name)
	tupleMCCas12Name='B2DstD0Kenupi0pi_'+str(year)+'_truthed_PIDWeight%s.root'%extra_file_string
	fileMCCas12 = ur.open(PATH_MC+tupleMCCas12Name)
	tupleMCCas13Name='B2D0KenuK_'+str(year)+'_truthed_PIDWeight%s.root'%extra_file_string
	fileMCCas13 = ur.open(PATH_MC+tupleMCCas13Name)

	tupleDataName='B2Kee_'+str(year)+'_CommonPresel_HltTOS_bdt_reduced.root'
	fileData = ur.open(PATH_Data+tupleDataName)

	PATH_Data='/disk/lhcb_data/davide/RKHighq2/tuples/'+str(year)+'/data/KeeTuples/HltTOS/'
	tupleDataName='B2Kee_'+str(year)+'_CommonPresel_HltTOS_bdt_reduced.root'

plotdir = saveLocation+'/'+str(foldernum)+'/'
os.system("mkdir -p "+plotdir)

print("\nPlots will be saved to: %s\n"%plotdir.replace('//', '/'))


####################################################################################################################################################
# Define selection ...
####################################################################################################################################################

obs = zfit.Space('B_plus_M', (lower_limit_of_mass_window,6200))
obs_extended = zfit.Space('B_plus_M', (extended_lower_limit_of_mass_window, 6200))
lower_limit_of_mass_window_kuu = 5180
obs_Kuu = zfit.Space('B_plus_M', (5180, 5600))
obs_right_sideband = zfit.Space('B_plus_M', (5400, 6200))

preselStringNoPid=getCommonPreselNoPidCutRun2(custom_cuts).replace('&&','and').replace('TMath::Abs','abs').replace('||','or')

print("*********************************")
print('\nSelection:\n')
print(preselStringNoPid+'\n')
print("*********************************")

preselString=getCommonPreselRun2(custom_cuts).replace('&&','and').replace('TMath::Abs','abs').replace('||','or') # use this for combinatorial
preselString=preselString.replace('e_minus_PIDe>3','e_minus_PIDe>4').replace('e_plus_PIDe>3','e_plus_PIDe>4')

q2=14.3*1e6
q2Cut = "J_psi_1S_TRACK_M*J_psi_1S_TRACK_M > "+str(q2)

massRangeCut= "B_plus_M>%d and B_plus_M<6200"%lower_limit_of_mass_window
massRangeCutExt = "B_plus_M>%d and B_plus_M<6200"%extended_lower_limit_of_mass_window
BDTCut_trigger_cuts = ["(BDTGETOSRun3T>0.78 and passTrigCat0==1)", "(BDTGETOSRun3T>0.75 and passTrigCat1==1)", "(BDTGfixedRun3T>0.8 and passTrigCat2==1)"]
BDTCut = "("
for loop_idx, trigger in enumerate(use_trigger_categories):
	if loop_idx != 0: BDTCut += " or "
	BDTCut += BDTCut_trigger_cuts[trigger]
BDTCut += ")"
BDTCut_comb = "BDT_score_selection>0.7"

blindRangeCut="(B_plus_M>5400 and B_plus_M<6200)"

default_weights = [kpipi_correction, SwaveWeight, kinematic_weight, trigger_weight_0, trigger_weight_1, trigger_weight_2, PID_weight]
alt_weights = [kpipi_correction, SwaveWeight, kinematic_weight, trigger_weight_0, trigger_weight_1, trigger_weight_2, PID_weight]
alt_fit = False
fit_modes = ['','_alt']
if alt_fit == False:
	fit_modes = ['']


def set_weight_names(fit_mode):
	set_weights = default_weights
	if 'alt' in fit_mode:
		set_weights = alt_weights
	global kpipi_correction, SwaveWeight, kinematic_weight, trigger_weight_0, trigger_weight_1, trigger_weight_2, PID_weight
	kpipi_correction = set_weights[0]
	SwaveWeight = set_weights[1]
	kinematic_weight = set_weights[2]
	trigger_weight_0 = set_weights[3]
	trigger_weight_1 = set_weights[4]
	trigger_weight_2 = set_weights[5]
	PID_weight = set_weights[6]


####################################################################################################################################################
# Create B mass arrays ...
####################################################################################################################################################

if not load_shapes_and_parameters:

	print('\n Opening tuples...\n')

	entry_stop = None
	if truncate_trees: 
		entry_stop = 1000
		print('\n Truncating trees at %d (for testing)\n'%entry_stop)

	treeMC=fileMC['DecayTree']
	KeeMC=treeMC.arrays(library='pd', filter_name=branches, entry_stop= entry_stop)
	KeeMC = KeeMC.query("subentry==0")
	KeeMC*=1.

	treeMCKpipi=fileMCKpipi['DecayTree']
	KpipiMC=treeMCKpipi.arrays(library='pd', filter_name=branches, entry_stop= entry_stop)
	KpipiMC = KpipiMC.query("subentry==0")
	KpipiMC*=1.

	treeMCKst=fileMCKst['DecayTree']
	KstMC=treeMCKst.arrays(library='pd', filter_name=branches, entry_stop= entry_stop)
	KstMC = KstMC.query("subentry==0")
	KstMC*=1.

	treeMCCas1=fileMCCas1['DecayTree']
	treeMCCas2=fileMCCas2['DecayTree']
	treeMCCas3=fileMCCas3['DecayTree']
	treeMCCas12=fileMCCas12['DecayTree']
	treeMCCas13=fileMCCas13['DecayTree']
	Cas1MC=treeMCCas1.arrays(library='pd', filter_name=branches, entry_stop= entry_stop)
	Cas1MC = Cas1MC.query("subentry==0")
	Cas1MC*=1.
	Cas2MC=treeMCCas2.arrays(library='pd', filter_name=branches, entry_stop= entry_stop)
	Cas2MC = Cas2MC.query("subentry==0")
	Cas2MC*=1.
	Cas3MC=treeMCCas3.arrays(library='pd', filter_name=branches, entry_stop= entry_stop)
	Cas3MC = Cas3MC.query("subentry==0")
	Cas3MC*=1.
	Cas12MC=treeMCCas12.arrays(library='pd', filter_name=branches, entry_stop= entry_stop)
	Cas12MC = Cas12MC.query("subentry==0")
	Cas12MC*=1.
	Cas13MC=treeMCCas13.arrays(library='pd', filter_name=branches, entry_stop= entry_stop)
	Cas13MC = Cas13MC.query("subentry==0")
	Cas13MC*=1.

	treeData=fileData['DecayTree']
	Data=treeData.arrays(library='pd', filter_name=branches)
	Data*=1.

	print("\n\n*********************************")
	print("Building MC arrays from tuples...")
	print("*********************************\n")

	KeeMC_arrays = get_MC_arrays(KeeMC, 'Kee')
	KpipiMC_arrays = get_MC_arrays(KpipiMC, 'Kpipi')
	KstMC_arrays = get_MC_arrays(KstMC, 'K*ee')
	Cas1MC_arrays = get_MC_arrays(Cas1MC, 'Cas1')
	Cas2MC_arrays = get_MC_arrays(Cas2MC, 'Cas2')
	Cas3MC_arrays = get_MC_arrays(Cas3MC, 'Cas3')
	Cas12MC_arrays = get_MC_arrays(Cas12MC, 'Cas12')
	Cas13MC_arrays = get_MC_arrays(Cas13MC, 'Cas13')
	CasMC_arrays = {}
	for key in Cas1MC_arrays:
		total_array = np.concatenate((Cas1MC_arrays[key], Cas2MC_arrays[key], Cas3MC_arrays[key], Cas12MC_arrays[key], Cas13MC_arrays[key]), axis=1)
		CasMC_arrays[key] = total_array[:,np.where(total_array[1]>0.)[0]]

	Combi_arrays = get_MC_arrays_data(Data, 'combi')

	if alt_fit:

		set_weight_names('_alt') # Set default weights

		KeeMC_arrays_alt = get_MC_arrays(KeeMC, 'Kee')
		KpipiMC_arrays_alt = get_MC_arrays(KpipiMC, 'Kpipi')
		KstMC_arrays_alt = get_MC_arrays(KstMC, 'K*ee')
		Cas1MC_arrays_alt = get_MC_arrays(Cas1MC, 'Cas1')
		Cas2MC_arrays_alt = get_MC_arrays(Cas2MC, 'Cas2')
		Cas3MC_arrays_alt = get_MC_arrays(Cas3MC, 'Cas3')
		Cas12MC_arrays_alt = get_MC_arrays(Cas12MC, 'Cas12')
		Cas13MC_arrays_alt = get_MC_arrays(Cas13MC, 'Cas13')
		CasMC_arrays_alt = {}
		for key in Cas1MC_arrays_alt:
			total_array_alt = np.concatenate((Cas1MC_arrays_alt[key], Cas2MC_arrays_alt[key], Cas3MC_arrays_alt[key], Cas12MC_arrays_alt[key], Cas13MC_arrays_alt[key]), axis=1)
			CasMC_arrays_alt[key] = total_array_alt[:,np.where(total_array_alt[1]>0.)[0]]

		Combi_arrays_alt = get_MC_arrays_data(Data, 'combi')

######################################################################################################################################################################################################################################################################################################

# # Plotting some example ArgusExponential distributions
# combi_models = {}
# # Build Kee mode PDFs
# for year_idx, year in enumerate(year_list):
# 	for trigger in trigger_list_all:
# 		for fit_mode in fit_modes:
# 			combi_models[year+'_'+trigger+fit_mode] = build_combi_model(pars, obs, run_string = year, trigger_string = trigger, extra_string = fit_mode, option = combi_option)
# 			plot_model_combi_nodata(combi_models[year+'_'+trigger+fit_mode], save_plot=True, fileName=plotdir+'CHECKcombi_sideband_BDT_%s_'%trigger+str(foldernum)+'%s'%fit_mode, xmin=4880, xmax=6200)
# 			quit()
# quit()

######################################################################################################################################################################################################################################################################################################


####################################################################################################################################################
print("Building the PDFs...")
####################################################################################################################################################

pars["meanShift"] = zfit.Parameter('meanShift', 0., floating=False)
pars["sigmaScale"] = zfit.Parameter('sigmaScale', 1., floating=False)

sig_models0 = {}
sig_models1 = {}
sig_models2 = {}
sig_models = {}

combi_models = {}
prc_models = {}
cas_models = {}
kpipi_models = {}

if save_shapes_and_parameters:
	prc_data_sets = {}
	cas_data_sets = {}
if load_shapes_and_parameters:
	prc_data_sets = pickle.load( open( 'number_storage/prc_data_sets.pickle', "rb" ) )
	cas_data_sets = pickle.load( open( 'number_storage/cas_data_sets.pickle', "rb" ) )

####################################################################################################################################################
# Build Kee PDFs ...
####################################################################################################################################################

for year_idx, year in enumerate(year_list):
	for trigger in trigger_list_all:
		for fit_mode in fit_modes:
			
			if not load_shapes_and_parameters:
				if fit_mode == '' or fit_mode == '_alt': # Select the array with the correct weights
					KeeMC_arrays_i = KeeMC_arrays
					KstMC_arrays_i = KstMC_arrays
					CasMC_arrays_i = CasMC_arrays
					Combi_arrays_i = Combi_arrays
					KpipiMC_arrays_i = KpipiMC_arrays
				else:
					KeeMC_arrays_i = KeeMC_arrays_alt
					KstMC_arrays_i = KstMC_arrays_alt
					CasMC_arrays_i = CasMC_arrays_alt
					Combi_arrays_i = Combi_arrays_alt
					KpipiMC_arrays_i = KpipiMC_arrays_alt

			if trigger != 'all': # The all PDF will be constructed later
				# Building signal PDFs
				if not load_shapes_and_parameters:
					try:
						pars['f0g_%s%s'%(trigger,fit_mode)] = zfit.Parameter("f0g_%s%s"%(trigger,fit_mode), np.sum(KeeMC_arrays_i['all_%s_0'%trigger][1])/np.sum(KeeMC_arrays_i['all_%s_all'%trigger][1]), floating=False)
						pars['f1g_%s%s'%(trigger,fit_mode)] = zfit.Parameter("f1g_%s%s"%(trigger,fit_mode), np.sum(KeeMC_arrays_i['all_%s_1'%trigger][1])/np.sum(KeeMC_arrays_i['all_%s_all'%trigger][1]), floating=False)
					except:
						pass
				else: # Load from file later
					try:
						pars['f0g_%s%s'%(trigger,fit_mode)] = zfit.Parameter("f0g_%s%s"%(trigger,fit_mode), 0., floating=False)
						pars['f1g_%s%s'%(trigger,fit_mode)] = zfit.Parameter("f1g_%s%s"%(trigger,fit_mode), 0., floating=False)
					except:
						pass

				sig_models0[year+'_'+trigger+fit_mode], sig_models1[year+'_'+trigger+fit_mode], sig_models2[year+'_'+trigger+fit_mode], sig_models[year+'_'+trigger+fit_mode] = build_sig_model(pars, obs, run_string = year, trigger_string = trigger, extra_string=fit_mode)

			# Building Combinatorial PDFs
			combi_models[year+'_'+trigger+fit_mode] = build_combi_model(pars, obs, run_string = year, trigger_string = trigger, extra_string = fit_mode, option = combi_option)

			sub_sample_string = '_%s_%s%s'%(year,trigger,fit_mode)

			# Building Kpipi PDFs
			kpipi_models[year+'_'+trigger+fit_mode] = build_kpipi_model(pars, obs, run_string = year, trigger_string = trigger, extra_string = fit_mode)

			# Building part-reco PDFs
			if load_shapes_and_parameters: # Load quickly numpy arrays from number_storage
				B_plus_M_partreco = zfit.Data.from_numpy(obs=obs_extended, array=prc_data_sets[year+'_'+trigger+fit_mode][0], weights=prc_data_sets[year+'_'+trigger+fit_mode][1])
				prc_models[year+'_'+trigger+fit_mode] = build_prc_model(pars, obs, B_plus_M_partreco, run_string = year, trigger_string = trigger, extra_string = fit_mode)
			else:
				B_plus_M_partreco = zfit.Data.from_numpy(obs=obs_extended, array=KstMC_arrays_i['all_%s_all'%trigger][0], weights=KstMC_arrays_i['all_%s_all'%trigger][1])
				prc_models[year+'_'+trigger+fit_mode] = build_prc_model(pars, obs, B_plus_M_partreco, run_string = year, trigger_string = trigger, extra_string = fit_mode)
			if save_shapes_and_parameters:
				prc_data_sets[year+'_'+trigger+fit_mode] = KstMC_arrays_i['all_%s_all'%trigger]

			# Building cascade PDFs
			if load_shapes_and_parameters: # Load quickly numpy arrays from number_storage
				B_plus_M_Cas = zfit.Data.from_numpy(obs=obs_extended, array=cas_data_sets[year+'_'+trigger+fit_mode][0], weights=cas_data_sets[year+'_'+trigger+fit_mode][1])
				cas_models[year+'_'+trigger+fit_mode] = build_cas_model(pars, obs, B_plus_M_Cas, run_string = year, trigger_string = trigger, extra_string = fit_mode)

			else:
				B_plus_M_Cas = zfit.Data.from_numpy(obs=obs_extended, array=CasMC_arrays_i['all_%s_all'%trigger][0], weights=CasMC_arrays_i['all_%s_all'%trigger][1])
				cas_models[year+'_'+trigger+fit_mode] = build_cas_model(pars, obs, B_plus_M_Cas, run_string = year, trigger_string = trigger, extra_string = fit_mode)
			if save_shapes_and_parameters:
				cas_data_sets[year+'_'+trigger+fit_mode] = CasMC_arrays_i['all_%s_all'%trigger]

			# Fit all the PDFs, just once - parameters will be shared between run periods later.
			if year_idx == 0 and not load_shapes_and_parameters:

				minimizer = zfit.minimize.Minuit(use_minuit_grad=True, minimize_strategy=2, verbosity=6)

				if trigger != 'all':
					# Fitting the signal PDFs
					# Create the Kee datasets...
					B_plus_M0 = zfit.Data.from_numpy(obs=obs, array=KeeMC_arrays_i['all_%s_0'%trigger][0], weights=KeeMC_arrays_i['all_%s_0'%trigger][1])
					B_plus_M1 = zfit.Data.from_numpy(obs=obs, array=KeeMC_arrays_i['all_%s_1'%trigger][0], weights=KeeMC_arrays_i['all_%s_1'%trigger][1])
					B_plus_M2 = zfit.Data.from_numpy(obs=obs, array=KeeMC_arrays_i['all_%s_2'%trigger][0], weights=KeeMC_arrays_i['all_%s_2'%trigger][1])
					B_plus_M = zfit.Data.from_numpy(obs=obs, array=KeeMC_arrays_i['all_%s_all'%trigger][0], weights=KeeMC_arrays_i['all_%s_all'%trigger][1])

					# Fitting the Kee models...
					nll0 = zfit.loss.UnbinnedNLL(sig_models0[year+'_'+trigger+fit_mode], B_plus_M0)
					result0 = minimizer.minimize(nll0)
					nll1 = zfit.loss.UnbinnedNLL(sig_models1[year+'_'+trigger+fit_mode], B_plus_M1)
					result1 = minimizer.minimize(nll1)
					nll2 = zfit.loss.UnbinnedNLL(sig_models2[year+'_'+trigger+fit_mode], B_plus_M2)
					result2 = minimizer.minimize(nll2)

					if makePlots and make_individual_PDF_plots: 
						# Plotting the models....
						plot_model(sig_models0[year+'_'+trigger+fit_mode], B_plus_M0, save_plot=True, fileName=plotdir+'plotKeePhot0_BDT_%s_'%trigger+str(foldernum)+'%s'%fit_mode)
						plot_model(sig_models1[year+'_'+trigger+fit_mode], B_plus_M1, save_plot=True, fileName=plotdir+'plotKeePhot1_BDT_%s_'%trigger+str(foldernum)+'%s'%fit_mode)
						plot_model(sig_models2[year+'_'+trigger+fit_mode], B_plus_M2, save_plot=True, fileName=plotdir+'plotKeePhot2_BDT_%s_'%trigger+str(foldernum)+'%s'%fit_mode)
						plot_model(sig_models[year+'_'+trigger+fit_mode], B_plus_M, save_plot=True, fileName=plotdir+'plotKeeTot_BDT_%s_'%trigger+str(foldernum)+'%s'%fit_mode)



				# Fitting the Combinatorial PDFs
				# Create the Combinatorial datasets...
				B_plus_M_data = zfit.Data.from_numpy(obs=obs_right_sideband, array=Combi_arrays_i['all_%s_all'%trigger])

				print('shape',np.shape(Combi_arrays_i['all_%s_all'%trigger]))

				# Fitting the Combinatorial models...
				with combi_models[year+'_'+trigger+fit_mode].set_norm_range(obs_right_sideband):

					if combi_option == 'ArgusExponential':
						sub_sample_string = '_%s_%s%s'%(year,trigger,fit_mode)
						if floatarguslambda == False:
							pars['combiargus_lam2%s'%(sub_sample_string)].floating = False
						pars['combiargus_scale%s'%(sub_sample_string)].floating = False

					nll_sidebands = zfit.loss.ExtendedUnbinnedNLL(combi_models[year+'_'+trigger+fit_mode], B_plus_M_data)
					minimizer.minimize(nll_sidebands)

					if combi_option == 'ArgusExponential':
						sub_sample_string = '_%s_%s%s'%(year,trigger,fit_mode)
						pars['combiargus_lam2%s'%(sub_sample_string)].floating = True
						pars['combiargus_scale%s'%(sub_sample_string)].floating = True

				# Plotting the models....
				if makePlots and make_individual_PDF_plots:
					plot_model_combi(combi_models[year+'_'+trigger+fit_mode],B_plus_M_data, save_plot=True, fileName=plotdir+'combi_sideband_BDT_%s_'%trigger+str(foldernum)+'%s'%fit_mode)
					plot_model_combi_nodata(combi_models[year+'_'+trigger+fit_mode], save_plot=True, fileName=plotdir+'combipdf_sideband_BDT_%s_'%trigger+str(foldernum)+'%s'%fit_mode, xmin=4880, xmax=6200)

				sub_sample_string = '_%s_%s%s'%(year,trigger,fit_mode)

				# Extrapolating the Combinatorial yield....
				if combi_option == 'Exponential':
					pars['estComb_%s%s'%(trigger,fit_mode)] = zfit.Parameter("estComb_%s%s"%(trigger,fit_mode), (pars['bkg_yield_combi_%s_%s%s'%(year,trigger,fit_mode)]/combi_models[year+'_'+trigger+fit_mode].integrate(limits=(5400,6200))).numpy()[0]*1., floating=False)
				elif combi_option == 'ArgusExponential':
					pars['estComb_%s%s'%(trigger,fit_mode)] = zfit.Parameter("estComb_%s%s"%(trigger,fit_mode), pars['bkg_yield_combi_%s_%s%s'%(year,trigger,fit_mode)].numpy()/combi_models[year+'_'+trigger+fit_mode].integrate(limits=(5400,6200)).numpy(), floating=False)

				sub_sample_string = '_%s_%s%s'%(year,trigger,fit_mode)

				# Create the Kpipi datasets...
				B_plus_M_kpipi = zfit.Data.from_numpy(obs=obs, array=KpipiMC_arrays_i['all_%s_all'%trigger][0], weights=KpipiMC_arrays_i['all_%s_all'%trigger][1])

				# Fitting the models....
				nll_kpipi = zfit.loss.UnbinnedNLL(kpipi_models[year+'_'+trigger+fit_mode], B_plus_M_kpipi)
				result_kpipi = minimizer.minimize(nll_kpipi)

				if makePlots and make_individual_PDF_plots:
					# Plotting the models....
					plot_model(kpipi_models[year+'_'+trigger+fit_mode], B_plus_M_kpipi, save_plot=True, fileName=plotdir+'plotKpipi_BDT_%s_'%trigger+str(foldernum)+'%s'%fit_mode)


				# Plotting the part-reco models....
				if makePlots and make_individual_PDF_plots:
					plot_model(prc_models[year+'_'+trigger+fit_mode], B_plus_M_partreco, save_plot=True, fileName=plotdir+'plotKstee_BDT_%s_'%trigger+str(foldernum)+'%s'%fit_mode)

				# Plotting the cascade models....
				if makePlots and make_individual_PDF_plots:
					plot_model(cas_models[year+'_'+trigger+fit_mode], B_plus_M_Cas, save_plot=True, fileName=plotdir+'plotCas_BDT_%s_'%trigger+str(foldernum)+'%s'%fit_mode)

####################################################################################################################################################
# Save the datasets which are required to create KDEs ...
####################################################################################################################################################

if save_shapes_and_parameters:
	with open('number_storage/cas_data_sets.pickle', 'wb') as handle:
		pickle.dump(cas_data_sets, handle, protocol=pickle.HIGHEST_PROTOCOL)
	with open('number_storage/prc_data_sets.pickle', 'wb') as handle:
		pickle.dump(prc_data_sets, handle, protocol=pickle.HIGHEST_PROTOCOL)


####################################################################################################################################################
# Build Kuu PDFs ...
####################################################################################################################################################

Kuu_models = {}
Kuucombi_models = {}

for year_idx, year in enumerate(year_list):
	for trigger in trigger_list_Kuu:
		for fit_mode in fit_modes:

			# Building Kuu PDFs
			Kuu_models[year+'_'+trigger+fit_mode] = build_kuu_signal_model(pars, obs_Kuu, run_string = year, trigger_string = trigger, extra_string = fit_mode)

			# Building Kuu Combinatorial PDFs
			Kuucombi_models[year+'_'+trigger+fit_mode] = build_kuucombi_model(pars, obs_Kuu, run_string = year, trigger_string = trigger, extra_string = fit_mode)
			Kuucombi_models[year+'_'+trigger+fit_mode].set_norm_range(obs_Kuu)
			if combi_option == 'Exponential':
				pars['lambda_Kuucombi_%s_%s%s'%(year,trigger,fit_mode)].set_value(pars['lambda_combi_%s_%s%s'%(year,trigger_list[0],fit_mode)].read_value().numpy())
			elif combi_option == 'ArgusExponential':
				pars['lambda_Kuucombi_%s_%s%s'%(year,trigger,fit_mode)].set_value(-1e-5)



####################################################################################################################################################
# Share parameters between years (as only fit 1 year in above code) ...
####################################################################################################################################################

# Share PDF parameters between run-periods.
for PDF_name in ['sig', 'combi', 'kpipi']:
	share_params(PDF_name,'2012', '2016') # copy param values from 2012 to 2016, if you didnt want to share the parameters you could minize each model to separate "B_plus_M0_run1"/"B_plus_M0_2016"/"B_plus_M0_2017" datasets.
	share_params(PDF_name,'2012', '2017')
	share_params(PDF_name,'2012', 'combined')




####################################################################################################################################################
# Fix all parameters except ...
####################################################################################################################################################

float_parameters = ['gamma','argus','lambda','yield']
for par in pars:
	if not any(x in pars[par].name for x in float_parameters):
		try:
			pars[par].floating = False
		except:
			pass

if not floatArgus:
	for par in pars:
		if 'combiargus_scale' in pars[par].name :
			pars[par].floating = False




####################################################################################################################################################
print("\n\n***********************************************")
print("Performing efficiency calculations to compute estimated yields..")
print("***********************************************")
####################################################################################################################################################


if not load_shapes_and_parameters:
	for trigger in trigger_list:
		for fit_mode in fit_modes:
			if trigger != ['all']:

				print('Computing yields for trigger category:',trigger)

				sub_sample_string = '%s%s'%(trigger,fit_mode)

				set_weight_names(fit_mode)

				effTotKee, effTotKee_err, countsTotKee, countsKeeerr =getEff(KeeMC, 'Kee', trigger)
				effTotKstee, effTotKstee_err, countsTotKstee, countsKsteeerr =getEff(KstMC,'K*ee', trigger)
				effTotCas1,effTotCas1_err, countsTotCas1, countsCas1err =getEff(Cas1MC,'Cas1', trigger)
				effTotCas2, effTotCas2_err, countsTotCas2, countsCas2err =getEff(Cas2MC,'Cas2', trigger)
				effTotCas3, effTotCas3_err, countsTotCas3, countsCas3err =getEff(Cas3MC,'Cas3', trigger)
				effTotCas12, effTotCas12_err, countsTotCas12, countsCas12err =getEff(Cas12MC,'Cas12', trigger)
				effTotCas13, effTotCas13_err, countsTotCas13, countsCas13err =getEff(Cas13MC,'Cas13', trigger)
				effTotKpipi, effTotKpipi_err, countsTotKpipi, countsKpipierr =getEff(KpipiMC,'Kpipi', trigger)

				if base_Kee_yield_off_2018_Kuu:
					Kmumu_2018_yield = 1459. #+- 46
					Kmumu_total_eff = 0.00564 #+- 0.00002
					Kee_geom_acc = 0.1765

					if trigger == 'eTOS':
						pars['value_for_corrections'].set_value(((effTotKee*Kee_geom_acc*effStrip)/Kmumu_total_eff))

					Kmumu_yields_vadym = np.asarray([483.,1125.,185.,1206.,1246.,1510.]) # including 2015, this was with q^2>14 absolute value doesnt matter, just fraction in 2018

					Kee_yield_2018 = Kmumu_2018_yield*((effTotKee*Kee_geom_acc*effStrip)/Kmumu_total_eff)
					Kee_yield_total = (1./RK)*Kee_yield_2018 * (np.sum(Kmumu_yields_vadym)/Kmumu_yields_vadym[-1])

					Kee_yield_2018_err = Kee_yield_2018*np.sqrt((0.00002/Kmumu_total_eff)**2+(46./Kmumu_2018_yield)**2)
					Kee_yield_total_err = (1./RK)*Kee_yield_2018_err * (np.sum(Kmumu_yields_vadym)/Kmumu_yields_vadym[-1])

					pars['Nkee_%s'%sub_sample_string] = zfit.Parameter("Nkee_%s"%(sub_sample_string), Kee_yield_total, floating=False)
					pars['Nkee_err_%s'%sub_sample_string] = zfit.Parameter("Nkee_err_%s"%(sub_sample_string), Kee_yield_total_err, floating=False)
				else:
					
					pars['Nkee_%s'%sub_sample_string] = zfit.Parameter("Nkee_%s"%(sub_sample_string), (NKee_bf_Run1*effStrip*0.66*effTotKee)+(NKee_bf_Run2*effStrip*effTotKee), floating=False)
					pars['Nkee_err_%s'%sub_sample_string] = zfit.Parameter("Nkee_err_%s"%(sub_sample_string), (NKee_bf_Run1*effStrip*0.66*effTotKee_err)+(NKee_bf_Run2*effStrip*effTotKee_err), floating=False)

				if fix_signal_yield: 
					err_estimation_ratio = fixed_signal_yield/pars['Nkee_%s'%sub_sample_string].read_value().numpy()
					pars['Nkee_%s'%sub_sample_string].set_value(fixed_signal_yield)
					pars['Nkee_err_%s'%sub_sample_string].set_value(pars['Nkee_err_%s'%sub_sample_string].read_value().numpy()*err_estimation_ratio)

				pars['Ncomb_%s'%sub_sample_string] = zfit.Parameter("Ncomb_%s"%(sub_sample_string), pars['estComb_%s'%sub_sample_string].read_value().numpy(), floating=False)
				pars['Ncomb_err_%s'%sub_sample_string] = zfit.Parameter("Ncomb_err_%s"%(sub_sample_string), np.sqrt(pars['Ncomb_%s'%sub_sample_string].read_value().numpy()), floating=False)


				# pars['NKstee_%s'%sub_sample_string] = zfit.Parameter("NKstee_%s"%(sub_sample_string), pars['Nkee_%s'%sub_sample_string].read_value().numpy()*(BFKstee*effTotKstee)/(BFKee*effTotKee), floating=False)
				# pars['NKstee_err_%s'%sub_sample_string] = zfit.Parameter("NKstee_err_%s"%(sub_sample_string), pars['NKstee_%s'%sub_sample_string].read_value().numpy()*np.sqrt((effTotKstee_err/effTotKstee)**2 + (effTotKee_err/effTotKee)**2), floating=False)
				# 
				pars['NKstee_%s'%sub_sample_string] = zfit.Parameter("NKstee_%s"%(sub_sample_string), pars['Nkee_%s'%sub_sample_string].read_value().numpy()*(BFKstee*countsTotKstee*DecProd['Kstee']/Nsim['Kstee'])/(BFKee*countsTotKee*DecProd['Kee']/Nsim['Kee']), floating=False)
				pars['NKstee_err_%s'%sub_sample_string] = zfit.Parameter("NKstee_err_%s"%(sub_sample_string), pars['NKstee_%s'%sub_sample_string].read_value().numpy()*np.sqrt((countsKsteeerr/countsTotKstee)**2 + (countsKeeerr/countsTotKee)**2), floating=False)

				pars['NKpipi_%s'%sub_sample_string] = zfit.Parameter("NKpipi_%s"%(sub_sample_string), pars['Nkee_%s'%sub_sample_string].read_value().numpy()*(BFMisID*effTotKpipi)/(BFKee*effTotKee), floating=False)
				pars['NKpipi_err_%s'%sub_sample_string] = zfit.Parameter("NKpipi_err_%s"%(sub_sample_string), pars['NKpipi_%s'%sub_sample_string].read_value().numpy()*np.sqrt((effTotKpipi_err/effTotKpipi)**2 + (effTotKee_err/effTotKee)**2), floating=False)

				pars['Ncas1_%s'%sub_sample_string] = zfit.Parameter("Ncas1_%s"%(sub_sample_string), pars['Nkee_%s'%sub_sample_string].read_value().numpy()*(BFcas1*countsTotCas1*DecProd['Cas1']/Nsim['Cas1'])/(BFKee*countsTotKee*DecProd['Kee']/Nsim['Kee']), floating=False)
				pars['Ncas2_%s'%sub_sample_string] = zfit.Parameter("Ncas2_%s"%(sub_sample_string), pars['Nkee_%s'%sub_sample_string].read_value().numpy()*(BFcas2*countsTotCas2*DecProd['Cas2']/Nsim['Cas2'])/(BFKee*countsTotKee*DecProd['Kee']/Nsim['Kee']), floating=False)
				pars['Ncas3_%s'%sub_sample_string] = zfit.Parameter("Ncas3_%s"%(sub_sample_string), pars['Nkee_%s'%sub_sample_string].read_value().numpy()*(BFcas3*countsTotCas3*DecProd['Cas3']/Nsim['Cas3'])/(BFKee*countsTotKee*DecProd['Kee']/Nsim['Kee']), floating=False)
				pars['Ncas12_%s'%sub_sample_string] = zfit.Parameter("Ncas12_%s"%(sub_sample_string), pars['Nkee_%s'%sub_sample_string].read_value().numpy()*(BFcas12*countsTotCas12*DecProd['Cas12']/Nsim['Cas12'])/(BFKee*countsTotKee*DecProd['Kee']/Nsim['Kee']), floating=False)
				pars['Ncas13_%s'%sub_sample_string] = zfit.Parameter("Ncas13_%s"%(sub_sample_string), pars['Nkee_%s'%sub_sample_string].read_value().numpy()*(BFcas13*countsTotCas13*DecProd['Cas13']/Nsim['Cas13'])/(BFKee*countsTotKee*DecProd['Kee']/Nsim['Kee']), floating=False)

				pars['Ncas1_err_%s'%sub_sample_string] = zfit.Parameter("Ncas1_err_%s"%(sub_sample_string), pars['Ncas1_%s'%sub_sample_string].read_value().numpy()*np.sqrt((countsCas1err/countsTotCas1)**2 + (countsKeeerr/countsTotKee)**2), floating=False)
				pars['Ncas2_err_%s'%sub_sample_string] = zfit.Parameter("Ncas2_err_%s"%(sub_sample_string), pars['Ncas2_%s'%sub_sample_string].read_value().numpy()*np.sqrt((countsCas2err/countsTotCas2)**2 + (countsKeeerr/countsTotKee)**2), floating=False)
				pars['Ncas3_err_%s'%sub_sample_string] = zfit.Parameter("Ncas3_err_%s"%(sub_sample_string), pars['Ncas3_%s'%sub_sample_string].read_value().numpy()*np.sqrt((countsCas3err/countsTotCas3)**2 + (countsKeeerr/countsTotKee)**2), floating=False)
				pars['Ncas12_err_%s'%sub_sample_string] = zfit.Parameter("Ncas12_err_%s"%(sub_sample_string), pars['Ncas12_%s'%sub_sample_string].read_value().numpy()*np.sqrt((countsCas12err/countsTotCas12)**2 + (countsKeeerr/countsTotKee)**2), floating=False)
				pars['Ncas13_err_%s'%sub_sample_string] = zfit.Parameter("Ncas13_err_%s"%(sub_sample_string), pars['Ncas13_%s'%sub_sample_string].read_value().numpy()*np.sqrt((countsCas13err/countsTotCas13)**2 + (countsKeeerr/countsTotKee)**2), floating=False)

				pars['NCas_%s'%sub_sample_string] =  zfit.Parameter("NCas_%s"%(sub_sample_string), pars['Ncas1_%s'%sub_sample_string].read_value().numpy() + pars['Ncas2_%s'%sub_sample_string].read_value().numpy() + pars['Ncas3_%s'%sub_sample_string].read_value().numpy(), floating=False)
				
				if np.isnan(pars['Ncas2_err_%s'%sub_sample_string].read_value().numpy()) == False:
					pars['NCas_err_%s'%sub_sample_string] =  zfit.Parameter("NCas_err_%s"%(sub_sample_string), np.sqrt(pars['Ncas1_err_%s'%sub_sample_string].read_value().numpy()**2+pars['Ncas2_err_%s'%sub_sample_string].read_value().numpy()**2+pars['Ncas3_err_%s'%sub_sample_string].read_value().numpy()**2+pars['Ncas12_err_%s'%sub_sample_string].read_value().numpy()**2+pars['Ncas13_err_%s'%sub_sample_string].read_value().numpy()**2), floating=False)
				else:
					pars['NCas_err_%s'%sub_sample_string] =  zfit.Parameter("NCas_err_%s"%(sub_sample_string), np.sqrt(pars['Ncas1_err_%s'%sub_sample_string].read_value().numpy()**2+pars['Ncas3_err_%s'%sub_sample_string].read_value().numpy()**2+pars['Ncas12_err_%s'%sub_sample_string].read_value().numpy()**2+pars['Ncas13_err_%s'%sub_sample_string].read_value().numpy()**2), floating=False)
				

				print("\n***********************************************")
				print("Yields:")
				print(" ")

				for component in ['Nkee', 'NKstee', 'NKpipi', 'Ncomb', 'Ncas1', 'Ncas2', 'Ncas3', 'Ncas12', 'Ncas13', 'NCas']:
					
					events_value = pars['%s_%s'%(component,sub_sample_string)].read_value().numpy()
					events_value_err = pars['%s_err_%s'%(component,sub_sample_string)].read_value().numpy()
					
					contamination_value = (events_value/pars['%s_%s'%('Nkee',sub_sample_string)].read_value().numpy())*100.

					print("%s: %.2f+-%.2f (%.2f%%)"%(component, events_value, events_value_err, contamination_value))

				print(" ")
				print("***********************************************\n\n")

else:
	for trigger in trigger_list:
		for fit_mode in fit_modes:

			sub_sample_string = '%s%s'%(trigger,fit_mode)

			if sub_sample_string != ['all']:
				pars['Nkee_%s'%sub_sample_string] = zfit.Parameter("Nkee_%s"%(sub_sample_string), 0., floating=False)
				pars['Ncomb_%s'%sub_sample_string] = zfit.Parameter("Ncomb_%s"%(sub_sample_string), 0., floating=False)
				pars['NKstee_%s'%sub_sample_string] = zfit.Parameter("NKstee_%s"%(sub_sample_string), 0., floating=False)
				pars['NKpipi_%s'%sub_sample_string] = zfit.Parameter("NKpipi_%s"%(sub_sample_string), 0., floating=False)
				pars['NCas_%s'%sub_sample_string] =  zfit.Parameter("NCas_%s"%(sub_sample_string), 0., floating=False)

# quit()


####################################################################################################################################################
# Saving/Loading parameters ...
####################################################################################################################################################

if save_shapes_and_parameters:
	save_pars = {}
	for par in pars:
		save_pars[pars[par].name] = pars[par].read_value().numpy()

	with open('number_storage/save_pars.pickle', 'wb') as handle:
		pickle.dump(save_pars, handle, protocol=pickle.HIGHEST_PROTOCOL)

if load_shapes_and_parameters:
	print("Loading values for all parameters...")
	save_pars = pickle.load( open( 'number_storage/save_pars.pickle', "rb" ) )
	for par in pars:
		try:
			pars[par].set_value(save_pars[pars[par].name])
		except:
			pass


####################################################################################################################################################
print("\nBoosting yields, if required...\n")
####################################################################################################################################################

yield_objects = ['Nkee', 'NKstee', 'NKpipi', 'NCas', 'Ncomb']
pars['Ntot_eTOS'] = zfit.Parameter("Ntot_eTOS", 0., floating=False)
pars['Ntot_TIS'] = zfit.Parameter("Ntot_TIS", 0., floating=False)
pars['Ntot_all'] = zfit.Parameter("Ntot_all", 0., floating=False)
for yield_object in yield_objects:
	pars['%s_all'%yield_object] = zfit.Parameter('%s_all'%yield_object, 0., floating=False)
	if 'eTOS' in trigger_list:
		pars['%s_eTOS'%yield_object].set_value(pars['%s_eTOS'%yield_object].read_value().numpy()*boost_eTOS)
		pars['%s_all'%yield_object].set_value(pars['%s_all'%yield_object].read_value().numpy()+pars['%s_eTOS'%yield_object].read_value().numpy())
		pars['Ntot_eTOS'].set_value(pars['Ntot_eTOS'].read_value().numpy()+pars['%s_eTOS'%yield_object].read_value().numpy())
		pars['Ntot_all'].set_value(pars['Ntot_all'].read_value().numpy()+pars['%s_eTOS'%yield_object].read_value().numpy())
	if 'TIS' in trigger_list:
		pars['%s_TIS'%yield_object].set_value(pars['%s_TIS'%yield_object].read_value().numpy()*boost_eTOS)
		pars['%s_all'%yield_object].set_value(pars['%s_all'%yield_object].read_value().numpy()+pars['%s_TIS'%yield_object].read_value().numpy())
		pars['Ntot_TIS'].set_value(pars['Ntot_TIS'].read_value().numpy()+pars['%s_TIS'%yield_object].read_value().numpy())
		pars['Ntot_all'].set_value(pars['Ntot_all'].read_value().numpy()+pars['%s_TIS'%yield_object].read_value().numpy())

pars['Kuu_frac_uTOS'] = zfit.Parameter('Kuu_frac_uTOS', (1/(1+combi_frac_in_kuu)), 0, 1)

######################################################################################################################################################################################################################################################################################################


####################################################################################################################################################
print("\nSetting up corrections (ratio of efficiencies), and organising Kuu and Kee yields from RK...\n")
####################################################################################################################################################

NKee_subsamples = {}
NKuu_subsamples = {}

for trigger in trigger_list_all:
	for year in year_list:  
		for fit_mode in fit_modes:
			sub_sample_string = '%s_%s%s'%(year,trigger,fit_mode)
			NKee_subsamples[trigger+'_'+year] = pars['Nkee_%s'%(trigger)].read_value().numpy()*expected_fracs_of_run_periods[year]
			if trigger == 'eTOS':
				pars['correction_%s'%(sub_sample_string)] = zfit.Parameter('correction_%s'%(sub_sample_string), pars['value_for_corrections'].read_value().numpy(), 0, 10)
			elif trigger == 'TIS':
				pars['correction_%s'%(sub_sample_string)] = zfit.Parameter('correction_%s'%(sub_sample_string), pars['value_for_corrections'].read_value().numpy()*(pars['Nkee_TIS'].read_value().numpy()/pars['Nkee_eTOS'].read_value().numpy()), 0, 10)
			elif trigger == 'all':
				pars['correction_%s'%(sub_sample_string)] = zfit.Parameter('correction_%s'%(sub_sample_string), pars['value_for_corrections'].read_value().numpy()+pars['value_for_corrections'].read_value().numpy()*(pars['Nkee_TIS'].read_value().numpy()/pars['Nkee_eTOS'].read_value().numpy()), 0, 10)

for trigger in ['eTOS']:
	for year in year_list:
		for fit_mode in fit_modes:
			sub_sample_string = '%s_%s%s'%(year,trigger,fit_mode)
			NKuu_subsamples['uTOS_'+year] = RK*NKee_subsamples[trigger+'_'+year]/pars['correction_%s'%(sub_sample_string)].read_value().numpy()


####################################################################################################################################################
# Make the PDFs extended ...
####################################################################################################################################################

print('Making PDFs extended...')
def sigYield_fn(RK_i, Nkuu_i, c):
	return (Nkuu_i*c)/RK_i

for year in year_list:
	for trigger in trigger_list_Kuu:
		for fit_mode in fit_modes:

			sub_sample_string = '_%s_%s%s'%(year,trigger,fit_mode)

			pars["kuuYield%s"%(sub_sample_string)] = zfit.Parameter("kuuYield%s"%(sub_sample_string), 100., 0, 1E6)
			Kuu_models[year+'_'+trigger+fit_mode] = Kuu_models[year+'_'+trigger+fit_mode].create_extended(pars["kuuYield%s"%(sub_sample_string)])

for year in year_list:
	for trigger in trigger_list_all:
		for fit_mode in fit_modes:

			sub_sample_string = '_%s_%s%s'%(year,trigger,fit_mode)

			if trigger != 'all':
				pars["sigYield_sig%s"%(sub_sample_string)] = zfit.ComposedParameter("sigYield_sig%s"%(sub_sample_string),sigYield_fn,params=[pars["RK"], pars["kuuYield_%s_uTOS%s"%(year,fit_mode)], pars['correction%s'%(sub_sample_string)]])
				sig_models[year+'_'+trigger+fit_mode] = sig_models[year+'_'+trigger+fit_mode].create_extended(pars["sigYield_sig%s"%(sub_sample_string)])

			pars["kpipiYield%s"%(sub_sample_string)] = zfit.Parameter("kpipiYield%s"%(sub_sample_string), 100., 0, 1E6)
			kpipi_models[year+'_'+trigger+fit_mode] = kpipi_models[year+'_'+trigger+fit_mode].create_extended(pars["kpipiYield%s"%(sub_sample_string)])

			pars["prcYield%s"%(sub_sample_string)] = zfit.Parameter("prcYield%s"%(sub_sample_string), 100., -1E6, 1E6)
			prc_models[year+'_'+trigger+fit_mode].set_yield(pars["prcYield%s"%(sub_sample_string)])

			pars["casYield%s"%(sub_sample_string)] = zfit.Parameter("casYield%s"%(sub_sample_string), 100., -1E6, 1E6)
			cas_models[year+'_'+trigger+fit_mode].set_yield(pars["casYield%s"%(sub_sample_string)])

####################################################################################################################################################
# Set RK to float ...
####################################################################################################################################################

if fit_BFKee == False:
	pars["RK"].floating = True
else:
	pars["BFKee"].floating = True





####################################################################################################################################################
# Seting yields ...
####################################################################################################################################################

print('Setting PDF yields (generation PDFs only)...')
for year in year_list:
	for trigger in trigger_list_Kuu:
		for fit_mode in fit_modes:

			sub_sample_string = '_%s_%s%s'%(year,trigger,fit_mode)

			pars["kuuYield%s"%(sub_sample_string)].set_value(NKuu_subsamples['uTOS_'+year])
			pars["bkg_yield_Kuucombi%s"%(sub_sample_string)].set_value(combi_frac_in_kuu*NKuu_subsamples['uTOS_'+year])

for year in year_list:
	for trigger in trigger_list_all:
		for fit_mode in fit_modes:

			sub_sample_string = '_%s_%s%s'%(year,trigger,fit_mode)

			pars["bkg_yield_combi%s"%(sub_sample_string)].set_value(expected_fracs_of_run_periods[year]*pars['Ncomb_%s'%trigger].read_value().numpy())
			if trigger == 'all':
				set_yield = 0.
				for trigger_i in trigger_list:
					if trigger_i != 'all':
						set_yield += pars['Ncomb_%s'%trigger_i].read_value().numpy()
				set_yield = expected_fracs_of_run_periods[year]*set_yield
				pars['bkg_yield_combi%s'%(sub_sample_string)].set_value(set_yield)

			pars["kpipiYield%s"%(sub_sample_string)].set_value(expected_fracs_of_run_periods[year]*pars['NKpipi_%s'%trigger].read_value().numpy())
			pars["prcYield%s"%(sub_sample_string)].set_value(expected_fracs_of_run_periods[year]*pars['NKstee_%s'%trigger].read_value().numpy())
			pars["casYield%s"%(sub_sample_string)].set_value(expected_fracs_of_run_periods[year]*pars['NCas_%s'%trigger].read_value().numpy())


####################################################################################################################################################
# Creating a signal mode for combined trigger categories, this only done because cannot simply sum them - will be too many brem fraction parameters  ...
####################################################################################################################################################

if 'eTOS' in trigger_list and 'TIS' in trigger_list:
	for fit_mode in fit_modes:

		#####################################
		print("\nCreate a combined signal model (eTOS + TIS) ...")
		#####################################

		NKee = {}
		NKee['eTOS'] = pars['Nkee_eTOS%s'%fit_mode].read_value().numpy()
		NKee['TIS'] = pars['Nkee_TIS%s'%fit_mode].read_value().numpy()

		for run_string in year_list:

			sub_sample_string = '_%s_%s%s'%(run_string,'all',fit_mode)

			
			frac = pars['f0g_eTOS%s'%fit_mode].read_value().numpy()*(NKee['eTOS']/(sum(NKee.values())))/(pars['f0g_eTOS%s'%fit_mode].read_value().numpy()*(NKee['eTOS']/(sum(NKee.values())))+pars['f0g_TIS%s'%fit_mode].read_value().numpy()*(NKee['TIS']/(sum(NKee.values()))))
			sig_models0_eTOS_TIS = zfit.pdf.SumPDF([sig_models0[run_string+'_eTOS'], sig_models0[run_string+'_TIS']], [frac])

			frac = pars['f1g_eTOS%s'%fit_mode].read_value().numpy()*(NKee['eTOS']/(sum(NKee.values())))/(pars['f1g_eTOS%s'%fit_mode].read_value().numpy()*(NKee['eTOS']/(sum(NKee.values())))+pars['f1g_TIS%s'%fit_mode].read_value().numpy()*(NKee['TIS']/(sum(NKee.values()))))
			sig_models1_eTOS_TIS = zfit.pdf.SumPDF([sig_models1[run_string+'_eTOS'], sig_models1[run_string+'_TIS']], [frac])

			frac = ((1-pars['f0g_eTOS%s'%fit_mode].read_value().numpy()-pars['f1g_eTOS%s'%fit_mode].read_value().numpy())*(NKee['eTOS']/(sum(NKee.values()))))/(((1-pars['f0g_eTOS%s'%fit_mode].read_value().numpy()-pars['f1g_eTOS%s'%fit_mode].read_value().numpy())*(NKee['eTOS']/(sum(NKee.values())))) + ((1-pars['f0g_TIS%s'%fit_mode].read_value().numpy()-pars['f1g_TIS%s'%fit_mode].read_value().numpy())*(NKee['TIS']/(sum(NKee.values())))))
			sig_models2_eTOS_TIS = zfit.pdf.SumPDF([sig_models2[run_string+'_eTOS'], sig_models2[run_string+'_TIS']], [frac])


			f0g_all = pars['f0g_eTOS%s'%fit_mode].read_value().numpy()*(NKee['eTOS']/(sum(NKee.values()))) + pars['f0g_TIS%s'%fit_mode].read_value().numpy()*(NKee['TIS']/(sum(NKee.values())))
			f1g_all = pars['f1g_eTOS%s'%fit_mode].read_value().numpy()*(NKee['eTOS']/(sum(NKee.values()))) + pars['f1g_TIS%s'%fit_mode].read_value().numpy()*(NKee['TIS']/(sum(NKee.values())))

			trigger_string = 'all'

			pars["frac0gamma_sig%s"%(sub_sample_string)] = zfit.Parameter("frac0gamma_sig%s"%(sub_sample_string), f0g_all, 0, 1, floating=True)
			pars["frac1_COMPOUND_gamma_sig%s"%(sub_sample_string)] = zfit.Parameter("frac1_COMPOUND_gamma_sig%s"%(sub_sample_string), f1g_all/(1-f0g_all), 0, 1, floating=True)
			pars["frac1gamma_sig%s"%(sub_sample_string)] =  zfit.ComposedParameter("frac1gamma_sig%s"%(sub_sample_string),compound_frac_fn,params=[pars["frac0gamma_sig%s"%(sub_sample_string)], pars["frac1_COMPOUND_gamma_sig%s"%(sub_sample_string)]])

			modles = [sig_models0_eTOS_TIS, sig_models1_eTOS_TIS, sig_models2_eTOS_TIS]
			fracs = [pars["frac0gamma_sig%s"%(sub_sample_string)], pars["frac1gamma_sig%s"%(sub_sample_string)]]
			sig_models[run_string+'_all'+fit_mode] = zfit.pdf.SumPDF(modles, fracs)

			pars["sigYield_sig%s"%(sub_sample_string)] = zfit.ComposedParameter("sigYield_sig%s"%(sub_sample_string),sigYield_fn,params=[pars["RK"], pars["kuuYield_%s_uTOS%s"%(run_string,fit_mode)], pars['correction%s'%(sub_sample_string)]])

			sig_models[run_string+'_'+trigger_string+fit_mode]=sig_models[run_string+'_'+trigger_string+fit_mode].create_extended(pars["sigYield_sig%s"%(sub_sample_string)])


######################################################################################################################################################################################################################################################################################################
print("\n***********************************************")
print("Combining models for each fit configuration...")
print("***********************************************\n")
######################################################################################################################################################################################################################################################################################################

if custom_cuts['mklcut']:
	modelTot_labels = ['Kee', "Combi", "prc", 'Kpipi']
else:
	modelTot_labels = ['Kee', "Combi", "prc", 'Kpipi', 'Cascasde']
modelTot_labels_kuu = ['Kuu',"Combi"]

total_models_for_fit_dict = {}
total_Kuumodels_for_fit_dict = {}

for fit_mode in fit_modes:

	for years_option in ['separate','combined']:

		# Construst different electron fit models for different fit options
		for trigger_option in trigger_options_to_fit_loop:
			this_dict = {}

			years_to_loop = ['combined']
			if years_option == 'separate': 
				years_to_loop = ['2012','2016','2017']
			triggers_to_loop = trigger_list
			if trigger_option == 'combined': 
				triggers_to_loop = ['all']
			if trigger_option == 'eTOS':
				triggers_to_loop = ['eTOS']

			for year in years_to_loop:
				for trigger in triggers_to_loop:
					if custom_cuts['mklcut']:
						this_dict[year+'_'+trigger] = zfit.pdf.SumPDF([sig_models[year+'_'+trigger+fit_mode], combi_models[year+'_'+trigger+fit_mode], prc_models[year+'_'+trigger+fit_mode], kpipi_models[year+'_'+trigger+fit_mode]])
					else:
						this_dict[year+'_'+trigger] = zfit.pdf.SumPDF([sig_models[year+'_'+trigger+fit_mode], combi_models[year+'_'+trigger+fit_mode], prc_models[year+'_'+trigger+fit_mode], kpipi_models[year+'_'+trigger+fit_mode], cas_models[year+'_'+trigger+fit_mode]])
			total_models_for_fit_dict["%s_%s%s"%(years_option,trigger_option,fit_mode)] = this_dict

		# Construst different muon fit models for different fit options
		this_dict = {}

		years_to_loop = ['combined']
		if years_option == 'separate': 
			years_to_loop = ['2012','2016','2017']

		for year in years_to_loop:
			for trigger in trigger_list_Kuu:
				this_dict[year+'_'+trigger] = zfit.pdf.SumPDF([Kuu_models[year+'_'+trigger+fit_mode], Kuucombi_models[year+'_'+trigger+fit_mode]])
		total_Kuumodels_for_fit_dict["%s%s"%(years_option,fit_mode)] = this_dict



######################################################################################################################################################################################################################################################################################################
print("\n***********************************************")
print("Save initial parameter values...")
print("***********************************************\n")
######################################################################################################################################################################################################################################################################################################

initial_params_dict = {}
list_of_floating_params_dict = {}

for fit_mode in fit_modes:

	for years_option in ['separate','combined']:

		for trigger_option in trigger_options_to_fit_loop:

			initial_params_dict_i = {}
			list_of_floating_params_dict_i = {}

			total_models_for_fit_dict_i = total_models_for_fit_dict["%s_%s%s"%(years_option,trigger_option,fit_mode)]
			for model in total_models_for_fit_dict_i:
				list_of_params = total_models_for_fit_dict_i[model].get_params()
				for param in list_of_params:
					initial_params_dict_i[param.name] = param.read_value().numpy()
					if param.floating: 
						if fixcascades:
							fix_parameters = ['casYield','correction','gamma','kpipi']
							if not any(x in param.name for x in fix_parameters):
								list_of_floating_params_dict_i[param.name] = param.read_value()
						elif fit_BFKee:
							fix_parameters = ['gamma','kpipi','kuu','Kuu']
							if not any(x in param.name for x in fix_parameters):
								list_of_floating_params_dict_i[param.name] = param.read_value()
						elif fix_the_corrections:
							fix_parameters = ['correction','gamma','kpipi']
							if not any(x in param.name for x in fix_parameters):
								list_of_floating_params_dict_i[param.name] = param.read_value()
						else:
							list_of_floating_params_dict_i[param.name] = param.read_value()

			total_models_for_fit_dict_i = total_Kuumodels_for_fit_dict["%s"%(years_option)]
			for model in total_models_for_fit_dict_i:
				list_of_params = total_models_for_fit_dict_i[model].get_params()
				for param in list_of_params:
					initial_params_dict_i[param.name] = param.read_value().numpy()
					if param.floating: 
						if fixcascades:
							fix_parameters = ['casYield','correction','gamma','kpipi']
							if not any(x in param.name for x in fix_parameters):
								list_of_floating_params_dict_i[param.name] = param.read_value()
						elif fit_BFKee:
							fix_parameters = ['gamma','kpipi','kuu','Kuu']
							if not any(x in param.name for x in fix_parameters):
								list_of_floating_params_dict_i[param.name] = param.read_value()
						elif fix_the_corrections:
							fix_parameters = ['correction','gamma','kpipi']
							if not any(x in param.name for x in fix_parameters):
								list_of_floating_params_dict_i[param.name] = param.read_value()
						else:
							list_of_floating_params_dict_i[param.name] = param.read_value()

			initial_params_dict["%s_%s%s"%(years_option,trigger_option,fit_mode)] = initial_params_dict_i
			list_of_floating_params_dict["%s_%s%s"%(years_option,trigger_option,fit_mode)] = list_of_floating_params_dict_i


######################################################################################################################################################################################################################################################################################################
print("\n***********************************************")
print("Setting up constraints...")
print("***********************************************\n")
######################################################################################################################################################################################################################################################################################################

def initialise_constraints(parameter_name, err_frac, pearsons_in, constraint_names, fit_mode):

	list_of_parameters_to_constrain = []

	parameter_observations_numpy = np.empty(0)
	parameter_err = np.empty(0)
	for trigger in trigger_list_all:
		for year in ['combined','2012','2016','2017']:

			err_frac_i = err_frac

			run_string = year
			try:
				list_of_parameters_to_constrain.append(pars["%s_%s_%s%s"%(parameter_name,run_string,trigger,fit_mode)])
				obs_value_i = pars["%s_%s_%s%s"%(parameter_name,run_string,trigger,fit_mode)].read_value().numpy()
			except:
				try:
					list_of_parameters_to_constrain.append(pars["%s%s_%s%s"%(parameter_name,run_string,trigger,fit_mode)])
					obs_value_i = pars["%s%s_%s%s"%(parameter_name,run_string,trigger,fit_mode)].read_value().numpy()
				except:
					try:
						list_of_parameters_to_constrain.append(pars["%s%s_%s%s"%(parameter_name,'',trigger,fit_mode)])
						obs_value_i = pars["%s%s_%s%s"%(parameter_name,'',trigger,fit_mode)].read_value().numpy()
					except:
						list_of_parameters_to_constrain.append(pars["%s_%s_%s%s"%(parameter_name,trigger,run_string,fit_mode)])
						obs_value_i = pars["%s_%s_%s%s"%(parameter_name,trigger,run_string,fit_mode)].read_value().numpy()

			parameter_observations_numpy = np.append(parameter_observations_numpy, obs_value_i)
			parameter_err = np.append(parameter_err, obs_value_i*err_frac_i)

	parameter_err_matrix = np.diag(parameter_err**2) # parameter_err is a list of the stds, parameter_err**2 is a list of the variance.
	for iidx in range(0, len(list_of_parameters_to_constrain)):
		for jidx in range(iidx+1, len(list_of_parameters_to_constrain)):
			if iidx != jidx:
				# Add some correlations
				parameter_err_matrix[iidx][jidx] = (np.sqrt(parameter_err_matrix[iidx][iidx])*np.sqrt(parameter_err_matrix[jidx][jidx]))*pearsons_in # np.sqrt(parameter_err_matrix[jidx][jidx]) = std_jj, using r^2 = cov(X,Y)^2/(var(X)*var(Y)) where var(X) = std_X^2
				parameter_err_matrix[jidx][iidx] = parameter_err_matrix[iidx][jidx]
	observations = []
	for idx, observation in enumerate(list_of_parameters_to_constrain):
		pars["%s_%s"%(observation.name,constraint_names)] = zfit.Parameter("%s_%s"%(observation.name,constraint_names), 0., floating=False)
		observations.append(pars["%s_%s"%(observation.name,constraint_names)])
	for idx, observation in enumerate(list_of_parameters_to_constrain):
		pars["%s_%s"%(observation.name,constraint_names)].set_value(parameter_observations_numpy[idx])

	constraint_out = zfit.constraint.GaussianConstraint(list_of_parameters_to_constrain, observation=observations, uncertainty=parameter_err_matrix)

	return constraint_out, [list_of_parameters_to_constrain, parameter_observations_numpy, parameter_err_matrix, constraint_names]


def blur_constraint(constraint_info):
	list_of_pars = constraint_info[0]	
	observations_i = constraint_info[1]
	err_matrix_i = constraint_info[2]
	par_string = constraint_info[3]
	blur = np.random.multivariate_normal(observations_i, err_matrix_i)
	for idx_i, observation_i in enumerate(list_of_pars):
		pars["%s_%s"%(observation_i.name,par_string)].set_value(blur[idx_i])

def blur_constraints_together(constraint_info, constraint_info_alt):
	list_of_pars = constraint_info[0]	
	observations_i = constraint_info[1]
	err_matrix_i = constraint_info[2]
	par_string = constraint_info[3]
	blur = np.random.multivariate_normal(observations_i, err_matrix_i)

	for idx_i, observation_i in enumerate(list_of_pars):
		pars["%s_%s"%(observation_i.name,par_string)].set_value(blur[idx_i])
		print("%s_%s"%(observation_i.name,par_string), blur[idx_i])

	list_of_pars = constraint_info_alt[0]	
	observations_i = constraint_info_alt[1]
	err_matrix_i = constraint_info_alt[2]
	par_string = constraint_info_alt[3]

	for idx_i, observation_i in enumerate(list_of_pars):
		pars["%s_%s"%(observation_i.name,par_string)].set_value(blur[idx_i])
		print("%s_%s"%(observation_i.name,par_string), blur[idx_i])

corrections_constraint, corrections_constraint_info = initialise_constraints(parameter_name='correction', err_frac=0.05, pearsons_in=0.8, constraint_names='corr',fit_mode='')
frac0_constraint, frac0_constraint_info = initialise_constraints(parameter_name='frac0gamma_sig', err_frac=0.01, pearsons_in=0., constraint_names='gamma0',fit_mode='')
frac1_constraint, frac1_constraint_info = initialise_constraints(parameter_name='frac1gamma_sig', err_frac=0.01, pearsons_in=0., constraint_names='gamma1',fit_mode='')
# prc_constraint, prc_constraint_info = initialise_constraints(parameter_name='prcYield', err_frac=0.25, pearsons_in=0., constraint_names='prcconstraint',fit_mode='')
# cas_constraint, cas_constraint_info = initialise_constraints(parameter_name='casYield', err_frac=0.25, pearsons_in=0., constraint_names='casconstraint',fit_mode='')
kpipi_constraint, kpipi_constraint_info = initialise_constraints(parameter_name='kpipiYield', err_frac=0.025, pearsons_in=0., constraint_names='kpipiconstraint',fit_mode='')
if alt_fit:
	corrections_constraint_alt, corrections_constraint_info_alt = initialise_constraints(parameter_name='correction', err_frac=frac_error_for_corrections, pearsons_in=0.8, constraint_names='corr_alt',fit_mode='_alt')
	frac0_constraint_alt, frac0_constraint_info_alt = initialise_constraints(parameter_name='frac0gamma_sig', err_frac=0.01, pearsons_in=0., constraint_names='gamma0_alt',fit_mode='_alt')
	frac1_constraint_alt, frac1_constraint_info_alt = initialise_constraints(parameter_name='frac1gamma_sig', err_frac=0.01, pearsons_in=0., constraint_names='gamma1_alt',fit_mode='_alt')
	# prc_constraint_alt, prc_constraint_info_alt = initialise_constraints(parameter_name='prcYield', err_frac=0.25, pearsons_in=0., constraint_names='prcconstraint_alt',fit_mode='_alt')
	# cas_constraint_alt, cas_constraint_info_alt = initialise_constraints(parameter_name='casYield', err_frac=0.25, pearsons_in=0., constraint_names='casconstraint_alt',fit_mode='_alt')
	kpipi_constraint_alt, kpipi_constraint_info_alt = initialise_constraints(parameter_name='kpipiYield', err_frac=0.025, pearsons_in=0., constraint_names='kpipiconstraint_alt',fit_mode='_alt')



####################################################################################################################################################
# If in batch mode want to generate toy data for multiple toys at once - much faster ...
####################################################################################################################################################
if batch:
	years_option = 'separate'
	trigger_option = 'separate'
	if len(trigger_list) == 1: trigger_option = 'eTOS'
	total_models_for_fit = total_models_for_fit_dict["%s_%s"%(years_option,trigger_option)]
	total_Kuumodels_for_fit = total_Kuumodels_for_fit_dict["%s"%(years_option)]

	batch_yields = {}
	batch_generate = {}

	print('Generating Kee and K\u03BC\u03BC toy data for %d toys at once (faster than individually)'%nToys)

	for year in ['2012','2016','2017']:
		for trigger in trigger_list:
			model_names = ['Kee', 'Combi', 'Prc', 'Kpipi', 'Cas']
			for model_idx, model_i in enumerate([sig_models, combi_models, prc_models, kpipi_models, cas_models]):
				for nToy in range(nToys):
					try:
						batch_yields[year+'_'+trigger+'_'+model_names[model_idx]] = np.append(batch_yields[year+'_'+trigger+'_'+model_names[model_idx]], np.random.poisson(model_i[year+'_'+trigger].get_yield().numpy()))
					except:
						batch_yields[year+'_'+trigger+'_'+model_names[model_idx]] = np.asarray([np.random.poisson(model_i[year+'_'+trigger].get_yield().numpy())])
				nGenerate = np.sum(batch_yields[year+'_'+trigger+'_'+model_names[model_idx]])
				generated = model_i[year+'_'+trigger].sample(n=nGenerate).numpy()
				labels = np.empty(0)
				for idx in range(nToys):
					labels = np.append(labels, idx*np.ones(batch_yields[year+'_'+trigger+'_'+model_names[model_idx]][idx]))
				batch_generate[year+'_'+trigger+'_'+model_names[model_idx]] = np.concatenate((generated,np.expand_dims(labels,1)),axis=1)

	batch_yields_Kuu = {}
	batch_generate_Kuu = {}

	for year in ['2012','2016','2017']:
		for trigger in trigger_list_Kuu:
			model_names = ['Kuu', 'KuuCombi']
			for model_idx, model_i in enumerate([Kuu_models, Kuucombi_models]):
				for nToy in range(nToys):
					try:
						batch_yields_Kuu[year+'_'+trigger+'_'+model_names[model_idx]] = np.append(batch_yields_Kuu[year+'_'+trigger+'_'+model_names[model_idx]], np.random.poisson(model_i[year+'_'+trigger].get_yield().numpy()))
					except:
						batch_yields_Kuu[year+'_'+trigger+'_'+model_names[model_idx]] = np.asarray([np.random.poisson(model_i[year+'_'+trigger].get_yield().numpy())])
				nGenerate = np.sum(batch_yields_Kuu[year+'_'+trigger+'_'+model_names[model_idx]])
				generated = model_i[year+'_'+trigger].sample(n=nGenerate).numpy()
				labels = np.empty(0)
				for idx in range(nToys):
					labels = np.append(labels, idx*np.ones(batch_yields_Kuu[year+'_'+trigger+'_'+model_names[model_idx]][idx]))
				batch_generate_Kuu[year+'_'+trigger+'_'+model_names[model_idx]] = np.concatenate((generated,np.expand_dims(labels,1)),axis=1)




####################################################################################################################################################
# Begin toy loop ...
####################################################################################################################################################


fit_results_all_fits = {}

list_of_toy_times = np.empty(0)

for toy_i in range(nToys):

	fit_results_all_fits['toy_%d'%toy_i] = {}

	t0 = time.time()

	print("\n\n***********************************************")
	print("Running toy number %d..."%toy_i)
	print("***********************************************")


	if batch:

		####################################################################################################################################################
		# Unpack correct data for this toy ...
		####################################################################################################################################################

		print("\nSelecting Kee and K\u03BC\u03BC toy data sets for toy number %d...\n"%toy_i)

		data_objects = {}
		for year in ['2012','2016','2017']:
			for trigger in trigger_list:
				model_names = ['Kee', 'Combi', 'Prc', 'Kpipi', 'Cas']
				data_ee_numpy = np.empty(0)
				for model_name in model_names:
					data_ee_numpy = np.append(data_ee_numpy, batch_generate[year+'_'+trigger+'_'+model_name][np.where(batch_generate[year+'_'+trigger+'_'+model_name][:,1]==toy_i)][:,0])
				data_ee = zfit.data.Data.from_numpy(obs, data_ee_numpy)
				data_objects[year+'_'+trigger] = data_ee

		data_objects_kuu = {}
		for year in ['2012','2016','2017']:
			for trigger in trigger_list_Kuu:
				model_names = ['Kuu', 'KuuCombi']
				data_uu_numpy = np.empty(0)
				for model_name in model_names:
					data_uu_numpy = np.append(data_uu_numpy, batch_generate_Kuu[year+'_'+trigger+'_'+model_name][np.where(batch_generate_Kuu[year+'_'+trigger+'_'+model_name][:,1]==toy_i)][:,0])
				data_uu = zfit.data.Data.from_numpy(obs, data_uu_numpy)
				data_objects_kuu[year+'_'+trigger] = data_uu

				
	else:

		####################################################################################################################################################
		# Generate data for a single year ...
		####################################################################################################################################################

		print("\nGenerating Kee and K\u03BC\u03BC toy data sets for toy number %d...\n"%toy_i)

		years_option = 'separate'
		trigger_option = 'separate'
		if len(trigger_list) == 1: trigger_option = 'eTOS'
		total_models_for_fit = total_models_for_fit_dict["%s_%s"%(years_option,trigger_option)]
		total_Kuumodels_for_fit = total_Kuumodels_for_fit_dict["%s"%(years_option)]

		# If batch mode generate events for all toys at once, then split up after. For speed.
		data_objects = {}
		for year in ['2012','2016','2017']:
			for trigger in trigger_list:

				with combi_models[year+'_'+trigger].set_norm_range([lower_limit_of_mass_window,6200]):
					print('{:<45s}{:>10s}{:<8s}{:>10s}{:<8s}{:>10s}{:<8s}{:>10s}{:<8s}{:>10s}{:<8s}'.format('Generating ee events for sub-sample %s %s'%(year,trigger), 'Kee:',' %.2f'%sig_models[year+'_'+trigger].get_yield().numpy()
						, 'Comb:',' %.2f'%combi_models[year+'_'+trigger].get_yield().numpy(), 'K*ee:',' %.2f'%prc_models[year+'_'+trigger].get_yield().numpy(), 'Kpipi:',' %.2f'%kpipi_models[year+'_'+trigger].get_yield().numpy()
						, 'Cas:',' %.2f'%cas_models[year+'_'+trigger].get_yield().numpy()))

					signal_n = np.random.poisson(sig_models[year+'_'+trigger].get_yield().numpy())
					combi_n = np.random.poisson(combi_models[year+'_'+trigger].get_yield().numpy())
					prc_n = np.random.poisson(prc_models[year+'_'+trigger].get_yield().numpy())
					kpipi_n = np.random.poisson(kpipi_models[year+'_'+trigger].get_yield().numpy())
					cas_n = np.random.poisson(cas_models[year+'_'+trigger].get_yield().numpy())

					data_ee_numpy = np.concatenate((sig_models[year+'_'+trigger].sample(n=signal_n).numpy(),
													combi_models[year+'_'+trigger].sample(n=combi_n).numpy(),
													prc_models[year+'_'+trigger].sample(n=prc_n).numpy(),
													kpipi_models[year+'_'+trigger].sample(n=kpipi_n).numpy(),
													cas_models[year+'_'+trigger].sample(n=cas_n).numpy()),axis=0)

					data_ee = zfit.data.Data.from_numpy(obs, data_ee_numpy)

					data_objects[year+'_'+trigger] = data_ee

		data_objects_kuu = {}
		for year in ['2012','2016','2017']:
			for trigger in trigger_list_Kuu:
				with Kuucombi_models[year+'_'+trigger].set_norm_range([lower_limit_of_mass_window_kuu,5600]):				
					print('{:<45s}{:>10s}{:<8s}{:>10s}{:<8s}'.format('Generating \u03BC\u03BC events for sub-sample %s %s'%(year,trigger), 'K\u03BC\u03BC:',' %.2f'%Kuu_models[year+'_'+trigger].get_yield().numpy()
						, 'Comb:',' %.2f'%Kuucombi_models[year+'_'+trigger].get_yield().numpy()))

					signal_n = np.random.poisson(Kuu_models[year+'_'+trigger].get_yield().numpy())
					bkg_n = np.random.poisson(Kuucombi_models[year+'_'+trigger].get_yield().numpy())

					data_uu_numpy = np.concatenate((Kuu_models[year+'_'+trigger].sample(n=signal_n).numpy(),Kuucombi_models[year+'_'+trigger].sample(n=bkg_n).numpy()),axis=0)

					data_uu = zfit.data.Data.from_numpy(obs_Kuu, data_uu_numpy)

					data_objects_kuu[year+'_'+trigger] = data_uu


	####################################################################################################################################################
	# Blur the constraints ...
	print("\nBlurring constraints before fitting toy %d..."%toy_i)
	####################################################################################################################################################

	blur_constraint(corrections_constraint_info)
	blur_constraint(frac0_constraint_info)
	blur_constraint(frac1_constraint_info)
	# blur_constraint(prc_constraint_info)
	# blur_constraint(cas_constraint_info)
	blur_constraint(kpipi_constraint_info)

	if alt_fit:
		blur_constraints_together(corrections_constraint_info, corrections_constraint_info_alt)
		blur_constraints_together(frac0_constraint_info, frac0_constraint_info_alt)
		blur_constraints_together(frac1_constraint_info, frac1_constraint_info_alt)
		# blur_constraints_together(prc_constraint_info, prc_constraint_info_alt)
		# blur_constraints_together(cas_constraint_info, cas_constraint_info_alt)
		blur_constraints_together(kpipi_constraint_info, kpipi_constraint_info_alt)






	####################################################################################################################################################
	# Loop over the various fitting options, split run periods, combine run periods etc ...
	####################################################################################################################################################

	for trigger_option in trigger_options_to_fit:
		for years_option in years_options_to_fit:

	
			if fit_uu_mode and not fit_ee_mode and trigger_option == 'separate':
				continue


			print('\n\nRunning a fit to toy %d with %s run periods and %s triggers.'%(toy_i,years_option,trigger_option))


			####################################################################################################################################################
			# Get the correct models ...
			####################################################################################################################################################

			total_models_for_fit = total_models_for_fit_dict["%s_%s"%(years_option,trigger_option)]
			total_Kuumodels_for_fit = total_Kuumodels_for_fit_dict["%s"%(years_option)]
			list_of_floating_params = list_of_floating_params_dict["%s_%s"%(years_option,trigger_option)]

			if alt_fit:
				total_models_for_fit_alt = total_models_for_fit_dict["%s_%s_alt"%(years_option,trigger_option)]
				total_Kuumodels_for_fit_alt = total_Kuumodels_for_fit_dict["%s_alt"%(years_option)]
				list_of_floating_params_alt = list_of_floating_params_dict["%s_%s_alt"%(years_option,trigger_option)]


			####################################################################################################################################################
			# Retrive initial parameter values ...
			####################################################################################################################################################

			initial_params = initial_params_dict["%s_%s"%(years_option,trigger_option)]



			####################################################################################################################################################
			# Organise required data sub-samples, combine where necessary...
			####################################################################################################################################################

			year_list_fit_option = ['combined']
			if years_option == 'separate':
				year_list_fit_option = ['2012','2016','2017']
			trigger_list_fit_option = ['combined']
			if trigger_option == 'separate':
				trigger_list_fit_option = trigger_list
			if trigger_option == 'eTOS':
				trigger_list_fit_option = ['eTOS']

			data_objects_for_fit = {}
			data_objects_kuu_for_fit = {}
			data_objects_keys = list(data_objects.keys())

			for year in year_list_fit_option:
				for trigger in trigger_list_fit_option:
					data_objects_keys_i = data_objects_keys
					if year == 'combined' and trigger != 'combined':
						data_objects_keys_i = [s for s in data_objects_keys_i if trigger in s]
					if year != 'combined' and trigger == 'combined':
						data_objects_keys_i = [s for s in data_objects_keys_i if year in s]
					if year != 'combined' and trigger != 'combined':
						data_objects_keys_i = [s for s in data_objects_keys_i if year in s]
						data_objects_keys_i = [s for s in data_objects_keys_i if trigger in s]
					data_i = np.empty((0,1))
					for key in data_objects_keys_i:
						data_i = np.concatenate((data_i, data_objects[key].numpy()),axis=0)
					data_objects_for_fit[year+'_'+trigger] = zfit.data.Data.from_numpy(obs=obs,array=data_i)

			data_objects_keys = list(data_objects_kuu.keys())

			trigger = 'uTOS'
			for year in year_list_fit_option:
				data_objects_keys_i = data_objects_keys
				if year != 'combined':
					data_objects_keys_i = [s for s in data_objects_keys_i if year in s]
					data_objects_keys_i = [s for s in data_objects_keys_i if trigger in s]
				data_i = np.empty((0,1))
				for key in data_objects_keys_i:
					data_i = np.concatenate((data_i, data_objects_kuu[key].numpy()),axis=0)
				data_objects_kuu_for_fit[year+'_'+trigger] = zfit.data.Data.from_numpy(obs=obs_Kuu,array=data_i)

			







			####################################################################################################################################################
			# Organise constraints ...
			####################################################################################################################################################

			list_of_constraints_employ = []
			list_of_constraints_employ_alt = []
			if add_constraints:
				print('Adding constraints...')
				list_of_constraints_employ.append(frac0_constraint)
				list_of_constraints_employ.append(frac1_constraint)
				# list_of_constraints_employ.append(prc_constraint)
				# list_of_constraints_employ.append(cas_constraint)
				list_of_constraints_employ.append(kpipi_constraint)
				if not fix_the_corrections:
					list_of_constraints_employ.append(corrections_constraint)

				if alt_fit:
					list_of_constraints_employ_alt.append(frac0_constraint_alt)
					list_of_constraints_employ_alt.append(frac1_constraint_alt)
					# list_of_constraints_employ_alt.append(prc_constraint_alt)
					# list_of_constraints_employ_alt.append(cas_constraint_alt)
					list_of_constraints_employ_alt.append(kpipi_constraint_alt)
					if not fix_the_corrections:
						list_of_constraints_employ_alt.append(corrections_constraint_alt)


			####################################################################################################################################################
			# If you want to plot the PDFs before the fit ...
			####################################################################################################################################################

			if toy_i == 0 and makePlots: 
				print("\nPlotting Toy...")
				if len(year_list_fit_option) > 1 or len(trigger_list_fit_option) > 1:
					for year_idx, year in enumerate(year_list_fit_option):
						for trigger in trigger_list_fit_option:
							trigger2 = trigger
							if trigger == 'combined': 
								trigger2 = 'all'
							plot_comp_model(total_models_for_fit[year+'_'+trigger2],data_objects_for_fit[year+'_'+trigger], save_plot=True, fileName=plotdir+'%s_%s_plotTotalToy_BDT'%(years_option,trigger_option)+str(foldernum)+'_toy'+str(toy_i)+'_%s_%s'%(year,trigger), labels=modelTot_labels)
				plot_comp_model_combine_separate_run_periods(total_models_for_fit,data_objects_for_fit, year_list_fit_option, trigger_list_fit_option, save_plot=True, fileName=plotdir+'%s_%s_plotTotalToy_BDT'%(years_option,trigger_option)+str(foldernum)+'_toy'+str(toy_i), labels=modelTot_labels, xlabel=r'$m(Kee)$ (MeV)')
				if len(year_list_fit_option) > 1 or len(trigger_list_Kuu) > 1:
					for year_idx, year in enumerate(year_list_fit_option):
						for trigger in trigger_list_Kuu:
							trigger2 = trigger
							if trigger == 'combined': 
								trigger2 = 'all'
							plot_comp_model(total_Kuumodels_for_fit[year+'_'+trigger2],data_objects_kuu_for_fit[year+'_'+trigger], save_plot=True, fileName=plotdir+'%s_%s_plotTotalToyKuu_BDT'%(years_option,trigger_option)+str(foldernum)+'_toy'+str(toy_i)+'_%s_%s'%(year,trigger), labels=modelTot_labels_kuu)
				plot_comp_model_combine_separate_run_periods(total_Kuumodels_for_fit,data_objects_kuu_for_fit, year_list_fit_option, trigger_list_Kuu, save_plot=True, fileName=plotdir+'%s_%s_plotTotalToyKuu_BDT'%(years_option,trigger_option)+str(foldernum)+'_toy'+str(toy_i), labels=modelTot_labels_kuu, xlabel=r'$m(K\mu\mu)$ (MeV)')



			####################################################################################################################################################
			# Create NLL and fit dataset ...
			####################################################################################################################################################

			print("Preparing NLL for toy number: %d"%toy_i)

			print('\nFloating parameters:',list(list_of_floating_params.keys()))
			print('\nNumber of floating parameters:', len(list_of_floating_params),'\n')
			# ensure other parameters are not floating...
			for par in list(pars.keys()):
				try:
					if pars[par].name not in list_of_floating_params.keys() and pars[par].floating == True:
						pars[par].floating = False
					elif pars[par].name in list_of_floating_params.keys():
						pars[par].floating = True
				except:
					pass

			models_ee = []
			datas_ee = []
			models_uu = []
			datas_uu = []

			if fit_ee_mode:
				for year_idx, year in enumerate(year_list_fit_option):
					for trigger in trigger_list_fit_option:
						trigger2 = trigger
						if trigger == 'combined': 
							trigger2 = 'all'
						print('Adding Kee sub-sample:',year,trigger)
						models_ee.append(total_models_for_fit[year+'_'+trigger2])
						datas_ee.append(data_objects_for_fit[year+'_'+trigger])

			if fit_uu_mode:
				for year_idx, year in enumerate(year_list_fit_option):
					for trigger in trigger_list_Kuu:
						trigger2 = trigger
						if trigger == 'combined': 
							trigger2 = 'all'
						print('Adding Kuu sub-sample:',year,trigger)
						models_uu.append(total_Kuumodels_for_fit[year+'_'+trigger2])
						datas_uu.append(data_objects_kuu_for_fit[year+'_'+trigger])

			nll_simultaneous = zfit.loss.ExtendedUnbinnedNLL(models_ee, datas_ee, constraints=list_of_constraints_employ) + zfit.loss.ExtendedUnbinnedNLL(models_uu, datas_uu) # make sure not to add constrains more than once!
			
			print('Begin minimize of standard fit')
			minimizer = zfit.minimize.Minuit(use_minuit_grad=True, minimize_strategy=2, verbosity=6)
			result = minimizer.minimize(nll_simultaneous)

			nll_simultaneous = 0 # clear from memory? unsure if this actually helps

			print("\nDone! getting the results...\n")

			print(result.params)

			correlations = result.correlation()

			if alt_fit:
				# CREATING NLL AND FITTING THE DATASETS
				print("Preparing NLL for toy number: %d"%toy_i)

				print('\nFloating parameters:',list(list_of_floating_params_alt.keys()))
				print('\nNumber of floating parameters:', len(list_of_floating_params_alt),'\n')
				# ensure other parameters are not floating...
				for par in list(pars.keys()):
					try:
						if pars[par].name not in list_of_floating_params_alt.keys() and pars[par].floating == True:
							pars[par].floating = False
						elif pars[par].name in list_of_floating_params_alt.keys():
							pars[par].floating = True
					except:
						pass

				models_ee = []
				datas_ee = []
				models_uu = []
				datas_uu = []

				if fit_ee_mode:
					for year_idx, year in enumerate(year_list_fit_option):
						for trigger in trigger_list_fit_option:
							trigger2 = trigger
							if trigger == 'combined': 
								trigger2 = 'all'
							print('Adding Kee sub-sample:',year,trigger)
							models_ee.append(total_models_for_fit_alt[year+'_'+trigger2])
							datas_ee.append(data_objects_for_fit[year+'_'+trigger])

				if fit_uu_mode:
					for year_idx, year in enumerate(year_list_fit_option):
						for trigger in trigger_list_Kuu:
							trigger2 = trigger
							if trigger == 'combined': 
								trigger2 = 'all'
							print('Adding Kuu sub-sample:',year,trigger)
							models_uu.append(total_Kuumodels_for_fit_alt[year+'_'+trigger2])
							datas_uu.append(data_objects_kuu_for_fit[year+'_'+trigger])

				nll_simultaneous = zfit.loss.ExtendedUnbinnedNLL(models_ee, datas_ee, constraints=list_of_constraints_employ_alt) + zfit.loss.ExtendedUnbinnedNLL(models_uu, datas_uu) # make sure not to add constrains more than once!

				print('Begin minimize of alternative fit')
				minimizer = zfit.minimize.Minuit(use_minuit_grad=True, minimize_strategy=2, verbosity=6)
				result_alt = minimizer.minimize(nll_simultaneous)

				nll_simultaneous = 0 # clear from memory? unsure if this actually helps

				print("\nDone! getting the results...\n")

				print(result_alt.params)

				correlations_alt = result_alt.correlation()



			####################################################################################################################################################
			# Plot fit result ...
			####################################################################################################################################################

			if toy_i == 0 and makePlots: 

				print("\nPlotting fit results...")

				plt.figure(figsize=(10,8))
				ax = plt.subplot(1,1,1)
				plt.imshow(correlations,vmin=-1., vmax=1.)
				plt.xticks(np.linspace(0,len(list_of_floating_params)-1,len(list_of_floating_params)), list_of_floating_params, rotation=90, fontsize=10)
				plt.yticks(np.linspace(0,len(list_of_floating_params)-1,len(list_of_floating_params)), list_of_floating_params, fontsize=10)
				ax.tick_params(axis=u'both', which=u'both',length=0)
				plt.colorbar()
				plt.savefig(plotdir+'%s_%s_correlations_'%(years_option,trigger_option)+str(foldernum)+'_toy'+str(toy_i)+'.pdf')
				plt.savefig(plotdir+'%s_%s_correlations_'%(years_option,trigger_option)+str(foldernum)+'_toy'+str(toy_i)+'.png')
				plt.close('all')


				if len(year_list_fit_option) > 1 or len(trigger_list_fit_option) > 1:
					for year_idx, year in enumerate(year_list_fit_option):
						for trigger in trigger_list_fit_option:
							trigger2 = trigger
							if trigger == 'combined': 
								trigger2 = 'all'
							plot_comp_model(total_models_for_fit[year+'_'+trigger2],data_objects_for_fit[year+'_'+trigger], save_plot=True, fileName=plotdir+'%s_%s_plotTotalFit_BDT'%(years_option,trigger_option)+str(foldernum)+'_toy'+str(toy_i)+'_%s_%s'%(year,trigger), labels=modelTot_labels)
				plot_comp_model_combine_separate_run_periods(total_models_for_fit,data_objects_for_fit, year_list_fit_option, trigger_list_fit_option, save_plot=True, fileName=plotdir+'%s_%s_plotTotalFit_BDT'%(years_option,trigger_option)+str(foldernum)+'_toy'+str(toy_i), labels=modelTot_labels, xlabel=r'$m(Kee)$ (MeV)')
				if len(year_list_fit_option) > 1 or len(trigger_list_Kuu) > 1:
					for year_idx, year in enumerate(year_list_fit_option):
						for trigger in trigger_list_Kuu:
							trigger2 = trigger
							if trigger == 'combined': 
								trigger2 = 'all'
							plot_comp_model(total_Kuumodels_for_fit[year+'_'+trigger2],data_objects_kuu_for_fit[year+'_'+trigger], save_plot=True, fileName=plotdir+'%s_%s_plotTotalFitKuu_BDT'%(years_option,trigger_option)+str(foldernum)+'_toy'+str(toy_i)+'_%s_%s'%(year,trigger), labels=modelTot_labels_kuu)
				plot_comp_model_combine_separate_run_periods(total_Kuumodels_for_fit,data_objects_kuu_for_fit, year_list_fit_option, trigger_list_Kuu, save_plot=True, fileName=plotdir+'%s_%s_plotTotalFitKuu_BDT'%(years_option,trigger_option)+str(foldernum)+'_toy'+str(toy_i), labels=modelTot_labels_kuu, xlabel=r'$m(K\mu\mu)$ (MeV)')







			####################################################################################################################################################
			# Print pulls and save results ...
			####################################################################################################################################################

			print("\n***********************************************")
			print("Pulls: toy number: %d"%toy_i)
			fitResults={}
			for item in result.hesse().items():
				if item[0].name in list_of_floating_params.keys():
					fittedVal=item[0].value().numpy()
					fittedError=item[1]['error']
					genVal=list_of_floating_params[item[0].name]

					pull = (fittedVal-genVal)/fittedError
					if np.abs(pull) > 3.: print('{:<36s}{:>15s}{:>15s}{:<15s}{:>15s}'.format(item[0].name,'(!!!)  '+str(np.around(pull, decimals=3)),str(np.around(fittedVal, decimals=4))+'+-',str(np.around(fittedError, decimals=4)),str(np.around(genVal, decimals=4))))
					else: print('{:<36s}{:>15s}{:>15s}{:<15s}{:>15s}'.format(item[0].name,str(np.around(pull, decimals=3)),str(np.around(fittedVal, decimals=4))+'+-',str(np.around(fittedError, decimals=4)),str(np.around(genVal, decimals=4))))

					fitResults[item[0].name]=fittedVal
					fitResults[item[0].name+'_err']=fittedError
					fitResults[item[0].name+'_pull']=pull

			fitResults["converged"] = result.converged
			fitResults["valid"] = result.valid

			print('result.converged:',result.converged)
			print('result.valid:',result.valid)

			if fit_BFKee == False:
				RK_asym_errors = result.errors(params=[pars["RK"]])
				fitResults["RK_lower"] = RK_asym_errors[0][pars["RK"]]['lower']
				fitResults["RK_upper"] = RK_asym_errors[0][pars["RK"]]['upper']
				print('RK minos errors: %.6f, %.6f'%(RK_asym_errors[0][pars["RK"]]['lower'], RK_asym_errors[0][pars["RK"]]['upper']))

				print("***********************************************")
				print('\n\nRK fit: %.6f+-%.6f (pull of %.6f) \n\n'%(fitResults['RK'],fitResults['RK_err'],fitResults['RK_pull']))

			fitResults["Correlation_matrix"] = correlations
			fitResults["list_of_floating_params"] = list(list_of_floating_params.keys())

			
			print("***********************************************\n")

			if alt_fit:
				print("\n***********************************************")
				print("Pulls alt: toy number: %d"%toy_i)
				fitResults_alt={}
				for item in result_alt.hesse().items():
					if item[0].name in list_of_floating_params.keys():
						fittedVal=item[0].value().numpy()
						fittedError=item[1]['error']
						genVal=list_of_floating_params[item[0].name]

						pull = (fittedVal-genVal)/fittedError
						if np.abs(pull) > 3.: print('{:<36s}{:>15s}{:>15s}{:<15s}{:>15s}'.format(item[0].name,'(!!!)  '+str(np.around(pull, decimals=3)),str(np.around(fittedVal, decimals=4))+'+-',str(np.around(fittedError, decimals=4)),str(np.around(genVal, decimals=4))))
						else: print('{:<36s}{:>15s}{:>15s}{:<15s}{:>15s}'.format(item[0].name,str(np.around(pull, decimals=3)),str(np.around(fittedVal, decimals=4))+'+-',str(np.around(fittedError, decimals=4)),str(np.around(genVal, decimals=4))))

						fitResults_alt[item[0].name]=fittedVal
						fitResults_alt[item[0].name+'_err']=fittedError
						fitResults_alt[item[0].name+'_pull']=pull

				fitResults_alt["converged"] = result.converged
				fitResults_alt["valid"] = result.valid

				print('result.converged:',result.converged)
				print('result.valid:',result.valid)

				if fit_BFKee == False:
					RK_asym_errors = result.errors(params=[pars["RK"]])
					fitResults_alt["RK_lower"] = RK_asym_errors[0][pars["RK"]]['lower']
					fitResults_alt["RK_upper"] = RK_asym_errors[0][pars["RK"]]['upper']

					print('RK minos errors: %.6f, %.6f'%(RK_asym_errors[0][pars["RK"]]['lower'], RK_asym_errors[0][pars["RK"]]['upper']))

					print("***********************************************")
					print('\n\nRK fit alt: %.6f+-%.6f (pull of %.6f) \n\n'%(fitResults_alt['RK'],fitResults_alt['RK_err'],fitResults_alt['RK_pull']))
					print("***********************************************\n")
				fitResults_alt["Correlation_matrix"] = correlations_alt
				fitResults_alt["list_of_floating_params"] = list(list_of_floating_params.keys())



			
			fit_results_all_fits['toy_%d'%toy_i]['%s_%s'%(years_option,trigger_option)] = fitResults
			if alt_fit:
				fit_results_all_fits['toy_%d'%toy_i]['%s_%s_alt'%(years_option,trigger_option)] = fitResults_alt

			save_to_file = plotdir+'fit_results_'+str(foldernum)+'process'+str(processID)+'.pickle'
			with open(save_to_file, 'wb') as handle:
				save_objects = [custom_cuts, fit_results_all_fits]
				pickle.dump(save_objects, handle, protocol=pickle.HIGHEST_PROTOCOL)




			####################################################################################################################################################
			# Return parameters to their initial values ...
			####################################################################################################################################################

			print('Resetting parameter values...')
			for par in list_of_floating_params:
				pars[par].set_value(initial_params[par])



			# Attempt to remove objects to avoid script being killed, unsure if this helps.
			fitResults = 0
			list_of_nll = 0
			nll_simultaneous = 0
			result = 0
			list_of_constraints_employ = 0
			total_models_for_fit = 0
			total_Kuumodels_for_fit = 0
			initial_params = 0
			list_of_floating_params = 0
			data_objects_for_fit = 0
			data_objects_kuu_for_fit = 0
			minimizer = 0





	t1 = time.time()
	list_of_toy_times = np.append(list_of_toy_times, t1-t0)

	print('Mean toy time: %.2f +- %.2f seconds'%(np.mean(list_of_toy_times), np.std(list_of_toy_times)/np.sqrt(np.float64(np.shape(list_of_toy_times)[0]))))





