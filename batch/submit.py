import os
import sys
from datetime import datetime

# espresso     = 20 minutes
# microcentury = 1 hour
# longlunch    = 2 hours
# workday      = 8 hours
# tomorrow     = 1 day
# testmatch    = 3 days
# nextweek     = 1 week

# Must have sourced python, and tarred the package


def submit(job_name, save_directory_base, numToys_per_job, numJobs, saveLOG, jobflavour, additional_options):
	###################################################################
	pwd = os.getcwd()

	# Setting up saving directory
	save_directory = save_directory_base+job_name
	try:
		os.mkdir(save_directory)
	except:
		pass
	print('\nOutput saved to',save_directory)

	# Setting up submission directory
	now = datetime.now()
	dt_string = now.strftime("%Hhr_%Mmin_%Ssec_%d_%m_%Y")
	submisson_directory = 'files_for_submission/'+job_name+'_'+dt_string
	try:
		os.mkdir(submisson_directory)
	except:
		print("submisson_directory %s already exists..."%(job_name+'_'+dt_string))
		quit()
	print('\nSubmisson location:',submisson_directory)


	# Organising options
	default_RKtoy_options = '--dir 1 --saveLocation $(pwd) --processID "$id" --nToys %d --makePlots false --batch true'%numToys_per_job
	default_condor_options = 'id=$(Process) outputFolder=\'%s\''%job_name


	additional_RKtoy_options = ''
	additional_condor_options = ''

	for key in list(additional_options.keys()):
		print('Adding option:', key, additional_options[key])
		additional_RKtoy_options += ' --%s %s'%(key, additional_options[key])


	# Creating run file from template
	f = open('templates/run', 'r')
	f_lines = f.readlines()
	with open('%s/run'%submisson_directory, 'a') as f_out:
		for idx, line in enumerate(f_lines):
			if 'PYTHONOPTIONS' in line:
				line = line.replace('PYTHONOPTIONS', default_RKtoy_options+additional_RKtoy_options)
			if 'SAVEDIR' in line:
				line = line.replace('SAVEDIR', save_directory_base)
			f_out.write(line)


	# Creating submit file from template
	f = open('templates/submit.job', 'r')
	f_lines = f.readlines()
	with open('%s/submit.job'%submisson_directory, 'a') as f_out:
		for idx, line in enumerate(f_lines):
			if 'CONDOROPTIONS' in line:
				line = line.replace('CONDOROPTIONS', default_condor_options+additional_condor_options)
			if 'NUMJOBS' in line:
				line = line.replace('NUMJOBS', str(numJobs))
			if not saveLOG:
				if 'job =' in line:
					continue
				if 'LOG/' in line:
					continue
			if 'JOBFLAVOUR' in line:
				line = line.replace('JOBFLAVOUR', jobflavour)
			f_out.write(line)

	# Creating LOG folder, if required
	if saveLOG:
		os.mkdir(submisson_directory+'/LOG')

	# Submitting jobs
	print("\n\nSubmitting %d jobs with %d toys per job from %s...\n"%(numJobs,numToys_per_job,job_name+'_'+dt_string))
	os.chdir(submisson_directory)
	os.system('condor_submit submit.job')
	os.chdir(pwd)
	print("\nDONE\n")


idx = 0
for floatArgus in ['true','false']:
	for usemklcut in ['true','false']:
		for floatarguslambda in ['true','false']:
			idx +=1
			additional_options = {}

			# Options...
			job_name = 'KeeYieldLARGE_floatArgus_%s_mklcut_%s_floatlambda_%s'%(floatArgus,usemklcut,floatarguslambda)
			save_directory_base = '/eos/lhcb/user/m/marshall/RK/zfit/RK_jobs/'
			numToys_per_job = 20
			numJobs = 125
			if idx == 1:
				saveLOG = True
			else:
				saveLOG = False
			jobflavour = 'workday'

			# Define additional options here...

			additional_options['floatArgus'] = floatArgus
			additional_options['usemklcut'] = usemklcut
			additional_options['floatarguslambda'] = floatarguslambda

			# additional_options['fixcascades'] = 'false'

			# additional_options['base_Kee_yield_off_2018_Kuu'] = 'true'
			# additional_options['fit_BFKee'] = 'true'

			if floatArgus == 'true' and usemklcut == 'true' and floatarguslambda == 'false': # Nominal
				additional_options['splityears'] = 'true'

			submit(job_name, save_directory_base, numToys_per_job, numJobs, saveLOG, jobflavour, additional_options)



additional_options = {}

# Options...
job_name = 'KeeYieldLARGE_floatArgus_true_mklcut_false_floatlambda_false_fixCascades_true'
save_directory_base = '/eos/lhcb/user/m/marshall/RK/zfit/RK_jobs/'
numToys_per_job = 20
numJobs = 125
saveLOG = False
jobflavour = 'workday'

# Define additional options here...

additional_options['floatArgus'] = 'true'
additional_options['usemklcut'] = 'false'
additional_options['floatarguslambda'] = 'false'
additional_options['fixcascades'] = 'true'

submit(job_name, save_directory_base, numToys_per_job, numJobs, saveLOG, jobflavour, additional_options)

# quit()

# additional_options = {}

# # Options...
# job_name = 'KeeYieldLARGE_floatArgus_true_mklcut_true_floatlambda_false'
# save_directory_base = '/eos/lhcb/user/m/marshall/RK/zfit/RK_jobs/'
# numToys_per_job = 10
# numJobs = 250
# saveLOG = False
# jobflavour = 'workday'

# # Define additional options here...

# additional_options['floatArgus'] = 'true'
# additional_options['usemklcut'] = 'true'
# additional_options['floatarguslambda'] = 'false'

# submit(job_name, save_directory_base, numToys_per_job, numJobs, saveLOG, jobflavour, additional_options)



additional_options = {}

# Options...
job_name = 'KeeYieldLARGE_floatArgus_true_mklcut_true_floatlambda_false_RK_0846'
save_directory_base = '/eos/lhcb/user/m/marshall/RK/zfit/RK_jobs/'
numToys_per_job = 20
numJobs = 125
saveLOG = False
jobflavour = 'workday'

# Define additional options here...

additional_options['floatArgus'] = 'true'
additional_options['usemklcut'] = 'true'
additional_options['floatarguslambda'] = 'false'
additional_options['RK'] = '0.846'
# additional_options['boost_stats'] = '10.'

additional_options['splityears'] = 'true'

submit(job_name, save_directory_base, numToys_per_job, numJobs, saveLOG, jobflavour, additional_options)




additional_options = {}

# Options...
job_name = 'KeeYieldLARGE_floatArgus_true_mklcut_true_floatlambda_false_RK_0846_BOOST'
save_directory_base = '/eos/lhcb/user/m/marshall/RK/zfit/RK_jobs/'
numToys_per_job = 20
numJobs = 125
saveLOG = False
jobflavour = 'workday'

# Define additional options here...

additional_options['floatArgus'] = 'true'
additional_options['usemklcut'] = 'true'
additional_options['floatarguslambda'] = 'false'
additional_options['RK'] = '0.846'
additional_options['boost_stats'] = '10.'

additional_options['splityears'] = 'true'

submit(job_name, save_directory_base, numToys_per_job, numJobs, saveLOG, jobflavour, additional_options)












