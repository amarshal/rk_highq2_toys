# RELIC

CODE MOVED TO RK HIGHQ2 PACKAGE [HERE](https://gitlab.cern.ch/LHCb-RD/ewp-rk-rkst-highq2).

# RK toys for high-q2 

* Default options are defined in `utils/config.py`
* Options can be defined and changed in `mainConfig.txt`
* Finally, options can be parsed. These become master options.
```
python RK_toy.py --dir 7 --use_trigger_categories 0,2
```

## Structure of the toy code

* PDFs are constructed in `utils/PDFs.py`
* Selection string are defined in `utils/getPreselection.py`
* Plotting functions for the toys are in `utils/plotFunctions.py`
* Functions to compute efficiencies, to organise MC arrays and other miscellaneous functions are in `utils/utilities.py`
* Some definitions are provided in `utils/definitions.py`, this file is somewhat redundant

## Other functionality

* There is a script to submit to a condor batch system, `batch/submit.py`. Within this can provide various options to each job. 
* There is then a script to plot output from set of toys, `tools/getPulls.py`.
* Note that the toys output `pickle` files containing all the toy results, pulls, correlation matrices, convergence success. For plotting these are organised with `tools/utils/loader.py`.

<img src="./example/combined_combined_plotTotalFit_BDT100_toy0.png" width=50% height=50%><img src="./example/combined_combined_plotTotalFitKuu_BDT100_toy0.png" width=50% height=50%>

*This code was developed from a starting point written by [Davide Lancierini](https://gitlab.cern.ch/dlancier/rk-highq2-sensitivity-studies/-/blob/master/sensitivity_studies_v1.py).*

*Code uses the [zfit](https://zfit.readthedocs.io/en/latest/) fitting software.*
