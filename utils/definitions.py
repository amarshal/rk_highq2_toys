from utils.config import * # Import all configurations 

trigger_cat_names = ['eTOS', 'hTOS', 'TIS']

# if noTIS:
#     print('\tNot using TIS, fitting trigger cats (only eTOS) together.')
#     use_trigger_categories = [0] # Default exclude hTOS
#     trigger_cat_names = ['eTOS']
#     triggers = 'combined'
