
# /*******************************************
#  * Functions that return ROOT cut strings. *
#  *******************************************/
# #include "getPreselection.h"

# //trigger cuts


# /********************************************
# PID Calib Stuff
# *********************************************/


def getPIDCalibEPTagCut(): return "(e_plus_P > 6000 && e_plus_PT > 1500 && e_plus_IPCHI2_OWNPV > 9 && e_plus_PIDe>5)";
def getPIDCalibEMTagCut(): return "(e_minus_P > 6000 && e_minus_PT > 1500 && e_minus_IPCHI2_OWNPV > 9 && e_minus_PIDe>5)";
def getPIDCalibEPProbeCut(): return "e_plus_P > 3000 && e_plus_PT > 500 && e_plus_IPCHI2_OWNPV > 9";
def getPIDCalibEMProbeCut(): return "e_minus_P > 3000 && e_minus_PT > 500 && e_minus_IPCHI2_OWNPV > 9";



def getPIDCalibPresel():

	cuts = "(B_plus_IPCHI2_OWNPV < 9 && (B_plus_ENDVERTEX_CHI2/B_plus_ENDVERTEX_NDOF) < 9 && e_plus_IPCHI2_OWNPV > 25 && e_minus_IPCHI2_OWNPV > 25 && J_psi_1S_M > 2450 && J_psi_1S_M < 3600 && K_Kst_ProbNNk>0.2";
	cuts += " && ( "+getPIDCalibEPTagCut()+" || "+getPIDCalibEMTagCut()+" ))";
	return cuts;

def getPIDCalibPresel2017():

	cuts = "(nSPDHits < 450 && e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3 && B_plus_M02_Subst0_e2pi>1885 && B_plus_M02>1885";
	cuts += " && B_plus_IPCHI2_OWNPV < 9 && (B_plus_ENDVERTEX_CHI2/B_plus_ENDVERTEX_NDOF) < 9 && J_psi_1S_M > 2450 && J_psi_1S_M < 3600 && K_Kst_MC15TuneV1_ProbNNk>0.2";
	# // differences in 2017
	cuts += " && e_plus_IPCHI2_OWNPV>25 && e_minus_IPCHI2_OWNPV>25 && (J_psi_1S_ENDVERTEX_CHI2/J_psi_1S_ENDVERTEX_NDOF) < 9";
	cuts += " && K_Kst_PIDK > 5 && K_Kst_PT>1000 && K_Kst_P>3000 && K_Kst_IPCHI2_OWNPV>9 && K_Kst_TRACK_GhostProb<0.3 && K_Kst_TRACK_CHI2NDOF<3 "; 
	cuts += " && abs(B_plus_M-5279)<1000";
	cuts += " && ( "+getPIDCalibEPTagCut()+" || "+getPIDCalibEMTagCut()+" ))";

	return cuts;


def getPIDCalibPresel2016():

	cuts = "(nSPDHits < 450 && e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3 && B_plus_M02_Subst0_e2pi>1885 && B_plus_M02>1885";
	cuts += " && B_plus_IPCHI2_OWNPV < 9 && (B_plus_ENDVERTEX_CHI2/B_plus_ENDVERTEX_NDOF) < 9 && J_psi_1S_M > 2450 && J_psi_1S_M < 3600 && K_Kst_MC15TuneV1_ProbNNk>0.2";
	cuts += " && ( "+getPIDCalibEPTagCut()+" || "+getPIDCalibEMTagCut()+" ))";

	return cuts;


def getPIDCalibPresel2015():

	cuts = "(nSPDHits < 450 && e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3";
	cuts += " && B_plus_IPCHI2_OWNPV < 9 && (B_plus_ENDVERTEX_CHI2/B_plus_ENDVERTEX_NDOF) < 9 && J_psi_1S_M > 2450 && J_psi_1S_M < 3600 && K_Kst_PIDK>15)";

	return cuts;


# /*******************************************
#  * Run1 CutStrings *
#  *******************************************/



# /*******************************************
#  * EE offline cuts
#  *******************************************/

def getCommonPresel():

	cuts = "";
	cuts="(K_Kst_TRACK_GhostProb<0.3 ";
	cuts+="&& e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3 ";
	cuts+="&& K_Kst_ProbNNk>0.2 && K_Kst_PIDe<0 ";
	cuts+="&& e_plus_PIDe>3 && e_minus_PIDe>3 ";
	cuts+="&& e_plus_hasCalo == 1 && e_plus_hasRich == 1 ";
	cuts+="&& K_Kst_hasRich == 1 ";
	cuts+="&& e_minus_hasCalo == 1 && e_minus_hasRich == 1 ";
	cuts+="&& (TMath::Abs(e_minus_L0Calo_ECAL_xProjection)>363.6 || TMath::Abs(e_minus_L0Calo_ECAL_yProjection)>282.6)";# //trigger fiducial
	cuts+="&& (TMath::Abs(e_plus_L0Calo_ECAL_xProjection)>363.6 || TMath::Abs(e_plus_L0Calo_ECAL_yProjection)>282.6)";
	cuts+="&& nSPDHits < 600";
	cuts+="&& e_plus_PT > 500 && e_minus_PT > 500 && e_plus_P > 3000 && e_minus_P > 3000"; #//PID fiducial
	cuts+=")";
	return cuts;



def getCommonPreselNoPidCut():

	cuts="";
	cuts="(K_Kst_TRACK_GhostProb<0.3 ";
	cuts+="&& e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3 ";
	cuts+="&& e_plus_hasCalo == 1 && e_plus_hasRich == 1 ";
	cuts+="&& K_Kst_hasRich == 1 ";
	cuts+="&& e_minus_hasCalo == 1 && e_minus_hasRich == 1 ";
	cuts+="&& (TMath::Abs(e_minus_L0Calo_ECAL_xProjection)>363.6 || TMath::Abs(e_minus_L0Calo_ECAL_yProjection)>282.6)"; #//trigger fiducial
	cuts+="&& (TMath::Abs(e_plus_L0Calo_ECAL_xProjection)>363.6 || TMath::Abs(e_plus_L0Calo_ECAL_yProjection)>282.6)";
	cuts+="&& nSPDHits < 600";
	cuts+="&& e_plus_PT > 500 && e_minus_PT > 500 && e_plus_P > 3000 && e_minus_P > 3000"; #//PID fiducial
	cuts+=")";
	return cuts;


def getCascadeVetoes():

	cuts= "(Kemu_MKl > 1885 && (Kemu_TRACK_MKl_l2pi < 1825 || Kemu_TRACK_MKl_l2pi > 1905))"

	return cuts

def getCascadeVetoesMuMu():

	cuts= "(Kemu_MKl_l2pi>1885 && B_plus_M02>1885)"

	return cuts



def getJPsiPresel():

	cuts = "("+getCommonPresel();
	cuts += " && (J_psi_1S_M*J_psi_1S_M)>6000000 && (J_psi_1S_M*J_psi_1S_M)<12960000) ";

	return cuts;



def getJPsiNoPidPresel():

	cuts="("+getCommonPreselNoPidCut();
	cuts += " && (J_psi_1S_M*J_psi_1S_M)>6000000 && (J_psi_1S_M*J_psi_1S_M)<12960000) ";

	return cuts;



def getPsi2SPresel():

	cuts = "("+getCommonPresel();
	cuts += " && J_psi_1S_M > 3150 && J_psi_1S_M<4050 )";

	return cuts;


def getPsi2SNoPidPresel():

	cuts="("+getCommonPreselNoPidCut();
	cuts += " && J_psi_1S_M > 3150 && J_psi_1S_M<4050 )";

	return cuts;


# /*******************************************
#  * MuMu offline cuts
#  *******************************************/

def getCommonPreselMuMu():


	cuts="";

	cuts += "(e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3";
	cuts += " && e_plus_hasRich == 1 && e_minus_hasRich == 1 && K_Kst_hasRich==1  && K_Kst_InMuonAcc == 1";   
	cuts += " && nSPDHits<600";

	cuts += " && e_plus_PT > 800 && e_minus_PT > 800 "; #//PID fiducial
	cuts += " && e_plus_isMuon && e_minus_isMuon && !K_Kst_isMuon";   
	cuts += " && e_plus_PIDmu>-3. && e_minus_PIDmu > -3 ";
	cuts += " && K_Kst_ProbNNk > 0.2 ";

	# //cuts += " && B_plus_M02_Subst0_mu2pi>1885 && B_plus_M02>1885 ";
	# // cuts += " && Kemu_MKl_l2pi>1885 && B_plus_M02>1885 ";  #//cascade vetoes
	cuts += " && abs(Kmu_M_K2mu -  3097.) > 60 && abs(Kmu_M_K2mu -  3686.) > 60 )";   #//misid

	return cuts;



def getCommonPreselMuMuNoPidCut():


	cuts="";

	cuts += "(e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3";
	cuts += " && e_plus_hasRich == 1 && e_minus_hasRich == 1 && K_Kst_hasRich==1 && K_Kst_InMuonAcc == 1";
	cuts += " && nSPDHits<600";

	cuts += " && e_plus_PT > 800 && e_minus_PT > 800"; #//PID fiducial
	cuts += " && e_plus_isMuon && e_minus_isMuon && !K_Kst_isMuon ";

	# //cuts += " && B_plus_M02_Subst0_mu2pi>1885 && B_plus_M02>1885 ";
	# // cuts += " && Kemu_MKl_l2pi>1885 && B_plus_M02>1885 "; #//cascad vetoes
	cuts += " && abs(Kmu_M_K2mu -  3097.) > 60 && abs(Kmu_M_K2mu -  3686.) > 60 )";#//misid

	return cuts;


def getCommonPreselMuMuNoPidNoIsMuonCut():

	cuts = "";

	cuts += "(e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3";
	cuts += " && e_plus_hasRich == 1 && e_minus_hasRich == 1 && K_Kst_hasRich==1 && K_Kst_InMuonAcc == 1 && e_plus_InMuonAcc == 1 && e_minus_InMuonAcc == 1";
	cuts += " && nSPDHits<600";
	
	# // cuts += " && Kemu_MKl_l2pi>1885 && B_plus_M02>1885 "; //no cascade!
	cuts += " && abs(Kmu_M_K2mu -  3097.) > 60 && abs(Kmu_M_K2mu -  3686.) > 60 ";
	cuts += " && e_plus_PT > 800 && e_minus_PT > 800)"; #//PID fiducial

	return cuts;


def getJPsiPreselMuMu():
 
	cuts = "(";
	cuts += getCommonPreselMuMu();
	cuts += " && J_psi_1S_M > 2946 && J_psi_1S_M < 3176 )";

	return cuts;


def getJPsiPreselMuMuNoPidCut():

	cuts = "(";
	cuts += getCommonPreselMuMuNoPidCut();
	cuts += " && J_psi_1S_M > 2946 && J_psi_1S_M < 3176 )";

	return cuts;


def getJPsiPreselMuMuNoPidNoIsMuonCut():

	cuts = "(";
	cuts += getCommonPreselMuMuNoPidNoIsMuonCut();
	cuts += " && J_psi_1S_M > 2946 && J_psi_1S_M < 3176 )";

	return cuts;


def getPsi2SPreselMuMu():
  
	cuts = "(";
	cuts += getCommonPreselMuMu();
	cuts += " && J_psi_1S_M > 3536 && J_psi_1S_M < 3766 )";

	return cuts;

def getPsi2SPreselMuMuNoPidCut():

	cuts = "(";
	cuts += getCommonPreselMuMuNoPidCut();
	cuts += " && J_psi_1S_M > 3536 && J_psi_1S_M < 3766 )";

	return cuts;


def getPsi2SPreselMuMuNoPidNoIsMuonCut():

	cuts = "(";
	cuts += getCommonPreselMuMuNoPidNoIsMuonCut();
	cuts += " && J_psi_1S_M > 3536 && J_psi_1S_M < 3766 )";

	return cuts;




# /****************
# Run1 Trigger Cuts
# ****************/

def getFullHlt1():

	return "(B_plus_Hlt1TrackAllL0Decision_TOS==1)";


def getFullHlt2():

	return "( (B_plus_Hlt2TopoE3BodyBBDTDecision_TOS==1) || (B_plus_Hlt2TopoE2BodyBBDTDecision_TOS==1) || (B_plus_Hlt2Topo3BodyBBDTDecision_TOS==1) || (B_plus_Hlt2Topo2BodyBBDTDecision_TOS==1))";


def getFullHlt():

  cuts = "";

  cuts+= getFullHlt1()+" && "+getFullHlt2();
  return cuts;



def getHltCutMuMu():

	cuts = "";

	cuts += "( (B_plus_Hlt1TrackMuonDecision_TOS || B_plus_Hlt1TrackAllL0Decision_TOS) ";
	cuts += " && (B_plus_Hlt2Topo2BodyBBDTDecision_TOS || B_plus_Hlt2Topo3BodyBBDTDecision_TOS ||B_plus_Hlt2TopoMu2BodyBBDTDecision_TOS || B_plus_Hlt2TopoMu3BodyBBDTDecision_TOS || B_plus_Hlt2DiMuonDecision_TOS) )";

	return cuts;



def getFullHltCommon():

	return "( (B_plus_Hlt1TrackAllL0Decision_TOS==1) &&  ( (B_plus_Hlt2Topo3BodyBBDTDecision_TOS==1) || (B_plus_Hlt2Topo2BodyBBDTDecision_TOS==1)) )";


def getFullL0():

	return "("+getL0ElectronTOS()+ " || "+getL0HadronTOS() + " || " + getL0TIS()+")";


def getFullTrig():

	return "("+getFullL0()+" && "+getFullHlt()+")";



def getTriggerCutMuMu():

	cuts = "";

	cuts += "( "+getL0MuonTOS();
	cuts += " && (B_plus_Hlt1TrackMuonDecision_TOS || B_plus_Hlt1TrackAllL0Decision_TOS) ";
	cuts += " && (B_plus_Hlt2Topo2BodyBBDTDecision_TOS || B_plus_Hlt2Topo3BodyBBDTDecision_TOS ||B_plus_Hlt2TopoMu2BodyBBDTDecision_TOS || B_plus_Hlt2TopoMu3BodyBBDTDecision_TOS || B_plus_Hlt2DiMuonDecision_TOS) )";

	return cuts;




def getTrigCat0():  return "("+getFullHlt()+" && (( e_plus_L0ElectronDecision_TOS && e_plus_L0Calo_ECAL_realET > 3000) || ( e_minus_L0ElectronDecision_TOS && e_minus_L0Calo_ECAL_realET > 3000)))";
def getTrigCat1():  return "("+getL0HadronTOS()+" && "+getFullHlt()+" && K_Kst_L0Calo_HCAL_realET > 3500 && !(( e_plus_L0ElectronDecision_TOS && e_plus_L0Calo_ECAL_realET > 3000) || ( e_minus_L0ElectronDecision_TOS && e_minus_L0Calo_ECAL_realET > 3000)))";
def getTrigCat2():  return "("+getL0TIS()+" && "+getFullHlt()+" && !(( e_plus_L0ElectronDecision_TOS && e_plus_L0Calo_ECAL_realET > 3000) || ( e_minus_L0ElectronDecision_TOS && e_minus_L0Calo_ECAL_realET > 3000)) && !("+getL0HadronTOS()+" && K_Kst_L0Calo_HCAL_realET > 3500))";
def getTrigCat3():  return "("+getL0TIS()+" && "+getFullHlt()+")";
def getTrigCat4():  return "("+getL0HadronTOS()+" && "+getFullHlt()+" && K_Kst_L0Calo_HCAL_realET > 3500)";
def getTrigCat5():  return "("+getL0ElectronTOS()+" && B_plus_Hlt1Phys_TIS && B_plus_Hlt2Phys_TIS)";
def getTrigCat6():  return "("+getL0HadronTOS()+" && B_plus_Hlt1Phys_TIS && B_plus_Hlt2Phys_TIS)";
def getTrigCat7():  return "("+getL0TIS()+" && B_plus_Hlt1Phys_TIS && B_plus_Hlt2Phys_TIS)";


def getTrigCat0Kemu():  return "("+getFullHlt()+" && (( e_plus_L0ElectronDecision_TOS && e_plus_L0Calo_ECAL_realET > 3000) || ( e_minus_L0MuonDecision_TOS && e_minus_PT > 800)))";
def getTrigCat1Kemu():  return "("+getL0HadronTOS()+" && "+getFullHlt()+" && K_Kst_L0Calo_HCAL_realET > 3500 && !(( e_plus_L0ElectronDecision_TOS && e_plus_L0Calo_ECAL_realET > 3000) || ( e_minus_L0MuonDecision_TOS && e_minus_PT > 800)))";
def getTrigCat2Kemu():  return "("+getL0TIS()+" && "+getFullHlt()+" && !(( e_plus_L0ElectronDecision_TOS && e_plus_L0Calo_ECAL_realET > 3000) || ( e_minus_L0MuonDecision_TOS && e_minus_PT > 800)) && !("+getL0HadronTOS()+" && K_Kst_L0Calo_HCAL_realET > 3500))";
def getTrigCat3Kemu():  return "("+getL0TIS()+" && "+getFullHlt()+")";
def getTrigCat4Kemu():  return "("+getL0HadronTOS()+" && "+getFullHlt()+" && K_Kst_L0Calo_HCAL_realET > 3500)";


def getTrigCat0MuMu():  return "("+getHltCutMuMu()+" && "+getL0MuonTOS(-1, 800)+")";
def getTrigCat1MuMu():  return "("+getL0HadronTOS(-1, 3500) + " && " + getHltCutMuMu() +" && !"+getL0MuonTOS(-1, 800)+")";
def getTrigCat2MuMu():  return "("+getL0TIS(-1)+" && "+getHltCutMuMu()+" && !("+getL0MuonTOS(-1, 800)+") && !("+getL0HadronTOS(-1, 3500)+ "))";
def getTrigCat3MuMu():  return "("+getL0TIS(-1)+" && "+getHltCutMuMu()+")";
def getTrigCat4MuMu():  return "("+getL0HadronTOS(-1, 3500)+" && "+getHltCutMuMu()+")";
def getTrigCat5MuMu():  return "("+getL0MuonTOS(-1)+" && B_plus_Hlt1Phys_TIS && B_plus_Hlt2Phys_TIS)";
def getTrigCat6MuMu():  return "("+getL0HadronTOS(-1)+" && B_plus_Hlt1Phys_TIS && B_plus_Hlt2Phys_TIS)";
def getTrigCat7MuMu():  return "("+getL0TIS(-1)+" && B_plus_Hlt1Phys_TIS && B_plus_Hlt2Phys_TIS)";




# /*******************************************
#  * Run2 CutStrings *
#  *******************************************/


# /*******************************************
#  * MuMu offline cuts
#  *******************************************/


def getCommonPreselMuMuRun2():

	cuts = "";

	cuts += "(e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3";
	cuts += " && e_plus_hasRich == 1 && e_minus_hasRich == 1 && K_Kst_hasRich==1  && K_Kst_InMuonAcc == 1";
	cuts += " && nSPDHits<450";
	# //cuts += " && B_plus_M02_Subst0_mu2pi>1885 && B_plus_M02>1885 "; //cascades
	# // cuts += " && Kemu_MKl_l2pi>1885 && B_plus_M02>1885 "; 
	cuts += " && e_plus_PT > 800 && e_minus_PT > 800"; #//PID fiducial
	cuts += " && e_plus_isMuon && e_minus_isMuon && !K_Kst_isMuon";
	cuts += " && e_plus_PIDmu>-3. && e_minus_PIDmu > -3";
	cuts += " && K_Kst_MC15TuneV1_ProbNNk > 0.2";

	cuts += " && abs(Kmu_M_K2mu -  3097.) > 60 && abs(Kmu_M_K2mu -  3686.) > 60 ";#//mis-id vetoes

	cuts += ")";

	return cuts;


def getCommonPreselMuMuNoPidCutRun2():

	cuts = "";

	cuts += "(e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3";
	cuts += " && e_plus_hasRich == 1 && e_minus_hasRich == 1 && K_Kst_hasRich==1  && K_Kst_InMuonAcc == 1";
	cuts += " && nSPDHits<450";
	# //cuts += " && B_plus_M02_Subst0_mu2pi>1885 && B_plus_M02>1885 "; //cascades
	# // cuts += " && Kemu_MKl_l2pi>1885 && B_plus_M02>1885 "; 
	cuts += " && e_plus_PT > 800 && e_minus_PT > 800"; #//PID fiducial
	cuts += " && e_plus_isMuon && e_minus_isMuon && !K_Kst_isMuon";
	cuts += " && abs(Kmu_M_K2mu -  3097.) > 60 && abs(Kmu_M_K2mu -  3686.) > 60 ";#//mis-id vetoes

	cuts += ")";

	return cuts;


def getCommonPreselMuMuNoPidNoIsMuonCutRun2():

	cuts = "";

	cuts += "(e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3";
	cuts += " && e_plus_hasRich == 1 && e_minus_hasRich == 1 && K_Kst_hasRich==1  && K_Kst_InMuonAcc == 1";
	cuts += " && nSPDHits<450";
	# //cuts += " && B_plus_M02_Subst0_mu2pi>1885 && B_plus_M02>1885 "; //cascades
	# // cuts += " && Kemu_MKl_l2pi>1885 && B_plus_M02>1885 "; 
	cuts += " && e_plus_PT > 800 && e_minus_PT > 800"; #//PID fiducial
	cuts += " && abs(Kmu_M_K2mu -  3097.) > 60 && abs(Kmu_M_K2mu -  3686.) > 60 ";#//mis-id vetoes

	cuts += ")";

	return cuts;


def getJPsiPreselMuMuRun2():
  
	cuts = "(";
	cuts += getCommonPreselMuMuRun2();
	cuts += " && J_psi_1S_M > 2946 && J_psi_1S_M < 3176 )";

	return cuts;


def getJPsiPreselMuMuNoPidCutRun2():

	cuts = "(";
	cuts += getCommonPreselMuMuNoPidCutRun2();
	cuts += " && J_psi_1S_M > 2946 && J_psi_1S_M < 3176 )";

	return cuts;


def getJPsiPreselMuMuNoPidNoIsMuonCutRun2():

	cuts = "(";
	cuts += getCommonPreselMuMuNoPidNoIsMuonCutRun2();
	cuts += " && J_psi_1S_M > 2946 && J_psi_1S_M < 3176 )";

	return cuts;


def getPsi2SPreselMuMuRun2():
  
	cuts = "(";
	cuts += getCommonPreselMuMuRun2();
	cuts += " && J_psi_1S_M > 3536 && J_psi_1S_M < 3766 )";

	return cuts;


def getPsi2SPreselMuMuNoPidCutRun2():

	cuts = "(";
	cuts += getCommonPreselMuMuNoPidCutRun2();
	cuts += " && J_psi_1S_M > 3536 && J_psi_1S_M < 3766 )";
	# // cuts += ")";

	return cuts;


def getPsi2SPreselMuMuNoPidNoIsMuonCutRun2():

	cuts = "(";
	cuts += getCommonPreselMuMuNoPidNoIsMuonCutRun2();
	cuts += " && J_psi_1S_M > 3536 && J_psi_1S_M < 3766 )";

	return cuts;


# /*******************************************
#  * EE offline cuts
#  *******************************************/

def getCommonPreselRun2(custom_cuts={}):

	cuts="";
	cuts="(K_Kst_TRACK_GhostProb<0.3 ";
	cuts+="&& e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3 ";
	cuts+="&& K_Kst_MC15TuneV1_ProbNNk>0.2 && K_Kst_PIDe<0 ";
	cuts+="&& e_plus_PIDe>3 && e_minus_PIDe>3 ";
	cuts+="&& e_plus_hasCalo == 1 && e_plus_hasRich == 1 ";
	cuts+="&& K_Kst_hasRich == 1 ";
	cuts+="&& e_minus_hasCalo == 1 && e_minus_hasRich == 1 ";
	cuts+="&& (TMath::Abs(e_minus_L0Calo_ECAL_xProjection)>363.6 || TMath::Abs(e_minus_L0Calo_ECAL_yProjection)>282.6)"; #//trigger fiducial
	cuts+="&& (TMath::Abs(e_plus_L0Calo_ECAL_xProjection)>363.6 || TMath::Abs(e_plus_L0Calo_ECAL_yProjection)>282.6)";
	cuts+="&& nSPDHits < 450";
	cuts+="&& e_plus_PT > 500 && e_minus_PT > 500 && e_plus_P > 3000 && e_minus_P > 3000"; #//PID fiducial
	# cuts+="&& (Kemu_TRACK_MKl_l2pi < 1825 || Kemu_TRACK_MKl_l2pi > 1905)"; #// Cascade cuts
	# cuts+="&& Kemu_TRACK_MKl_l2pi > 1150";
	# cuts+="&& (J_psi_1S_TRACK_M*J_psi_1S_TRACK_M < 19e6 || Kemu_TRACK_MKl_l2pi > 1600)";

	if custom_cuts:

		try:
			if custom_cuts['central']:
				cuts+="&& (Kemu_TRACK_MKl_l2pi < 1825 || Kemu_TRACK_MKl_l2pi > 1905)"; #// Cascade cuts
				cuts+="&& (Kemu_MKl > 1885)"; #// Cascade cuts
		except:
			if custom_cuts['window']: cuts+="&& (Kemu_TRACK_MKl_l2pi < 1825 || Kemu_TRACK_MKl_l2pi > 1905)"; #// Cascade cuts
			if custom_cuts['kpipi_cuts']:
				cuts+="&& Kemu_TRACK_MKl_l2pi > 1150";
				cuts+="&& (J_psi_1S_TRACK_M*J_psi_1S_TRACK_M < 19e6 || Kemu_TRACK_MKl_l2pi > 1600)";
			# if custom_cuts['casBDT']: cuts+="&& (Kemu_MKl > 1885 || cascadeBDTround2BDTGfixedRun3T > %.5f)"%custom_cuts['casBDTcut'];
			# if custom_cuts['casBDT']: cuts+="&& (Kemu_MKl > 1885 || CascadeBDTpython > %.5f)"%custom_cuts['casBDTcut'];
			# if custom_cuts['prcBDT']: cuts+="&& prcBDTround2BDTGfixedRun3T > %.5f"%custom_cuts['prcBDTcut'];
			if custom_cuts['mklcut']: cuts+="&& Kemu_MKl > %d"%custom_cuts['mklcut_value'];

	else:
		cuts+="&& (Kemu_TRACK_MKl_l2pi < 1825 || Kemu_TRACK_MKl_l2pi > 1905)"; #// Cascade cuts
		cuts+="&& Kemu_TRACK_MKl_l2pi > 1150";
		cuts+="&& (J_psi_1S_TRACK_M*J_psi_1S_TRACK_M < 19e6 || Kemu_TRACK_MKl_l2pi > 1600)";
		# cuts+="&& (Kemu_MKl > 1885 || cascadeBDTround2BDTGfixedRun3T > 0.47)";
		# cuts+="&& prcBDTround2BDTGfixedRun3T > 0.471875";


	cuts+=")";
	return cuts;



def getCommonPreselNoPidCutRun2(custom_cuts={}):

	cuts="";
	cuts+="(K_Kst_TRACK_GhostProb<0.3 ";
	cuts+="&& e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3 ";
	cuts+="&& e_plus_hasCalo == 1 && e_plus_hasRich == 1 ";
	cuts+="&& K_Kst_hasRich == 1 ";
	cuts+="&& e_minus_hasCalo == 1 && e_minus_hasRich == 1 ";
	cuts+="&& (TMath::Abs(e_minus_L0Calo_ECAL_xProjection)>363.6 || TMath::Abs(e_minus_L0Calo_ECAL_yProjection)>282.6)"; #//trigger fiducial
	cuts+="&& (TMath::Abs(e_plus_L0Calo_ECAL_xProjection)>363.6 || TMath::Abs(e_plus_L0Calo_ECAL_yProjection)>282.6)";
	cuts+="&& nSPDHits < 450";
	cuts+="&& e_plus_PT > 500 && e_minus_PT > 500 && e_plus_P > 3000 && e_minus_P > 3000"; #//PID fiducial
	
	if custom_cuts:

		try:
			if custom_cuts['central']:
				cuts+="&& (Kemu_TRACK_MKl_l2pi < 1825 || Kemu_TRACK_MKl_l2pi > 1905)"; #// Cascade cuts
				cuts+="&& (Kemu_MKl > 1885)"; #// Cascade cuts
		except:
			if custom_cuts['window']: cuts+="&& (Kemu_TRACK_MKl_l2pi < 1825 || Kemu_TRACK_MKl_l2pi > 1905)"; #// Cascade cuts
			if custom_cuts['kpipi_cuts']:
				cuts+="&& Kemu_TRACK_MKl_l2pi > 1150";
				cuts+="&& (J_psi_1S_TRACK_M*J_psi_1S_TRACK_M < 19e6 || Kemu_TRACK_MKl_l2pi > 1600)";
			if custom_cuts['casBDT']: cuts+="&& (Kemu_MKl > 1885 || CascadeBDTpython > %.5f)"%custom_cuts['casBDTcut'];
			if custom_cuts['prcBDT']: cuts+="&& prcBDTround2BDTGfixedRun3T > %.5f"%custom_cuts['prcBDTcut'];
			if custom_cuts['mklcut']: cuts+="&& Kemu_MKl > %d"%custom_cuts['mklcut_value'];

	else:
		cuts+="&& (Kemu_TRACK_MKl_l2pi < 1825 || Kemu_TRACK_MKl_l2pi > 1905)"; #// Cascade cuts
		cuts+="&& Kemu_TRACK_MKl_l2pi > 1150";
		cuts+="&& (J_psi_1S_TRACK_M*J_psi_1S_TRACK_M < 19e6 || Kemu_TRACK_MKl_l2pi > 1600)";
		cuts+="&& (Kemu_MKl > 1885 || cascadeBDTround2BDTGfixedRun3T > 0.47)";
		cuts+="&& prcBDTround2BDTGfixedRun3T > 0.471875";

	cuts+=")";

	return cuts;

def getCommonPreselNoPidCutRun2_relaxCascades():

	cuts="";
	cuts+="(K_Kst_TRACK_GhostProb<0.3 ";
	cuts+="&& e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3 ";
	cuts+="&& e_plus_hasCalo == 1 && e_plus_hasRich == 1 ";
	cuts+="&& K_Kst_hasRich == 1 ";
	cuts+="&& e_minus_hasCalo == 1 && e_minus_hasRich == 1 ";
	cuts+="&& (TMath::Abs(e_minus_L0Calo_ECAL_xProjection)>363.6 || TMath::Abs(e_minus_L0Calo_ECAL_yProjection)>282.6)"; #//trigger fiducial
	cuts+="&& (TMath::Abs(e_plus_L0Calo_ECAL_xProjection)>363.6 || TMath::Abs(e_plus_L0Calo_ECAL_yProjection)>282.6)";
	cuts+="&& nSPDHits < 450";
	cuts+="&& e_plus_PT > 500 && e_minus_PT > 500 && e_plus_P > 3000 && e_minus_P > 3000"; #//PID fiducial
	# cuts+="&& (Kemu_TRACK_MKl_l2pi < 1825 || Kemu_TRACK_MKl_l2pi > 1905)"; #// Cascade cuts
	# cuts+="&& Kemu_TRACK_MKl_l2pi > 1150";
	# cuts+="&& (J_psi_1S_TRACK_M*J_psi_1S_TRACK_M < 19e6 || Kemu_TRACK_MKl_l2pi > 1600)";
	# cuts+="&& prcBDTround2BDTGfixedRun3T > 0.471875";
	# cuts+="&& (Kemu_MKl > 1885 || cascadeBDTround2BDTGfixedRun3T > 0.47)";
	cuts+=")";
	return cuts;




def getJPsiPreselRun2():

	cuts="("+getCommonPreselRun2();
	cuts += " && (J_psi_1S_M*J_psi_1S_M)>6000000 && (J_psi_1S_M*J_psi_1S_M)<12960000) ";

	return cuts;



def getJPsiNoPidPreselRun2():

	cuts="("+getCommonPreselNoPidCutRun2();
	cuts += " && (J_psi_1S_M*J_psi_1S_M)>6000000 && (J_psi_1S_M*J_psi_1S_M)<12960000) ";

	return cuts;



def getPsi2SKPreselRun2():

	cuts="("+getCommonPreselRun2();
	cuts += " && J_psi_1S_M > 3150 && J_psi_1S_M<4050 )";

	return cuts;


def getPsi2SNoPidPreselRun2():

	cuts="("+getCommonPreselNoPidCutRun2();
	cuts += " && J_psi_1S_M > 3150 && J_psi_1S_M<4050 )";

	return cuts;




# /*******************************************
#  * Run2 Trigger Nominal Cuts
#  *******************************************/


def getMuPlusMuonTOSRun2():


	ret="";
	ret += "((TCKCat == 0 || TCKCat == 1 || TCKCat == 3) && e_plus_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 37)" ;
	ret += " || ((TCKCat == 4 || TCKCat == 8) && e_plus_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 29)";
	ret += " || ((TCKCat == 5) && e_plus_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 35)";
	ret += " || ((TCKCat == 6) && e_plus_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 31)";
	ret += " || ((TCKCat == 7) && e_plus_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 39)";
	ret += " || ((TCKCat == 9) && e_plus_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 36)";
	ret += " || (TCKCat == 2 && e_plus_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 57))"; 

	return ret;


def getMuMinusMuonTOSRun2():



	ret="(";
	ret += "((TCKCat == 0 || TCKCat == 1 || TCKCat == 3) && e_minus_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 37)" ;
	ret += " || ((TCKCat == 4 || TCKCat == 8) && e_minus_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 29)";
	ret += " || ((TCKCat == 5) && e_minus_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 35)";
	ret += " || ((TCKCat == 6) && e_minus_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 31)";
	ret += " || ((TCKCat == 7) && e_minus_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 39)";
	ret += " || ((TCKCat == 9) && e_minus_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 36)";
	ret += " || (TCKCat == 2 && e_minus_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 57))"; 

	return ret;


def getEPlusElectronTOSRun2():


	# // before 2017 had to be introduced
	ret="(e_plus_L0ElectronDecision_TOS==1)";

	return ret;

def getEMinusElectronTOSRun2():


	ret="(e_minus_L0ElectronDecision_TOS==1)";

	return ret;


def getL0ElectronTOS(TCKCat=-1, offlineThreshold=-1):

	if(offlineThreshold < 0):
	
		return "(e_plus_L0ElectronDecision_TOS==1 || e_minus_L0ElectronDecision_TOS==1)";

	if(offlineThreshold >= 0):
	
		#// tighter threshold in 2017 and 2018
		if(TCKCat > 3 and TCKCat < 9): offlineThreshold = 2955;#//3000;
		if(TCKCat == 9): offlineThreshold = 3150;
		thresh=str(offlineThreshold);

		return "( (e_plus_L0ElectronDecision_TOS==1 && e_plus_L0Calo_ECAL_realET > "+thresh+") || (e_minus_L0ElectronDecision_TOS==1 && e_minus_L0Calo_ECAL_realET > "+thresh+") )";

	print("WARNING: in getL0ElectronTOS, not a valid TCKCat");
	return "(1)";


def getL0HadronTOS(TCKCat=-1, offlineThreshold=-1):

	if(offlineThreshold < 0):
	
	  return "(K_Kst_L0HadronDecision_TOS==1)";
	
	if(offlineThreshold >= 0):
	
	  thresh=str(offlineThreshold);

	  return "(K_Kst_L0HadronDecision_TOS==1 && K_Kst_L0Calo_HCAL_realET > "+thresh+")";
	

	print("WARNING: in getL0HadronTOS, not a valid TCKCat")
	return "(1)";


def getL0MuonTOS(TCKCat=-1, offlineThreshold=-1):
	
	if(offlineThreshold < 0 ):
	
		#// No L0 cut
		if(TCKCat < 0):  return "(e_plus_L0MuonDecision_TOS || e_minus_L0MuonDecision_TOS)";
		#// 2016
		if(TCKCat == 0 or TCKCat == 1 or TCKCat == 3): return "((e_plus_L0MuonDecision_TOS || e_minus_L0MuonDecision_TOS) && L0Data_Muon1_Pt >= 37)";
		#// 2015
		if(TCKCat == 2): return "((e_plus_L0MuonDecision_TOS || e_minus_L0MuonDecision_TOS) && L0Data_Muon1_Pt >= 57)";
		#// 2017
		if(TCKCat == 4 or TCKCat == 8): return "((e_plus_L0MuonDecision_TOS || e_minus_L0MuonDecision_TOS) && L0Data_Muon1_Pt >= 29)";
		if(TCKCat == 5): return "((e_plus_L0MuonDecision_TOS || e_minus_L0MuonDecision_TOS) && L0Data_Muon1_Pt >= 35)";
		if(TCKCat == 6): return "((e_plus_L0MuonDecision_TOS || e_minus_L0MuonDecision_TOS) && L0Data_Muon1_Pt >= 31)";
		if(TCKCat == 7): return "((e_plus_L0MuonDecision_TOS || e_minus_L0MuonDecision_TOS) && L0Data_Muon1_Pt >= 39)";
		#// 2018
		if(TCKCat == 9): return "((e_plus_L0MuonDecision_TOS || e_minus_L0MuonDecision_TOS) && L0Data_Muon1_Pt >= 36)";

	if(offlineThreshold >= 0 ):
	
		thresh=str(offlineThreshold);
		#// No L0 cut
		if(TCKCat < 0):  return "( (e_plus_L0MuonDecision_TOS && e_plus_PT > "+thresh+") || (e_minus_L0MuonDecision_TOS && e_minus_PT > "+thresh+") )";
		#// 2016
		if(TCKCat == 0 or TCKCat == 1 or TCKCat == 3): return "( ((e_plus_L0MuonDecision_TOS && e_plus_PT > "+thresh+") || (e_minus_L0MuonDecision_TOS && e_minus_PT > "+thresh+")) && L0Data_Muon1_Pt >= 37)";
		#// 2015
		if(TCKCat == 2): return "( ((e_plus_L0MuonDecision_TOS && e_plus_PT > "+thresh+") || (e_minus_L0MuonDecision_TOS && e_minus_PT > "+thresh+")) && L0Data_Muon1_Pt >= 57)";
		#// 2017
		if(TCKCat == 4 or TCKCat == 8): return "( ((e_plus_L0MuonDecision_TOS && e_plus_PT > "+thresh+") || (e_minus_L0MuonDecision_TOS && e_minus_PT > "+thresh+")) && L0Data_Muon1_Pt >= 29)";
		if(TCKCat == 5): return "( ((e_plus_L0MuonDecision_TOS && e_plus_PT > "+thresh+") || (e_minus_L0MuonDecision_TOS && e_minus_PT > "+thresh+")) && L0Data_Muon1_Pt >= 35)";
		if(TCKCat == 6): return "( ((e_plus_L0MuonDecision_TOS && e_plus_PT > "+thresh+") || (e_minus_L0MuonDecision_TOS && e_minus_PT > "+thresh+")) && L0Data_Muon1_Pt >= 31)";
		if(TCKCat == 7): return "( ((e_plus_L0MuonDecision_TOS && e_plus_PT > "+thresh+") || (e_minus_L0MuonDecision_TOS && e_minus_PT > "+thresh+")) && L0Data_Muon1_Pt >= 39)";
		#// 2018
		if(TCKCat == 9): return "( ((e_plus_L0MuonDecision_TOS && e_plus_PT > "+thresh+") || (e_minus_L0MuonDecision_TOS && e_minus_PT > "+thresh+")) && L0Data_Muon1_Pt >= 36)";


	print("WARNING: in getL0MuonTOS, not a valid TCKCat");
	return "(1)";


def getL0TIS(TCKCat=-1):


	TISString =  "((B_plus_L0ElectronDecision_TIS==1) || (B_plus_L0HadronDecision_TIS==1) || (B_plus_L0MuonDecision_TIS==1) || (B_plus_L0PhotonDecision_TIS==1))" ; 
	# No L0 cut
	if(TCKCat < 0 ): return TISString;
	# 2016
	if(TCKCat == 0 or TCKCat == 1 or TCKCat == 3): return "( (B_plus_L0PhotonDecision_TIS) || (B_plus_L0ElectronDecision_TIS) || (B_plus_L0HadronDecision_TIS) || (B_plus_L0MuonDecision_TIS && L0Data_Muon1_Pt >= 37) )";
	# 2015
	if(TCKCat == 2): return "( (B_plus_L0PhotonDecision_TIS) || (B_plus_L0ElectronDecision_TIS) || (B_plus_L0HadronDecision_TIS) || (B_plus_L0MuonDecision_TIS && L0Data_Muon1_Pt >= 57) )";
	# 2017
	if(TCKCat == 4 or TCKCat == 8): return "( (B_plus_L0PhotonDecision_TIS) || (B_plus_L0ElectronDecision_TIS) || (B_plus_L0HadronDecision_TIS) || (B_plus_L0MuonDecision_TIS && L0Data_Muon1_Pt >= 29))";
	if(TCKCat == 5): return "( (B_plus_L0PhotonDecision_TIS) || (B_plus_L0ElectronDecision_TIS) || (B_plus_L0HadronDecision_TIS) || (B_plus_L0MuonDecision_TIS && L0Data_Muon1_Pt >= 35))";
	if(TCKCat == 6): return "( (B_plus_L0PhotonDecision_TIS) || (B_plus_L0ElectronDecision_TIS) || (B_plus_L0HadronDecision_TIS) || (B_plus_L0MuonDecision_TIS && L0Data_Muon1_Pt >= 31))";
	if(TCKCat == 7): return "( (B_plus_L0PhotonDecision_TIS) || (B_plus_L0ElectronDecision_TIS) || (B_plus_L0HadronDecision_TIS) || (B_plus_L0MuonDecision_TIS && L0Data_Muon1_Pt >= 39))";
	# 2018
	if(TCKCat == 9): return "( (B_plus_L0PhotonDecision_TIS) || (B_plus_L0ElectronDecision_TIS) || (B_plus_L0HadronDecision_TIS) || (B_plus_L0MuonDecision_TIS && L0Data_Muon1_Pt >= 36))";

	print("WARNING: in getL0TIS, not a valid TCKCat");
	return "(1)";


def getL0HadronTOSOnly(TCKCat=-1, EOfflineThreshold=-1., KOfflineThreshold=-1.):

	#  the 2017 offline threshold cut in !getL0e is propagated by the TCKCat
	return "("+getL0HadronTOS(TCKCat, KOfflineThreshold)+" && !("+getL0ElectronTOS(TCKCat, EOfflineThreshold)+"))";


def getL0TISOnly(TCKCat=-1, EOfflineThreshold=-1., KOfflineThreshold=-1.):

	# the 2017 offline threshold cut in !getL0e is propagated by the TCKCat
	return "("+getL0TIS(TCKCat)+" && !("+getL0ElectronTOS(TCKCat, EOfflineThreshold)+") && !("+getL0HadronTOS(TCKCat, KOfflineThreshold)+") )";


def getHlt1TrackMVACut(b):

	bStr = str(b);
	cut="(";
	cut += "(e_plus_Hlt1TrackMVADecision_TOS && ( TMath::Log(e_plus_IPCHI2_OWNPV) > (1.0/TMath::Power(0.001*e_plus_TRACK_PT-1., 2)) + ("+bStr+"/25000)*(25000-e_plus_TRACK_PT) + TMath::Log(7.4) ))";
	cut += " || (e_minus_Hlt1TrackMVADecision_TOS && ( TMath::Log(e_minus_IPCHI2_OWNPV) > (1.0/TMath::Power(0.001*e_minus_TRACK_PT-1., 2)) + ("+bStr+"/25000)*(25000-e_minus_TRACK_PT) + TMath::Log(7.4) ))";
	cut += " || (K_Kst_Hlt1TrackMVADecision_TOS && ( TMath::Log(K_Kst_IPCHI2_OWNPV) > (1.0/TMath::Power(0.001*K_Kst_TRACK_PT-1., 2)) + ("+bStr+"/25000)*(25000-K_Kst_TRACK_PT) + TMath::Log(7.4) ))";
	cut += ")";
	return cut;


def getFullHlt1Run2(TCKCat=-1):

	# No L0 cut
	if(TCKCat < 0 ): return "(B_plus_Hlt1TrackMVADecision_TOS)";
	# 2015 + some 2016
	if(TCKCat == 0 or TCKCat == 2): return getHlt1TrackMVACut(1.1);
	# the rest of 2016
	if(TCKCat == 1): return getHlt1TrackMVACut(2.3);
	if(TCKCat == 3): return getHlt1TrackMVACut(1.6);
	# 2017
	# if(TCKCat >= 4 && TCKCat <= 8) return getHlt1TrackMVACut(1.1);
	# 2018
	# if(TCKCat >= 9) return "(B_plus_Hlt1TrackMVADecision_TOS)";
	# 2017+2018
	return "(B_plus_Hlt1TrackMVADecision_TOS)";

	# print("In getFullHlt1Run2, abnormal TCKCat: {0}".format(TCKCat));
	# return "(1)";


def getFullHlt2Run2():

	return "(B_plus_Hlt2Topo2BodyDecision_TOS || B_plus_Hlt2Topo3BodyDecision_TOS)";


def getFullHltRun2(TCKCat=-1):

	return "("+getFullHlt1Run2(TCKCat) + " && " + getFullHlt2Run2()+")";


def getHltCutMuMuRun2(TCKCat=-1):

	cuts="";

	#cuts += "( (B_plus_Hlt1TrackMuonDecision_TOS || B_plus_Hlt1TrackMVADecision_TOS || B_plus_Hlt1TwoTrackMVADecision_TOS) ";
	cuts =  "("+getFullHlt1Run2(TCKCat);
	cuts += " && (B_plus_Hlt2Topo2BodyDecision_TOS || B_plus_Hlt2Topo3BodyDecision_TOS || B_plus_Hlt2TopoMu2BodyDecision_TOS || B_plus_Hlt2TopoMu3BodyDecision_TOS))";

	return cuts;



# /*******************************************
#  * Alternative Cuts used for eff corrections
#  *******************************************/


def getL0HMuTIS(TCKCat=-1):


	TISString =  "((B_plus_L0HadronDecision_TIS==1) || (B_plus_L0MuonDecision_TIS==1))" ; 
	# // No L0 cut
	if(TCKCat < 0 ): return TISString;
	# // 2016
	if(TCKCat == 0 or TCKCat == 1 or TCKCat == 3): return "( (B_plus_L0HadronDecision_TIS) || (B_plus_L0MuonDecision_TIS && L0Data_Muon1_Pt >= 37) )";
	# // 2015
	if(TCKCat == 2): return "( (B_plus_L0HadronDecision_TIS) || (B_plus_L0MuonDecision_TIS && L0Data_Muon1_Pt >= 57) )";
	# // 2017
	if(TCKCat == 4 or TCKCat == 8): return "( (B_plus_L0HadronDecision_TIS) || (B_plus_L0MuonDecision_TIS && L0Data_Muon1_Pt >= 29))";
	if(TCKCat == 5): return "( (B_plus_L0HadronDecision_TIS) || (B_plus_L0MuonDecision_TIS && L0Data_Muon1_Pt >= 35))";
	if(TCKCat == 6): return "( (B_plus_L0HadronDecision_TIS) || (B_plus_L0MuonDecision_TIS && L0Data_Muon1_Pt >= 31))";
	if(TCKCat == 7): return "( (B_plus_L0HadronDecision_TIS) || (B_plus_L0MuonDecision_TIS && L0Data_Muon1_Pt >= 39))";
	# // 2018
	if(TCKCat == 9): return "( (B_plus_L0HadronDecision_TIS) || (B_plus_L0MuonDecision_TIS && L0Data_Muon1_Pt >= 36))";

	print("WARNING: in getL0TIS, not a valid TCKCat: {}".format(TCKCat));
	return "(1)";


def getL0HMuTIS2017():


	ret = "(";
	ret += "(TCKCat == 4 && "+getL0HMuTIS(4)+") || ";
	ret += "(TCKCat == 5 && "+getL0HMuTIS(5)+") || ";
	ret += "(TCKCat == 6 && "+getL0HMuTIS(6)+") || ";
	ret += "(TCKCat == 7 && "+getL0HMuTIS(7)+") || ";
	ret += "(TCKCat == 8 && "+getL0HMuTIS(8)+") )";

	return ret;


def getL0TISTotRun2():


	ret = "(";
	ret += "(TCKCat == 0 && "+getL0TIS(0)+") || ";
	ret += "(TCKCat == 1 && "+getL0TIS(1)+") || ";
	ret += "(TCKCat == 3 && "+getL0TIS(3)+") || ";
	ret += "(TCKCat == 4 && "+getL0TIS(4)+") || ";
	ret += "(TCKCat == 5 && "+getL0TIS(5)+") || ";
	ret += "(TCKCat == 6 && "+getL0TIS(6)+") || ";
	ret += "(TCKCat == 7 && "+getL0TIS(7)+") || ";
	ret += "(TCKCat == 8 && "+getL0TIS(8)+") || ";
	ret += "(TCKCat == 9 && "+getL0TIS(9)+") || ";
	ret += "(TCKCat == 2 && "+getL0TIS(2)+") )";

	return ret;


def getL0CaloTISTotRun2():


	return getL0CaloTIS(-1);


def getL0CaloTIS(TCKCat=-1):


	return "((B_plus_L0ElectronDecision_TIS==1) || (B_plus_L0HadronDecision_TIS==1) || (B_plus_L0PhotonDecision_TIS==1))"; 


def getKMuonTOSRun2():


	ret = "(";
	ret += "((TCKCat == 0 || TCKCat == 1 || TCKCat == 3) && K_Kst_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 37)" ;
	ret += " || ((TCKCat == 4 || TCKCat == 8) && K_Kst_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 29)";
	ret += " || ((TCKCat == 5) && K_Kst_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 35)";
	ret += " || ((TCKCat == 6) && K_Kst_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 31)";
	ret += " || ((TCKCat == 7) && K_Kst_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 39)";
	ret += " || ((TCKCat == 9) && K_Kst_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 36)";
	ret += " || (TCKCat == 2 && K_Kst_L0MuonDecision_TOS==1 && L0Data_Muon1_Pt >= 57))"; 

	return ret;


def getFullHltTotRun2():


	ret="(";
	ret += "(TCKCat == 0 && "+getFullHltRun2(0)+") || ";
	ret += "(TCKCat == 1 && "+getFullHltRun2(1)+") || ";
	ret += "(TCKCat == 3 && "+getFullHltRun2(3)+") || ";
	ret += "(TCKCat == 4 && "+getFullHltRun2(4)+") || ";
	ret += "(TCKCat == 5 && "+getFullHltRun2(5)+") || ";
	ret += "(TCKCat == 6 && "+getFullHltRun2(6)+") || ";
	ret += "(TCKCat == 7 && "+getFullHltRun2(7)+") || ";
	ret += "(TCKCat == 8 && "+getFullHltRun2(8)+") || ";
	ret += "(TCKCat == 9 && "+getFullHltRun2(9)+") || ";
	ret += "(TCKCat == 2 && "+getFullHltRun2(2)+"))";

	return ret;


def getFullHltTot2017():

	ret = "(";
	ret += "(TCKCat == 4 && "+getFullHltRun2(4)+") || ";
	ret += "(TCKCat == 5 && "+getFullHltRun2(5)+") || ";
	ret += "(TCKCat == 6 && "+getFullHltRun2(6)+") || ";
	ret += "(TCKCat == 7 && "+getFullHltRun2(7)+") || ";
	ret += "(TCKCat == 8 && "+getFullHltRun2(8)+"))";

	return ret;


def getFullHltMuMuTotRun2():


	ret="(";
	ret += "(TCKCat == 0 && "+getHltCutMuMuRun2(0)+") || ";
	ret += "(TCKCat == 1 && "+getHltCutMuMuRun2(1)+") || ";
	ret += "(TCKCat == 2 && "+getHltCutMuMuRun2(2)+") )";

	return ret;


def getL0HadronTotRun2():


	return getL0HadronTOS(-1);


def getFullL0Run2():

	return "(B_plus_L0ElectronDecision_TOS || B_plus_L0HadronDecision_TOS || B_plus_L0Global_TIS)";


def getFullTrigRun2():

	return "("+getFullL0Run2()+" && "+getFullHlt1Run2()+" && "+getFullHlt2Run2()+")";



# /*******************************************
#  * TCKs definitions
#  *******************************************/


def getTCK(TCKLabel=-1):

	ret="";

	if TCKLabel == 1 :
		ret  =  "( HLTTCK == 3538994 || HLTTCK == 4718642 || HLTTCK == 5898290 || HLTTCK == 5963826 || HLTTCK == 7143474 ";
		ret += "|| HLTTCK == 7340084 || HLTTCK == 7405621 || HLTTCK == 7536693 || HLTTCK == 7733303 || HLTTCK == 7929911 ";
		ret += "|| HLTTCK == 7929911 || HLTTCK == 7929912 )";
		

	elif TCKLabel == 2 :
		ret = "(HLTTCK == 6094899 || HLTTCK == 4849715)";
		

	elif TCKLabel == 3 :
		ret = "(HLTTCK == 7602230)";
		

	elif TCKLabel == 4 :

		ret = "(HLTTCK == 8257594)";
		
	elif TCKLabel == 5 :

		ret = "(HLTTCK == 8323136 || HLTTCK == 8781888 || HLTTCK == 9175104 || HLTTCK == 9306176)";
		
	elif TCKLabel == 6 :

		ret = "(HLTTCK == 9699389 || HLTTCK == 9764925 || HLTTCK == 9895997 || HLTTCK == 10027074 || HLTTCK == 10027075 || HLTTCK == 10092610)";
		
	elif TCKLabel == 7 :

		ret = "(HLTTCK == 10027076 || HLTTCK == 10551364 || HLTTCK == 10616900 || HLTTCK == 10682436)";
		
	elif TCKLabel == 8 : 

		ret = "(HLTTCK == 10420293 || HLTTCK == 10551365)";
		
	elif TCKLabel == 9 :

		ret = "(HLTTCK == 10682438 || HLTTCK == 11075654 || HLTTCK == 11206726 || HLTTCK == 11272262 || HLTTCK == 11337798)";
		
	else:
		print("WARNING: in getTCK, invaliv TCKLabel = {}".format(TCKLabel));
		ret = "(0)";

	return ret;


def getTriggerThreshold(TCKLabel=-1, trigCat = None):


	ret = 1e10;

	if(trigCat == "E" or trigCat == "e"):
	
		if(TCKLabel == 1 or TCKLabel == 2 or TCKLabel == 4 or TCKLabel == 5):
		
			ret = 2500; 
			
		elif(TCKLabel == 3):
			ret = 3000;
			

		elif(TCKLabel == 6):
			ret = 2720;
				
		elif(TCKLabel == 7 or TCKLabel == 8): 
			ret = 2960;
			
		elif(TCKLabel == 9): 
			ret = 2860;
			
		else:	
			print("WARNING: in getTriggerThreshold, invaliv TCKLabel = {}".format(TCKLabel));
		
	if(trigCat == "H" or trigCat == "h"):
	
		if(TCKLabel == 1 or TCKLabel == 5):
		
			ret = 3500; 
			

		elif(TCKLabel == 2):

			ret = 2400;
			

		elif(TCKLabel == 3):

			ret = 2720;
				

		elif(TCKLabel == 4): 

			ret = 3300;
			

		elif(TCKLabel == 6): 

			ret = 3620;
						

		elif(TCKLabel == 7): 

			ret = 3680;
						

		if(TCKLabel == 8 or TCKLabel == 9):
		
			ret = 3500; 
			

		else:	
			print("WARNING: in getTriggerThreshold, invaliv TCKLabel = {}".format(TCKLabel));
		
	if(trigCat == "Mu" or trigCat == "mu"):
	
		if(TCKLabel == 1 or TCKLabel == 4 or TCKLabel == 5):
		
			ret = 1850; 
			
			
		elif(TCKLabel == 2):
			ret = 1000;
			

		elif(TCKLabel == 3):
			ret = 2000;
				
		if(TCKLabel == 6 or TCKLabel == 7 or TCKLabel == 8 or TCKLabel == 9):
			ret = 2200;
			

		else:	
			print("WARNING: in getTriggerThreshold, invaliv TCKLabel = {}".format(TCKLabel));
		

	if(ret>1e9): print("WARNING: in getTriggerThreshold, invalid trigCat: {}").format(trigCat);

	return ret;



# /*************
# TRIGCAT LINES
# **************/

def getTrigCat0Run2(TCKCat=None):
	# eTOS inclusive
	if TCKCat is not None: return "("+ getFullHltRun2(TCKCat) + " && " + getL0ElectronTOS(TCKCat, 2700) + ")";
	else: return "((TCKCat == 0 && "+getTrigCat0Run2(0)+") || (TCKCat == 1 && "+getTrigCat0Run2(1)+") || (TCKCat == 2 && "+getTrigCat0Run2(2)+") || (TCKCat == 3 && "+getTrigCat0Run2(3)+") || (TCKCat == 4 && "+getTrigCat0Run2(4)+") || (TCKCat == 5 && "+getTrigCat0Run2(5)+") || (TCKCat == 6 && "+getTrigCat0Run2(6)+") || (TCKCat == 7 && "+getTrigCat0Run2(7)+") || (TCKCat == 8 && "+getTrigCat0Run2(8)+") || (TCKCat == 9 && "+getTrigCat0Run2(9)+"))"; 
def getTrigCat1Run2(TCKCat=None):
	# hTOS exclusive
	if TCKCat is not None: return "("+ getFullHltRun2(TCKCat) + " && " + getL0HadronTOSOnly(TCKCat, 2700, 3500) + ")";
	else: return "((TCKCat == 0 && "+getTrigCat1Run2(0)+") || (TCKCat == 1 && "+getTrigCat1Run2(1)+") || (TCKCat == 2 && "+getTrigCat1Run2(2)+") || (TCKCat == 3 && "+getTrigCat1Run2(3)+") || (TCKCat == 4 && "+getTrigCat1Run2(4)+") || (TCKCat == 5 && "+getTrigCat1Run2(5)+") || (TCKCat == 6 && "+getTrigCat1Run2(6)+") || (TCKCat == 7 && "+getTrigCat1Run2(7)+") || (TCKCat == 8 && "+getTrigCat1Run2(8)+") || (TCKCat == 9 && "+getTrigCat1Run2(9)+"))"; 
def getTrigCat2Run2(TCKCat=None):
	# TIS exclusive
	if TCKCat is not None: return  "("+ getFullHltRun2(TCKCat) + " && " + getL0TISOnly(TCKCat, 2700, 3500) + ")";
	else: return "((TCKCat == 0 && "+getTrigCat2Run2(0)+") || (TCKCat == 1 && "+getTrigCat2Run2(1)+") || (TCKCat == 2 && "+getTrigCat2Run2(2)+") || (TCKCat == 3 && "+getTrigCat2Run2(3)+") || (TCKCat == 4 && "+getTrigCat2Run2(4)+") || (TCKCat == 5 && "+getTrigCat2Run2(5)+") || (TCKCat == 6 && "+getTrigCat2Run2(6)+") || (TCKCat == 7 && "+getTrigCat2Run2(7)+") || (TCKCat == 8 && "+getTrigCat2Run2(8)+") || (TCKCat == 9 && "+getTrigCat2Run2(9)+"))"; 
def getTrigCat3Run2(TCKCat=None):
	# TIS inclusive
	if TCKCat is not None: return  "("+ getFullHltRun2(TCKCat) + " && " + getL0TIS(TCKCat) + ")";
	else: return "((TCKCat == 0 && "+getTrigCat3Run2(0)+") || (TCKCat == 1 && "+getTrigCat3Run2(1)+") || (TCKCat == 2 && "+getTrigCat3Run2(2)+") || (TCKCat == 3 && "+getTrigCat3Run2(3)+") || (TCKCat == 4 && "+getTrigCat3Run2(4)+") || (TCKCat == 5 && "+getTrigCat3Run2(5)+") || (TCKCat == 6 && "+getTrigCat3Run2(6)+") || (TCKCat == 7 && "+getTrigCat3Run2(7)+") || (TCKCat == 8 && "+getTrigCat3Run2(8)+") || (TCKCat == 9 && "+getTrigCat3Run2(9)+"))"; 
def getTrigCat4Run2(TCKCat=None):
	# hTOS inclusive
	if TCKCat is not None: return  "("+ getFullHltRun2(TCKCat) + " && " + getL0HadronTOS(TCKCat, 3500) + ")";
	else: return "((TCKCat == 0 && "+getTrigCat4Run2(0)+") || (TCKCat == 1 && "+getTrigCat4Run2(1)+") || (TCKCat == 2 && "+getTrigCat4Run2(2)+") || (TCKCat == 3 && "+getTrigCat4Run2(3)+") || (TCKCat == 4 && "+getTrigCat4Run2(4)+") || (TCKCat == 5 && "+getTrigCat4Run2(5)+") || (TCKCat == 6 && "+getTrigCat4Run2(6)+") || (TCKCat == 7 && "+getTrigCat4Run2(7)+") || (TCKCat == 8 && "+getTrigCat4Run2(8)+") || (TCKCat == 9 && "+getTrigCat4Run2(9)+"))"; 
def getTrigCat5Run2(TCKCat=None):
	# eTOS inclusive + HltPhys
	if TCKCat is not None: return  "("+ getL0ElectronTOS(TCKCat) + " &&  B_plus_Hlt1Phys_TIS && B_plus_Hlt2Phys_TIS)";
	else: return "((TCKCat == 0 && "+getTrigCat5Run2(0)+") || (TCKCat == 1 && "+getTrigCat5Run2(1)+") || (TCKCat == 2 && "+getTrigCat5Run2(2)+") || (TCKCat == 3 && "+getTrigCat5Run2(3)+") || (TCKCat == 4 && "+getTrigCat5Run2(4)+") || (TCKCat == 5 && "+getTrigCat5Run2(5)+") || (TCKCat == 6 && "+getTrigCat5Run2(6)+") || (TCKCat == 7 && "+getTrigCat5Run2(7)+") || (TCKCat == 8 && "+getTrigCat5Run2(8)+") || (TCKCat == 9 && "+getTrigCat5Run2(9)+"))"; 
def getTrigCat6Run2(TCKCat=None):
	# hTOS inclusive + HltPhys
	if TCKCat is not None: return  "("+ getL0HadronTOS(TCKCat) + " &&  B_plus_Hlt1Phys_TIS && B_plus_Hlt2Phys_TIS)";
	else: return "((TCKCat == 0 && "+getTrigCat6Run2(0)+") || (TCKCat == 1 && "+getTrigCat6Run2(1)+") || (TCKCat == 2 && "+getTrigCat6Run2(2)+") || (TCKCat == 3 && "+getTrigCat6Run2(3)+") || (TCKCat == 4 && "+getTrigCat6Run2(4)+") || (TCKCat == 5 && "+getTrigCat6Run2(5)+") || (TCKCat == 6 && "+getTrigCat6Run2(6)+") || (TCKCat == 7 && "+getTrigCat6Run2(7)+") || (TCKCat == 8 && "+getTrigCat6Run2(8)+") || (TCKCat == 9 && "+getTrigCat6Run2(9)+"))"; 
def getTrigCat7Run2(TCKCat=None):
	# TIS inclusive + HltPhys
	if TCKCat is not None: return  "("+ getL0TIS(TCKCat) + " &&  B_plus_Hlt1Phys_TIS && B_plus_Hlt2Phys_TIS)";
	else: return "((TCKCat == 0 && "+getTrigCat7Run2(0)+") || (TCKCat == 1 && "+getTrigCat7Run2(1)+") || (TCKCat == 2 && "+getTrigCat7Run2(2)+") || (TCKCat == 3 && "+getTrigCat7Run2(3)+") || (TCKCat == 4 && "+getTrigCat7Run2(4)+") || (TCKCat == 5 && "+getTrigCat7Run2(5)+") || (TCKCat == 6 && "+getTrigCat7Run2(6)+") || (TCKCat == 7 && "+getTrigCat7Run2(7)+") || (TCKCat == 8 && "+getTrigCat7Run2(8)+") || (TCKCat == 9 && "+getTrigCat7Run2(9)+"))"; 
def getTrigCat8Run2(TCKCat=None):
	# TOS exclusive
	if TCKCat is not None: return  "("+ getFullHltRun2(TCKCat)+" && "+getL0ElectronTOS(TCKCat, 2700)+" && !("+getL0TIS(TCKCat)+")";
	else: return "((TCKCat == 0 && "+getTrigCat8Run2(0)+") || (TCKCat == 1 && "+getTrigCat8Run2(1)+") || (TCKCat == 2 && "+getTrigCat8Run2(2)+") || (TCKCat == 3 && "+getTrigCat8Run2(3)+") || (TCKCat == 4 && "+getTrigCat8Run2(4)+") || (TCKCat == 5 && "+getTrigCat8Run2(5)+") || (TCKCat == 6 && "+getTrigCat8Run2(6)+") || (TCKCat == 7 && "+getTrigCat8Run2(7)+") || (TCKCat == 8 && "+getTrigCat8Run2(8)+") || (TCKCat == 9 && "+getTrigCat8Run2(9)+"))"; 



def getTrigCat0MuMuRun2(TCKCat=None):
	# muTOS inclusive
	if TCKCat is not None: return "("+getHltCutMuMuRun2(TCKCat)+" && "+getL0MuonTOS(TCKCat, 800)+")";
	else: return "((TCKCat == 0 && "+getTrigCat0MuMuRun2(0)+") || (TCKCat == 1 && "+getTrigCat0MuMuRun2(1)+") || (TCKCat == 2 && "+getTrigCat0MuMuRun2(2)+") || (TCKCat == 3 && "+getTrigCat0MuMuRun2(3)+") || (TCKCat == 4 && "+getTrigCat0MuMuRun2(4)+") || (TCKCat == 5 && "+getTrigCat0MuMuRun2(5)+") || (TCKCat == 6 && "+getTrigCat0MuMuRun2(6)+") || (TCKCat == 7 && "+getTrigCat0MuMuRun2(7)+") || (TCKCat == 8 && "+getTrigCat0MuMuRun2(8)+") || (TCKCat == 9 && "+getTrigCat0MuMuRun2(9)+"))"; 
def getTrigCat1MuMuRun2(TCKCat=None):
	# hTOS not muTOS
	if TCKCat is not None: return "("+getHltCutMuMuRun2(TCKCat)+ " && " + getL0HadronTOS(TCKCat, 3500)  +" && !"+getL0MuonTOS(TCKCat, 800)+")";
	else: return "((TCKCat == 0 && "+getTrigCat1MuMuRun2(0)+") || (TCKCat == 1 && "+getTrigCat1MuMuRun2(1)+") || (TCKCat == 2 && "+getTrigCat1MuMuRun2(2)+") || (TCKCat == 3 && "+getTrigCat1MuMuRun2(3)+") || (TCKCat == 4 && "+getTrigCat1MuMuRun2(4)+") || (TCKCat == 5 && "+getTrigCat1MuMuRun2(5)+") || (TCKCat == 6 && "+getTrigCat1MuMuRun2(6)+") || (TCKCat == 7 && "+getTrigCat1MuMuRun2(7)+") || (TCKCat == 8 && "+getTrigCat1MuMuRun2(8)+") || (TCKCat == 9 && "+getTrigCat1MuMuRun2(9)+"))"; 
def getTrigCat2MuMuRun2(TCKCat=None):
	# TIS not muTOS not hTOS	
	if TCKCat is not None: return "("+getHltCutMuMuRun2(TCKCat)+" && "+getL0TIS(TCKCat)+" && !("+getL0MuonTOS(TCKCat, 800)+") && !("+getL0HadronTOS(TCKCat, 3500)+ "))";
	else: return "((TCKCat == 0 && "+getTrigCat2MuMuRun2(0)+") || (TCKCat == 1 && "+getTrigCat2MuMuRun2(1)+") || (TCKCat == 2 && "+getTrigCat2MuMuRun2(2)+") || (TCKCat == 3 && "+getTrigCat2MuMuRun2(3)+") || (TCKCat == 4 && "+getTrigCat2MuMuRun2(4)+") || (TCKCat == 5 && "+getTrigCat2MuMuRun2(5)+") || (TCKCat == 6 && "+getTrigCat2MuMuRun2(6)+") || (TCKCat == 7 && "+getTrigCat2MuMuRun2(7)+") || (TCKCat == 8 && "+getTrigCat2MuMuRun2(8)+") || (TCKCat == 9 && "+getTrigCat2MuMuRun2(9)+"))"; 
def getTrigCat3MuMuRun2(TCKCat=None):
	# TIS inclusive
	if TCKCat is not None: return "("+getHltCutMuMuRun2(TCKCat)+" && "+getL0TIS(TCKCat)+")";
	else: return "((TCKCat == 0 && "+getTrigCat3MuMuRun2(0)+") || (TCKCat == 1 && "+getTrigCat3MuMuRun2(1)+") || (TCKCat == 2 && "+getTrigCat3MuMuRun2(2)+") || (TCKCat == 3 && "+getTrigCat3MuMuRun2(3)+") || (TCKCat == 4 && "+getTrigCat3MuMuRun2(4)+") || (TCKCat == 5 && "+getTrigCat3MuMuRun2(5)+") || (TCKCat == 6 && "+getTrigCat3MuMuRun2(6)+") || (TCKCat == 7 && "+getTrigCat3MuMuRun2(7)+") || (TCKCat == 8 && "+getTrigCat3MuMuRun2(8)+") || (TCKCat == 9 && "+getTrigCat3MuMuRun2(9)+"))"; 
def getTrigCat4MuMuRun2(TCKCat=None):
	# hTOS inclusive
	if TCKCat is not None: return "("+getHltCutMuMuRun2(TCKCat)+" && "+getL0HadronTOS(TCKCat, 3500)+")";
	else: return "((TCKCat == 0 && "+getTrigCat4MuMuRun2(0)+") || (TCKCat == 1 && "+getTrigCat4MuMuRun2(1)+") || (TCKCat == 2 && "+getTrigCat4MuMuRun2(2)+") || (TCKCat == 3 && "+getTrigCat4MuMuRun2(3)+") || (TCKCat == 4 && "+getTrigCat4MuMuRun2(4)+") || (TCKCat == 5 && "+getTrigCat4MuMuRun2(5)+") || (TCKCat == 6 && "+getTrigCat4MuMuRun2(6)+") || (TCKCat == 7 && "+getTrigCat4MuMuRun2(7)+") || (TCKCat == 8 && "+getTrigCat4MuMuRun2(8)+") || (TCKCat == 9 && "+getTrigCat4MuMuRun2(9)+"))"; 
def getTrigCat5MuMuRun2(TCKCat=None):
	# muTOS inclusive + HltPhys1 and 2
	if TCKCat is not None: return "("+getL0MuonTOS(TCKCat)+" && B_plus_Hlt1Phys_TIS && B_plus_Hlt2Phys_TIS)";
	else: return "((TCKCat == 0 && "+getTrigCat5MuMuRun2(0)+") || (TCKCat == 1 && "+getTrigCat5MuMuRun2(1)+") || (TCKCat == 2 && "+getTrigCat5MuMuRun2(2)+") || (TCKCat == 3 && "+getTrigCat5MuMuRun2(3)+") || (TCKCat == 4 && "+getTrigCat5MuMuRun2(4)+") || (TCKCat == 5 && "+getTrigCat5MuMuRun2(5)+") || (TCKCat == 6 && "+getTrigCat5MuMuRun2(6)+") || (TCKCat == 7 && "+getTrigCat5MuMuRun2(7)+") || (TCKCat == 8 && "+getTrigCat5MuMuRun2(8)+") || (TCKCat == 9 && "+getTrigCat5MuMuRun2(9)+"))"; 
def getTrigCat6MuMuRun2(TCKCat=None):
	# hTOS inclusive + HltPhys1 and 2
	if TCKCat is not None: return "("+getL0HadronTOS(TCKCat)+" && B_plus_Hlt1Phys_TIS && B_plus_Hlt2Phys_TIS)";
	else: return "((TCKCat == 0 && "+getTrigCat6MuMuRun2(0)+") || (TCKCat == 1 && "+getTrigCat6MuMuRun2(1)+") || (TCKCat == 2 && "+getTrigCat6MuMuRun2(2)+") || (TCKCat == 3 && "+getTrigCat6MuMuRun2(3)+") || (TCKCat == 4 && "+getTrigCat6MuMuRun2(4)+") || (TCKCat == 5 && "+getTrigCat6MuMuRun2(5)+") || (TCKCat == 6 && "+getTrigCat6MuMuRun2(6)+") || (TCKCat == 7 && "+getTrigCat6MuMuRun2(7)+") || (TCKCat == 8 && "+getTrigCat6MuMuRun2(8)+") || (TCKCat == 9 && "+getTrigCat6MuMuRun2(9)+"))"; 
def getTrigCat7MuMuRun2(TCKCat=None):
	# TIS inclusive + HltPhys1 and 2
	if TCKCat is not None:return "("+getL0TIS(TCKCat)+" && B_plus_Hlt1Phys_TIS && B_plus_Hlt2Phys_TIS)";
	else: return "((TCKCat == 0 && "+getTrigCat7MuMuRun2(0)+") || (TCKCat == 1 && "+getTrigCat7MuMuRun2(1)+") || (TCKCat == 2 && "+getTrigCat7MuMuRun2(2)+") || (TCKCat == 3 && "+getTrigCat7MuMuRun2(3)+") || (TCKCat == 4 && "+getTrigCat7MuMuRun2(4)+") || (TCKCat == 5 && "+getTrigCat7MuMuRun2(5)+") || (TCKCat == 6 && "+getTrigCat7MuMuRun2(6)+") || (TCKCat == 7 && "+getTrigCat7MuMuRun2(7)+") || (TCKCat == 8 && "+getTrigCat7MuMuRun2(8)+") || (TCKCat == 9 && "+getTrigCat7MuMuRun2(9)+"))"; 
def getTrigCat8MuMuRun2(TCKCat=None):
	# TOS exclusive 
	if TCKCat is not None: return "("+getHltCutMuMuRun2(TCKCat)+" && "+getL0MuonTOS(TCKCat, 800)+" && !("+getL0TIS(TCKCat)+")";
	else: return "((TCKCat == 0 && "+getTrigCat8MuMuRun2(0)+") || (TCKCat == 1 && "+getTrigCat8MuMuRun2(1)+") || (TCKCat == 2 && "+getTrigCat8MuMuRun2(2)+") || (TCKCat == 3 && "+getTrigCat8MuMuRun2(3)+") || (TCKCat == 4 && "+getTrigCat8MuMuRun2(4)+") || (TCKCat == 5 && "+getTrigCat8MuMuRun2(5)+") || (TCKCat == 6 && "+getTrigCat8MuMuRun2(6)+") || (TCKCat == 7 && "+getTrigCat8MuMuRun2(7)+") || (TCKCat == 8 && "+getTrigCat8MuMuRun2(8)+") || (TCKCat == 9 && "+getTrigCat8MuMuRun2(9)+"))"; 


def getTrigCat0KemuRun2(TCKCat=None): 
	if TCKCat is not None:
		thresh = 2700;
		if (TCKCat > 3): thresh = 2955;
		if(TCKCat == 9): thresh = 3150;
		return "("+ getFullHltRun2(TCKCat) +" && (( e_plus_L0ElectronDecision_TOS && e_plus_L0Calo_ECAL_realET > "+str(thresh)+") || ( e_minus_L0MuonDecision_TOS && e_minus_PT > 800)))";
	else: return "((TCKCat == 0 && "+getTrigCat0KemuRun2(0)+") || (TCKCat == 1 && "+getTrigCat0KemuRun2(1)+") || (TCKCat == 2 && "+getTrigCat0KemuRun2(2)+") || (TCKCat == 3 && "+getTrigCat0KemuRun2(3)+") || (TCKCat == 4 && "+getTrigCat0KemuRun2(4)+") || (TCKCat == 5 && "+getTrigCat0KemuRun2(5)+") || (TCKCat == 6 && "+getTrigCat0KemuRun2(6)+") || (TCKCat == 7 && "+getTrigCat0KemuRun2(7)+") || (TCKCat == 8 && "+getTrigCat0KemuRun2(8)+") || (TCKCat == 9 && "+getTrigCat0KemuRun2(9)+"))"; 
def getTrigCat1KemuRun2(TCKCat=None):
	if TCKCat is not None:
		thresh = 2700;
		if (TCKCat > 3): thresh = 2955;
		if(TCKCat == 9): thresh = 3150;
		return "("+getL0HadronTOS()+" && "+ getFullHltRun2(TCKCat) +" && K_Kst_L0Calo_HCAL_realET > 3500 && !(( e_plus_L0ElectronDecision_TOS && e_plus_L0Calo_ECAL_realET > "+i2s(thresh)+") || ( e_minus_L0MuonDecision_TOS && e_minus_PT > 800)))";
	else: return "((TCKCat == 0 && "+getTrigCat1KemuRun2(0)+") || (TCKCat == 1 && "+getTrigCat1KemuRun2(1)+") || (TCKCat == 2 && "+getTrigCat1KemuRun2(2)+") || (TCKCat == 3 && "+getTrigCat1KemuRun2(3)+") || (TCKCat == 4 && "+getTrigCat1KemuRun2(4)+") || (TCKCat == 5 && "+getTrigCat1KemuRun2(5)+") || (TCKCat == 6 && "+getTrigCat1KemuRun2(6)+") || (TCKCat == 7 && "+getTrigCat1KemuRun2(7)+") || (TCKCat == 8 && "+getTrigCat1KemuRun2(8)+") || (TCKCat == 9 && "+getTrigCat1KemuRun2(9)+"))"; 
def getTrigCat2KemuRun2(TCKCat=None):
	if TCKCat is not None:
		thresh = 2700;
		if (TCKCat > 3): thresh = 2955;
		if(TCKCat == 9): thresh = 3150;
		return "("+getL0TIS(TCKCat)+" && "+ getFullHltRun2(TCKCat) +" && !(( e_plus_L0ElectronDecision_TOS && e_plus_L0Calo_ECAL_realET > "+i2s(thresh)+") || ( e_minus_L0MuonDecision_TOS && e_minus_PT > 800)) && !("+getL0HadronTOS()+" && K_Kst_L0Calo_HCAL_realET > 3500))";
	else: return "((TCKCat == 0 && "+getTrigCat2KemuRun2(0)+") || (TCKCat == 1 && "+getTrigCat2KemuRun2(1)+") || (TCKCat == 2 && "+getTrigCat2KemuRun2(2)+") || (TCKCat == 3 && "+getTrigCat2KemuRun2(3)+") || (TCKCat == 4 && "+getTrigCat2KemuRun2(4)+") || (TCKCat == 5 && "+getTrigCat2KemuRun2(5)+") || (TCKCat == 6 && "+getTrigCat2KemuRun2(6)+") || (TCKCat == 7 && "+getTrigCat2KemuRun2(7)+") || (TCKCat == 8 && "+getTrigCat2KemuRun2(8)+") || (TCKCat == 9 && "+getTrigCat2KemuRun2(9)+"))"; 
def getTrigCat3KemuRun2(TCKCat=None):
	if TCKCat is not None:
		return "("+getL0TIS(TCKCat)+" && "+ getFullHltRun2(TCKCat) +")";
	else: return "((TCKCat == 0 && "+getTrigCat3KemuRun2(0)+") || (TCKCat == 1 && "+getTrigCat3KemuRun2(1)+") || (TCKCat == 2 && "+getTrigCat3KemuRun2(2)+") || (TCKCat == 3 && "+getTrigCat3KemuRun2(3)+") || (TCKCat == 4 && "+getTrigCat3KemuRun2(4)+") || (TCKCat == 5 && "+getTrigCat3KemuRun2(5)+") || (TCKCat == 6 && "+getTrigCat3KemuRun2(6)+") || (TCKCat == 7 && "+getTrigCat3KemuRun2(7)+") || (TCKCat == 8 && "+getTrigCat3KemuRun2(8)+") || (TCKCat == 9 && "+getTrigCat3KemuRun2(9)+"))"; 
def getTrigCat4KemuRun2(TCKCat=None):
	if TCKCat is not None:
		return "("+getL0HadronTOS()+" && "+  getFullHltRun2(TCKCat)  +" && K_Kst_L0Calo_HCAL_realET > 3500)";
	else: return "((TCKCat == 0 && "+getTrigCat4KemuRun2(0)+") || (TCKCat == 1 && "+getTrigCat4KemuRun2(1)+") || (TCKCat == 2 && "+getTrigCat4KemuRun2(2)+") || (TCKCat == 3 && "+getTrigCat4KemuRun2(3)+") || (TCKCat == 4 && "+getTrigCat4KemuRun2(4)+") || (TCKCat == 5 && "+getTrigCat4KemuRun2(5)+") || (TCKCat == 6 && "+getTrigCat4KemuRun2(6)+") || (TCKCat == 7 && "+getTrigCat4KemuRun2(7)+") || (TCKCat == 8 && "+getTrigCat4KemuRun2(8)+") || (TCKCat == 9 && "+getTrigCat4KemuRun2(9)+"))"; 



def getStripping20Cuts():

	return "(B_plus_M < 5880 && B_plus_M > 4680 && K_Kst_PT > 800 && e_plus_IPCHI2_OWNPV > 16 && e_minus_IPCHI2_OWNPV > 16 && e_plus_PT > 500 && e_minus_PT > 500 && e_plus_PIDe>1 && e_minus_PIDe>1)";



def getKemuPreselRun2():

	cuts = "";
	cuts += "(e_minus_PIDmu>0 && e_minus_isMuon == 1 && e_plus_PIDe>3 && e_plus_isMuon == 0";
	cuts+="&& K_Kst_TRACK_GhostProb<0.3 ";
	cuts+="&& e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3 ";
	cuts+="&& K_Kst_MC15TuneV1_ProbNNk>0.2 && K_Kst_PIDe<0 && !K_Kst_isMuon && K_Kst_PIDmu < 5";
	cuts+="&& Kemu_MKl>1885 && Kemu_MKl_l2pi>1885 ";

	cuts+="&& e_plus_hasCalo == 1 && e_plus_hasRich == 1 ";
	cuts+="&& K_Kst_hasRich == 1 ";
	cuts+="&& e_minus_hasCalo == 1 && e_minus_hasRich == 1 ";
	cuts+="&& (TMath::Abs(e_minus_L0Calo_ECAL_xProjection)>363.6 || TMath::Abs(e_minus_L0Calo_ECAL_yProjection)>282.6)"; #//trigger fiducial
	cuts+="&& (TMath::Abs(e_plus_L0Calo_ECAL_xProjection)>363.6 || TMath::Abs(e_plus_L0Calo_ECAL_yProjection)>282.6)";
	cuts+="&& nSPDHits < 450";
	cuts+="&& e_plus_PT > 500 && e_minus_PT > 500 && e_plus_P > 3000 && e_minus_P > 3000)"; #//PID fiducial
	# //cuts+="&& B_plus_M02>1885 && B_plus_M02_Subst0_mu2pi>1885 )";

	return cuts;



def getCommonPreselTrigMuMuRun2():

	return "(e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3 && e_plus_hasRich == 1 && e_minus_hasRich == 1 && K_Kst_hasRich==1  && K_Kst_InMuonAcc == 1 && e_plus_isMuon && e_minus_isMuon && nSPDHits<450 && Kemu_MKl_l2pi>1885 && B_plus_M02>1885  && abs(Kmu_M_K2mu -  3097.) > 60 && abs(Kmu_M_K2mu -  3686.) > 60  && e_plus_PIDmu>-3. && e_minus_PIDmu > -3 && e_plus_PT > 800 && e_minus_PT > 800 && !K_Kst_isMuon && K_Kst_MC15TuneV1_ProbNNk > 0.2 ) && (e_plus_L0MuonDecision_TOS || e_minus_L0MuonDecision_TOS) && (B_plus_Hlt1TrackMVADecision_TOS && (B_plus_Hlt2Topo2BodyDecision_TOS || B_plus_Hlt2Topo3BodyDecision_TOS || B_plus_Hlt2TopoMu2BodyDecision_TOS || B_plus_Hlt2TopoMu3BodyDecision_TOS))";



# # /**********
# # BDT STRINGS
# # ***********/

# def getBDTCutMuMu(int run, int trigCat)
# {
#    trigCat = 0;
#    // if(run == 2018) run = 2017;// Until 2018 BDT is optimised (and exists)
#    string BDTFileName("$DISKROOT/Rpsi2S/BDTCutStrings/BDTMuMuRun20"+i2s(run)+"TrigCat"+i2s(trigCat)+".dat"); 
#    return readSingleLineFile(BDTFileName);
# }


# def getBDTCutEE(int run, int trigCat)
# {
#    if(trigCat == 3) trigCat = 2;
#    if(trigCat == 4) trigCat = 1;
#    if(trigCat == 5) trigCat = 0;
#    if(trigCat == 6) trigCat = 1;
#    if(trigCat == 7) trigCat = 2;

#    string BDTFileName(""); 
#    if(run > 3){
#       BDTFileName = "$DISKROOT/Rpsi2S/BDTCutStrings/BDTEERun20"+i2s(run)+"TrigCat"+i2s(trigCat)+".dat"; 
#    }
#    if(run == 2 || run == 1){
#       BDTFileName = "$DISKROOT/Rpsi2S_legacy/BDTCutStrings/BDTEERun"+i2s(run)+"TrigCat"+i2s(trigCat)+".dat"; 
#    }

#    return readSingleLineFile(BDTFileName);
# }


# def getGhostCut(bool wantMuon)
# {
#    int lID(11);
#    if(wantMuon) lID = 13;

#    return "(B_plus_BKGCAT == 60 && e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3 && K_Kst_TRACK_GhostProb<0.3 && ( (TMath::Abs(e_plus_TRUEID) == "+i2s(lID)+" && TMath::Abs(e_minus_TRUEID) == "+i2s(lID)+") || (TMath::Abs(K_Kst_TRUEID) == 321 && TMath::Abs(e_minus_TRUEID) == "+i2s(lID)+") || (TMath::Abs(K_Kst_TRUEID) == 321 && TMath::Abs(e_plus_TRUEID) == "+i2s(lID)+") ) )";
# }


# /*****************
# * Other Run1 Cuts*
# ******************/
# // def getKemuPresel()
# // {
# // //   cuts += "((( abs(e_plus_ID) == 13 && e_plus_PIDmu>0 && e_plus_isMuon == 1 && e_minus_PIDe>3 && e_minus_isMuon == 0) || ( abs(e_minus_ID) == 13 && e_minus_PIDmu>0 && e_minus_isMuon == 1 && e_plus_PIDe>3 && e_plus_isMuon == 0 ))";
# //   // cuts += " && (J_psi_1S_M)>1000 && (J_psi_1S_M*J_psi_1S_M)<6000000 ";
# //    string cuts("");
# //    cuts += "(e_minus_PIDmu>0 && e_minus_isMuon == 1 && e_plus_PIDe>3 && e_plus_isMuon == 0";
# //    cuts+="&& K_Kst_TRACK_GhostProb<0.3 ";
# //    cuts+="&& e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3 ";
# //    cuts+="&& K_Kst_ProbNNk>0.2 && K_Kst_PIDe<0 && !K_Kst_isMuon && K_Kst_PIDmu < 5";
# //    //cuts+="&& B_plus_M02>1885 && B_plus_M02_Subst0_mu2pi>1885 )";
# //    cuts+="&& Kemu_MKl>1885 && Kemu_MKl_l2pi>1885 ";

# //    cuts+="&& e_plus_hasCalo == 1 && e_plus_hasRich == 1 ";
# //    cuts+="&& K_Kst_hasRich == 1 ";
# //    cuts+="&& e_minus_hasCalo == 1 && e_minus_hasRich == 1 ";
# //    cuts+="&& (TMath::Abs(e_minus_L0Calo_ECAL_xProjection)>363.6 || TMath::Abs(e_minus_L0Calo_ECAL_yProjection)>282.6)"; //trigger fiducial
# //    cuts+="&& (TMath::Abs(e_plus_L0Calo_ECAL_xProjection)>363.6 || TMath::Abs(e_plus_L0Calo_ECAL_yProjection)>282.6)";
# //    cuts+="&& nSPDHits < 600";
# //    cuts+="&& e_plus_PT > 500 && e_minus_PT > 500 && e_plus_P > 3000 && e_minus_P > 3000)"; //PID fiducial

# //    return cuts;
# // }



# // def getKemuPreselLoose()
# // {
# //    string cuts("");
# //    cuts += "(e_plus_PIDe>3";
# //    cuts+="&& K_Kst_TRACK_GhostProb<0.3 ";
# //    cuts+="&& e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3 ";
# //    cuts+="&& K_Kst_ProbNNk>0.2 && K_Kst_PIDe<0 ";
# //    cuts+="&& Kemu_MKl>1885 && Kemu_MKl_l2pi>1885 )";

# //    return cuts;
# // }

# // def getQ2P()
# // {
# //    return "(1.0e-6*(TMath::Power(B_plus_DTFM_VtxBMassConst_J_psi_1S_eminus_0_PE[0]+B_plus_DTFM_VtxBMassConst_J_psi_1S_eminus_PE[0],2)-TMath::Power(B_plus_DTFM_VtxBMassConst_J_psi_1S_eminus_0_PX[0]+B_plus_DTFM_VtxBMassConst_J_psi_1S_eminus_PX[0],2)-TMath::Power(B_plus_DTFM_VtxBMassConst_J_psi_1S_eminus_0_PY[0]+B_plus_DTFM_VtxBMassConst_J_psi_1S_eminus_PY[0],2)-TMath::Power(B_plus_DTFM_VtxBMassConst_J_psi_1S_eminus_0_PZ[0]+B_plus_DTFM_VtxBMassConst_J_psi_1S_eminus_PZ[0],2)))";
# // }


# // def getCtrlPresel()
# // {
# //    string cuts("("+getCommonPresel());
# //    cuts += " && (J_psi_1S_M*J_psi_1S_M)>6000000 && (J_psi_1S_M*J_psi_1S_M)<10080000 ";
# //    cuts += "&& B_plus_DTFM_M>5200 && B_plus_DTFM_M<5350 )";

# //    return cuts;
# // }


# // def getPrcPresel()
# // {
# //    string cuts("("+getCommonPresel());
# //    cuts += " && (J_psi_1S_M*J_psi_1S_M)>6000000 && (J_psi_1S_M*J_psi_1S_M)<10080000 ";
# //    cuts +=" && B_plus_DTFM_M<5200)";

# //    return cuts;
# // }


# // def getBkgPresel()
# // {
# //    string cuts("("+getCommonPresel());
# //    cuts += "&& (J_psi_1S_M*J_psi_1S_M)>1.1e6 && (J_psi_1S_M*J_psi_1S_M)<6000000 ";
# //    cuts += "&& B_plus_M>5400) ";

# //    return cuts;
# // }

# // def getBkgNoPidPresel()
# // {
# //    string cuts("("+getCommonPreselNoPidCut());
# //    cuts += "&& (J_psi_1S_M*J_psi_1S_M)>1.1e6 && (J_psi_1S_M*J_psi_1S_M)<6000000 ";
# //    cuts += "&& B_plus_M>5400) ";

# //    return cuts;
# // }

# // def getPieePresel()
# // {
# //    string cuts_piee("(K_Kst_TRACK_GhostProb<0.3 ");
# //    cuts_piee+="&& e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3 ";
# //    cuts_piee+="&& K_Kst_ProbNNk<0.2 && K_Kst_PIDe<0";
# //    cuts_piee+="&& e_plus_PIDe>3 && e_minus_PIDe>3";
# //    //cuts_piee+="&& B_plus_M02_Subst0_e2pi>1885 && B_plus_M02>1885 ";
# //    //cuts_piee+="&& Kemu_TRACK_MKl_l2pi > 1885 && Kemu_TRACK_MKl_l2pi > 1885";
# //    cuts_piee+="&& Kemu_MKl > 1885 && (Kemu_TRACK_MKl_l2pi < 1825 || Kemu_TRACK_MKl_l2pi > 1905) ";
# //    cuts_piee+="&& (J_psi_1S_M*J_psi_1S_M)>1.1e6 && (J_psi_1S_M*J_psi_1S_M)<6000000";
# //    cuts_piee+="&& K_Kst_ProbNNpi> 0.6 && K_Kst_ProbNNk < 0.15)";

# //    return cuts_piee;
# // }

# // def getPieeNoWindowPresel()
# // {
# //    string cuts_piee("(K_Kst_TRACK_GhostProb<0.3 ");
# //    cuts_piee+="&& e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3 ";
# //    cuts_piee+="&& K_Kst_ProbNNk<0.2 && K_Kst_PIDe<0";
# //    cuts_piee+="&& e_plus_PIDe>3 && e_minus_PIDe>3";
# //    //cuts_piee+="&& B_plus_M02_Subst0_e2pi>1885 && B_plus_M02>1885) ";
# //    //cuts_piee+="&& Kemu_TRACK_MKl_l2pi > 1885 && Kemu_TRACK_MKl_l2pi > 1885)";
# //    cuts_piee+="&& Kemu_MKl > 1885 && (Kemu_TRACK_MKl_l2pi < 1825 || Kemu_TRACK_MKl_l2pi > 1905)) ";

# //    return cuts_piee;
# // }

# // def getMarieHeleneCut(double hopshift)
# // {
# //    return "(B_plus_HOP_M>("+d2s(hopshift+4000)+")+100*TMath::Log(B_plus_FDCHI2_OWNPV))";
# //    return "(B_plus_HOP_M>(4160)+100*TMath::Log(B_plus_FDCHI2_OWNPV))";
# // }

# // def getCommonPreselTrigMuMu()
# // {
# //     return "(e_plus_TRACK_GhostProb<0.3 && e_minus_TRACK_GhostProb<0.3 && e_plus_hasRich == 1 && e_minus_hasRich == 1 && K_Kst_hasRich==1  && K_Kst_InMuonAcc == 1 && e_plus_isMuon && e_minus_isMuon && nSPDHits<450 && Kemu_MKl_l2pi>1885 && B_plus_M02>1885  && abs(Kmu_M_K2mu -  3097.) > 60 && abs(Kmu_M_K2mu -  3686.) > 60  && e_plus_PIDmu>-3. && e_minus_PIDmu > -3 && e_plus_PT > 800 && e_minus_PT > 800 && !K_Kst_isMuon && K_Kst_ProbNNk > 0.2 ) && (e_plus_L0MuonDecision_TOS || e_minus_L0MuonDecision_TOS) && ((B_plus_Hlt1TrackMuonDecision_TOS || B_plus_Hlt1TrackAllL0Decision_TOS) && (B_plus_Hlt2Topo2BodyBBDTDecision_TOS || B_plus_Hlt2Topo3BodyBBDTDecision_TOS ||B_plus_Hlt2TopoMu2BodyBBDTDecision_TOS || B_plus_Hlt2TopoMu3BodyBBDTDecision_TOS || B_plus_Hlt2DiMuonDecision_TOS))";
# // }

