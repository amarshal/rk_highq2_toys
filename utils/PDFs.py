import zfit
import zfit_physics as zphys
import zfit.z.numpy as znp
import __main__

def meanShift_fn(mean, shift):
	return mean+shift
def sigmaScale_fn(sigma, scale):
	return sigma*scale
def compound_frac_fn(first_frac, second_frac):
	return (1-first_frac)*second_frac

def build_sig_model(pars, obs, run_string = '', trigger_string = '', extra_string = ''):

	sub_sample_string = '_%s_%s%s'%(run_string,trigger_string,extra_string)

	for nPhotons in [0, 1, 2]:
		pars["mean"+str(nPhotons)+"_sig%s"%(sub_sample_string)] = zfit.Parameter("mean"+str(nPhotons)+"_sig%s"%(sub_sample_string), 5280, 5000, 5500)
		pars["sigma"+str(nPhotons)+"_sig%s"%(sub_sample_string)] = zfit.Parameter("sigma"+str(nPhotons)+"_sig%s"%(sub_sample_string), 25, 7, 100)
		pars["sigma2"+str(nPhotons)+"_sig%s"%(sub_sample_string)] = zfit.Parameter("sigma2"+str(nPhotons)+"_sig%s"%(sub_sample_string), 50, 7, 300)

		pars["shiftedMean"+str(nPhotons)+"_sig%s"%(sub_sample_string)] = zfit.ComposedParameter("shiftedMean"+str(nPhotons)+"_sig%s"%(sub_sample_string),meanShift_fn,params=[pars["mean"+str(nPhotons)+"_sig%s"%(sub_sample_string)], pars["meanShift"]])
		pars["scaledSigma"+str(nPhotons)+"_sig%s"%(sub_sample_string)] = zfit.ComposedParameter("scaledSigma"+str(nPhotons)+"_sig%s"%(sub_sample_string),sigmaScale_fn,params=[pars["sigma"+str(nPhotons)+"_sig%s"%(sub_sample_string)], pars["sigmaScale"]])
		pars["scaledSigma2"+str(nPhotons)+"_sig%s"%(sub_sample_string)] = zfit.ComposedParameter("scaledSigma2"+str(nPhotons)+"_sig%s"%(sub_sample_string),sigmaScale_fn,params=[pars["sigma2"+str(nPhotons)+"_sig%s"%(sub_sample_string)], pars["sigmaScale"]])

		pars["alpha_right_"+str(nPhotons)+"_sig%s"%(sub_sample_string)] = zfit.Parameter("alpha_right_"+str(nPhotons)+"_sig%s"%(sub_sample_string), .1, .01, 2)
		pars["alpha_left_"+str(nPhotons)+"_sig%s"%(sub_sample_string)] = zfit.Parameter("alpha_left_"+str(nPhotons)+"_sig%s"%(sub_sample_string), -.1, -2, -.01)
		pars["n_left_"+str(nPhotons)+"_sig%s"%(sub_sample_string)] = zfit.Parameter("n_left_"+str(nPhotons)+"_sig%s"%(sub_sample_string), 7, 2, 20)
		pars["n_right_"+str(nPhotons)+"_sig%s"%(sub_sample_string)] = zfit.Parameter("n_right_"+str(nPhotons)+"_sig%s"%(sub_sample_string), 7, 2, 20)
		pars["fracCB"+str(nPhotons)+"_sig%s"%(sub_sample_string)] = zfit.Parameter("fracCB"+str(nPhotons)+"_sig%s"%(sub_sample_string), 0.66, 0.00001, 0.99999)

	nPhotons = 0
	cb10 = zfit.pdf.CrystalBall(obs=obs, mu=pars["shiftedMean"+str(nPhotons)+"_sig%s"%(sub_sample_string)], sigma=pars["scaledSigma"+str(nPhotons)+"_sig%s"%(sub_sample_string)], alpha=pars["alpha_right_"+str(nPhotons)+"_sig%s"%(sub_sample_string)], n=pars["n_right_"+str(nPhotons)+"_sig%s"%(sub_sample_string)])
	cb20 = zfit.pdf.CrystalBall(obs=obs, mu=pars["shiftedMean"+str(nPhotons)+"_sig%s"%(sub_sample_string)], sigma=pars["scaledSigma2"+str(nPhotons)+"_sig%s"%(sub_sample_string)], alpha=pars["alpha_left_"+str(nPhotons)+"_sig%s"%(sub_sample_string)], n=pars["n_left_"+str(nPhotons)+"_sig%s"%(sub_sample_string)])
	sigModel0 = zfit.pdf.SumPDF([cb10, cb20], [pars["fracCB"+str(nPhotons)+"_sig%s"%(sub_sample_string)]])

	for nPhotons in [1,2]:
		pars["sigmaGaus3"+str(nPhotons)+"_sig%s"%(sub_sample_string)] = zfit.Parameter("sigmaGaus3"+str(nPhotons)+"_sig%s"%(sub_sample_string),100, 10 , 2000)
		pars["scaledSigma3"+str(nPhotons)+"_sig%s"%(sub_sample_string)] = zfit.ComposedParameter("scaledSigma3"+str(nPhotons)+"_sig%s"%(sub_sample_string),sigmaScale_fn,params=[pars["sigmaGaus3"+str(nPhotons)+"_sig%s"%(sub_sample_string)], pars["sigmaScale"]])
		pars["fracGaus"+str(nPhotons)+"_sig%s"%(sub_sample_string)] = zfit.Parameter("fracGaus"+str(nPhotons)+"_sig%s"%(sub_sample_string), 0.50, 0.0001, 0.999)

	nPhotons = 1
	cb11 = zfit.pdf.CrystalBall(obs=obs, mu=pars["shiftedMean"+str(nPhotons)+"_sig%s"%(sub_sample_string)], sigma=pars["scaledSigma"+str(nPhotons)+"_sig%s"%(sub_sample_string)], alpha=pars["alpha_right_"+str(nPhotons)+"_sig%s"%(sub_sample_string)], n=pars["n_right_"+str(nPhotons)+"_sig%s"%(sub_sample_string)])
	cb21 = zfit.pdf.CrystalBall(obs=obs, mu=pars["shiftedMean"+str(nPhotons)+"_sig%s"%(sub_sample_string)], sigma=pars["scaledSigma2"+str(nPhotons)+"_sig%s"%(sub_sample_string)], alpha=pars["alpha_left_"+str(nPhotons)+"_sig%s"%(sub_sample_string)], n=pars["n_left_"+str(nPhotons)+"_sig%s"%(sub_sample_string)])
	sigGaus1 = zfit.pdf.Gauss(obs=obs, mu=pars["shiftedMean"+str(nPhotons)+"_sig%s"%(sub_sample_string)], sigma=pars["scaledSigma3"+str(nPhotons)+"_sig%s"%(sub_sample_string)])
	sigModelCB1= zfit.pdf.SumPDF([cb11, cb21], [pars["fracCB"+str(nPhotons)+"_sig%s"%(sub_sample_string)]])
	sigModel1 = zfit.pdf.SumPDF([sigModelCB1,sigGaus1],[pars["fracGaus"+str(nPhotons)+"_sig%s"%(sub_sample_string)]])

	nPhotons = 2
	cb12 = zfit.pdf.CrystalBall(obs=obs, mu=pars["shiftedMean"+str(nPhotons)+"_sig%s"%(sub_sample_string)], sigma=pars["scaledSigma"+str(nPhotons)+"_sig%s"%(sub_sample_string)], alpha=pars["alpha_right_"+str(nPhotons)+"_sig%s"%(sub_sample_string)], n=pars["n_right_"+str(nPhotons)+"_sig%s"%(sub_sample_string)])
	cb22 = zfit.pdf.CrystalBall(obs=obs, mu=pars["shiftedMean"+str(nPhotons)+"_sig%s"%(sub_sample_string)], sigma=pars["scaledSigma2"+str(nPhotons)+"_sig%s"%(sub_sample_string)], alpha=pars["alpha_left_"+str(nPhotons)+"_sig%s"%(sub_sample_string)], n=pars["n_left_"+str(nPhotons)+"_sig%s"%(sub_sample_string)])
	sigGaus2 = zfit.pdf.Gauss(obs=obs, mu=pars["shiftedMean"+str(nPhotons)+"_sig%s"%(sub_sample_string)], sigma=pars["scaledSigma3"+str(nPhotons)+"_sig%s"%(sub_sample_string)])
	sigModelCB2 = zfit.pdf.SumPDF([cb12, cb22], [pars["fracCB"+str(nPhotons)+"_sig%s"%(sub_sample_string)]])
	sigModel2 = zfit.pdf.SumPDF([sigModelCB2,sigGaus2],[pars["fracGaus"+str(nPhotons)+"_sig%s"%(sub_sample_string)]])

	pars["frac0gamma_sig%s"%(sub_sample_string)] = zfit.Parameter("frac0gamma_sig%s"%(sub_sample_string), pars['f0g_%s%s'%(trigger_string,extra_string)].read_value().numpy(), 0, 1, floating=True)
	pars["frac1_COMPOUND_gamma_sig%s"%(sub_sample_string)] = zfit.Parameter("frac1_COMPOUND_gamma_sig%s"%(sub_sample_string), pars['f1g_%s%s'%(trigger_string,extra_string)].read_value().numpy()/(1-pars['f0g_%s%s'%(trigger_string,extra_string)].read_value().numpy()), 0, 1, floating=True)
	pars["frac1gamma_sig%s"%(sub_sample_string)] =  zfit.ComposedParameter("frac1gamma_sig%s"%(sub_sample_string),compound_frac_fn,params=[pars["frac0gamma_sig%s"%(sub_sample_string)], pars["frac1_COMPOUND_gamma_sig%s"%(sub_sample_string)]])
		

	try:
		sigModelTot = zfit.pdf.SumPDF([sigModel0, sigModel1, sigModel2], [pars["frac0gamma_sig%s"%(sub_sample_string)], pars["frac1gamma_sig%s"%(sub_sample_string)]])
	except:
		sigModelTot = zfit.pdf.SumPDF([sigModel0, sigModel1, sigModel2], [pars["frac0gamma_sig%s"%(sub_sample_string)], 1.-pars["frac0gamma_sig%s"%(sub_sample_string)]]) # precision error possible if nothing in brem2 category

	return sigModel0, sigModel1, sigModel2, sigModelTot



class ArgusExponential(zfit.pdf.BaseFunctor):
	def __init__(self, m0, c, p, q2cut, lam, obs):
		"""Argus mixed with an Exponential shape

		Args:
			m0: Argus parameter
			c: Argus parameter
			p: Argus parameter
			q2cut: q2(track) cut in GeV2. Should be around 10 to 20
			lam: Exponential parameter
			obs: Input observable
		"""	
		kaon_mass_gev = 0.493
		shift = q2cut ** 0.5 + kaon_mass_gev
		shift *= 1000
		self._shift = shift
		shifted_obs = zfit.Space(obs.obs, (max(obs.lower[0] - shift, 0), obs.upper[0] - shift))
		argus = zphys.pdf.Argus(m0=m0, c=c, p=p, obs=shifted_obs)
		exp = zfit.pdf.Exponential(obs=shifted_obs, lam=lam)
		pdfs = [
			argus,
			exp
		]
		super().__init__(pdfs=pdfs, name="MirroredArgus", obs=obs)

	def _unnormalized_pdf(self, x):
		shift = self._shift
		# print(x.numpy(), shift)
		x = zfit.z.unstack_x(x)
		x -= shift
		# print(shift, self.space.upper[0])
		scale = x / (self.space.upper[0] - shift)
		scale = znp.maximum(0., scale)
		scale = znp.minimum(1., scale)
		# scale = tf.round(scale)
		scale **= 2  # maybe also not good, can be tuned
		exp = self.pdfs[1].pdf(x)
		argus = self.pdfs[0].pdf(x)
		# print(x,scale)
		# quit()
		mixture = argus * (1. - scale) + exp * scale

		return mixture

def build_combi_model(pars, obs, run_string = '', trigger_string = '', extra_string = '', option = 'Exponential'):

	sub_sample_string = '_%s_%s%s'%(run_string,trigger_string,extra_string)

	if option == 'Exponential':

		pars['lambda_combi%s'%(sub_sample_string)] = zfit.Parameter('lambda_combi%s'%(sub_sample_string), -1e-3, -0.1, 0.01)
		comb_bkg = zfit.pdf.Exponential(pars['lambda_combi%s'%(sub_sample_string)], obs=obs)
		pars['bkg_yield_combi%s'%(sub_sample_string)] = zfit.Parameter('bkg_yield_combi%s'%(sub_sample_string), 2000, 0, 100000)
		comb_bkg_ext = comb_bkg.create_extended(pars['bkg_yield_combi%s'%(sub_sample_string)])

	elif option == 'ArgusExponential':

		pars['combiargus_lam2%s'%(sub_sample_string)] = zfit.Parameter('combiargus_lam2%s'%(sub_sample_string), -0.0005705, -1., 1., floating=True) # -0.0005705  +/- 3.9e-05 
		pars['combiargus_m0%s'%(sub_sample_string)] = zfit.Parameter('combiargus_m0%s'%(sub_sample_string), 7500, floating=False)
		# pars['combiargus_c%s'%(sub_sample_string)] = zfit.Parameter('combiargus_c%s'%(sub_sample_string), 12.15, -40, 40, step_size=2.2)
		# pars['combiargus_p%s'%(sub_sample_string)] = zfit.Parameter('combiargus_p%s'%(sub_sample_string), -1.132, step_size=0.1)

		pars['combiargus_scale%s'%(sub_sample_string)] = zfit.Parameter('combiargus_scale%s'%(sub_sample_string), 0.5923, -10, 10, step_size=0.1) # 0.5923  +/-   0.034  
		pars['combiargus_p%s'%(sub_sample_string)] = zfit.ComposedParameter('combiargus_p%s'%(sub_sample_string), lambda params: params * 20, params=pars['combiargus_scale%s'%(sub_sample_string)])
		pars['combiargus_c%s'%(sub_sample_string)] = zfit.ComposedParameter('combiargus_c%s'%(sub_sample_string), lambda params: params * 1, params=pars['combiargus_scale%s'%(sub_sample_string)])

		comb_bkg_ext = ArgusExponential(pars['combiargus_m0%s'%(sub_sample_string)], pars['combiargus_c%s'%(sub_sample_string)], pars['combiargus_p%s'%(sub_sample_string)], 14.3, pars['combiargus_lam2%s'%(sub_sample_string)], obs)
		pars['bkg_yield_combi%s'%(sub_sample_string)] = zfit.Parameter('bkg_yield_combi%s'%(sub_sample_string), 2000, 0, 100000)
		comb_bkg_ext.set_yield(pars['bkg_yield_combi%s'%(sub_sample_string)]) # create_extended can fail for custom PDFs

	return comb_bkg_ext



def build_kuu_signal_model(pars, obs, run_string = '', trigger_string = '', extra_string = ''):

	sub_sample_string = '_%s_%s%s'%(run_string,trigger_string,extra_string)

	pars["mean_kuu%s"%(sub_sample_string)] = zfit.Parameter("mean_kuu%s"%(sub_sample_string), 5280, 5000, 5500)
	pars["sigmaGaus3_kuu%s"%(sub_sample_string)] = zfit.Parameter("sigmaGaus3_kuu%s"%(sub_sample_string),17.5, 10 , 2000)

	sigModel_kuu = zfit.pdf.Gauss(obs=obs, mu=pars["mean_kuu%s"%(sub_sample_string)], sigma=pars["sigmaGaus3_kuu%s"%(sub_sample_string)])

	return sigModel_kuu



def build_kuucombi_model(pars, obs, run_string = '', trigger_string = '', extra_string = ''):

	sub_sample_string = '_%s_%s%s'%(run_string,trigger_string,extra_string)

	pars['lambda_Kuucombi%s'%(sub_sample_string)] = zfit.Parameter('lambda_Kuucombi%s'%(sub_sample_string), -1e-5, -0.1, 0.01)
	Kuucomb_bkg = zfit.pdf.Exponential(pars['lambda_Kuucombi%s'%(sub_sample_string)], obs=obs)
	pars['bkg_yield_Kuucombi%s'%(sub_sample_string)] = zfit.Parameter('bkg_yield_Kuucombi%s'%(sub_sample_string), 2000, 0, 100000)
	Kuucomb_bkg_ext = Kuucomb_bkg.create_extended(pars['bkg_yield_Kuucombi%s'%(sub_sample_string)])

	return Kuucomb_bkg_ext


def build_prc_model(pars, obs, data_to_fit, run_string = '', trigger_string = '', extra_string = ''):
	part_reco = zfit.pdf.GaussianKDE1DimV1(obs=obs, data=data_to_fit, bandwidth='adaptive')
	return part_reco

def build_cas_model(pars, obs, data_to_fit, run_string = '', trigger_string = '', extra_string = ''):
	Cas_bkg_model = zfit.pdf.GaussianKDE1DimV1(obs=obs, data=data_to_fit, bandwidth=50)
	return Cas_bkg_model




def build_kpipi_model(pars, obs, run_string = '', trigger_string = '', extra_string = ''):

	sub_sample_string = '_%s_%s%s'%(run_string,trigger_string,extra_string)
	
	pars["mean_kpipi%s"%(sub_sample_string)] = zfit.Parameter("mean_kpipi%s"%(sub_sample_string), 5280, 5000, 5500)
	pars["sigma_kpipi%s"%(sub_sample_string)] = zfit.Parameter("sigma_kpipi%s"%(sub_sample_string), 50, 7, 100)
	pars["sigma2_kpipi%s"%(sub_sample_string)] = zfit.Parameter("sigma2_kpipi%s"%(sub_sample_string), 50, 7, 100)

	pars["alpha_right_kpipi%s"%(sub_sample_string)] = zfit.Parameter("alpha_right_kpipi%s"%(sub_sample_string), .1, .001, 2)
	pars["alpha_left_kpipi%s"%(sub_sample_string)] = zfit.Parameter("alpha_left_kpipi%s"%(sub_sample_string), -.1, -2, -.001)
	pars["n_left_kpipi%s"%(sub_sample_string)] = zfit.Parameter("n_left_kpipi%s"%(sub_sample_string), 7, 2, 25)
	pars["n_right_kpipi%s"%(sub_sample_string)] = zfit.Parameter("n_right_kpipi%s"%(sub_sample_string), 7, 2, 25)

	pars["sigmaGaus3_kpipi%s"%(sub_sample_string)] = zfit.Parameter("sigmaGaus3_kpipi%s"%(sub_sample_string),100, 10 , 2000)
	pars["fracGaus_kpipi%s"%(sub_sample_string)] = zfit.Parameter("fracGaus_kpipi%s"%(sub_sample_string), 0.50, 0.0001, 0.999)

	pars["fracCB_kpipi%s"%(sub_sample_string)] = zfit.Parameter("fracCB_kpipi%s"%(sub_sample_string), 0.66, 0.00001, 0.99999)
	cb1_kpipi = zfit.pdf.CrystalBall(obs=obs, mu=pars["mean_kpipi%s"%(sub_sample_string)], sigma=pars["sigma_kpipi%s"%(sub_sample_string)], alpha=pars["alpha_left_kpipi%s"%(sub_sample_string)], n=pars["n_left_kpipi%s"%(sub_sample_string)])
	cb2_kpipi = zfit.pdf.CrystalBall(obs=obs, mu=pars["mean_kpipi%s"%(sub_sample_string)], sigma=pars["sigma2_kpipi%s"%(sub_sample_string)], alpha=pars["alpha_right_kpipi%s"%(sub_sample_string)], n=pars["n_right_kpipi%s"%(sub_sample_string)])
	sigGaus_kpipi = zfit.pdf.Gauss(obs=obs, mu=pars["mean_kpipi%s"%(sub_sample_string)], sigma=pars["sigmaGaus3_kpipi%s"%(sub_sample_string)])

	sigModelCB_kpipi = zfit.pdf.SumPDF([cb1_kpipi, cb2_kpipi], [pars["fracCB_kpipi%s"%(sub_sample_string)]])
	sigModel_kpipi = zfit.pdf.SumPDF([sigModelCB_kpipi,sigGaus_kpipi],[pars["fracGaus_kpipi%s"%(sub_sample_string)]])

	return sigModel_kpipi





