import matplotlib.pyplot as plt
plt.rcParams["font.family"] = "DejaVu Sans"
from matplotlib import pylab
import zfit
from zfit import z
import numpy as np
import tensorflow as tf
# import mplhep
# mplhep.style.use('LHCb2')
import warnings
warnings.filterwarnings("ignore")

initial_plot = True

def plot_x_y_yerr(data, weights=None, bins=50, x_range=None,normalised=False):
    if weights is None: 
        weights = np.ones(np.shape(data))
    hist_i = np.histogram(data, bins=bins, range=x_range, weights=weights)
    x_points = hist_i[1][:-1] + (hist_i[1][1]-hist_i[1][0])/2.
    y_points = hist_i[0]
    yerr_points = np.sqrt(np.histogram(data, bins=bins, range=[np.amin(hist_i[1]),np.amax(hist_i[1])], weights=weights*weights)[0])
    y_counts = np.histogram(data, bins=bins, range=[np.amin(hist_i[1]),np.amax(hist_i[1])])[0]

    if normalised:
        sumy_points = np.sum(y_points)
        y_points = y_points/sumy_points
        yerr_points = yerr_points/sumy_points

    plt.errorbar(x_points, y_points, yerr=yerr_points,color='k',marker='o',fmt=' ',capsize=3,linewidth=0.75, markersize=5)

    return x_points, y_points, yerr_points, [np.amin(hist_i[1]),np.amax(hist_i[1])]

def plot_model(model, data, scale=1, plot_data=True, save_plot=False, hidey=True, fileName=None, label='', initial_plot = True):  # we will use scale later on
    
    if initial_plot: 
        plt.figure(figsize=(13,11))
        ax_pull = plt.axes([0.1, 0.18, 0.8, 0.7])

    nbins = 100
    data_plot = zfit.run(z.unstack_x(data))
    
    weights_set=None
    sumW=1.
    
    if data.has_weights:
        weights_set=data.weights.numpy()
        sumW=weights_set.sum()/len(weights_set)
    
    lower, upper = data.data_range.limit1d

    hist=np.histogram(data_plot, weights=weights_set, bins=nbins, range=[lower,upper])[1]
    binw=hist[1]-hist[0]
    
    n_evts=len(data_plot)
    
    scale2=1.
    if(data.space.limits!=model.space.limits): scale2=model.integrate(data.space.limits)

    x = tf.linspace(lower, upper, num=1000)  
    y = model.pdf(x)* n_evts * binw * sumW /scale2
    y *= scale
    if label != '': plt.plot(x, y, label=label)
    else: plt.plot(x, y)
      
    if plot_data:
        x_points, y_points, yerr_points, x_range_hist = plot_x_y_yerr(data_plot, weights_set, bins=nbins, x_range=[lower, upper]) 
        y_fit = model.pdf(x_points)* n_evts * binw * sumW /scale2 
        y_fit *= scale  
        pulls = (y_fit-y_points)/yerr_points
        plt.xlim(lower, upper)

        if hidey:
            frame=pylab.gca()
        if save_plot:
            plt.ylim(ymin=0)
            if label != '': plt.legend()
            xlim = plt.gca().get_xlim() 
            ax_pull = plt.axes([0.1, 0.015, 0.8, 0.07])
            colors = np.asarray(['#737373' for i in x_points])
            for pull_idx, pull in enumerate(pulls):
                if pull > 3.:
                    colors[pull_idx] = '#f5a742'
                if pull > 5.:
                    colors[pull_idx] = '#f54242'
            plt.bar(x_points, pulls,color=colors,width=((x_range_hist[1]-x_range_hist[0])/nbins),align='center',alpha=1.)
            plt.axhline(y=0,c='k',linewidth=0.5)
            plt.xlim(xlim[0], xlim[1])
            plt.ylim(-5, 5)
            plt.xticks([],[])
            plt.yticks([-5,0,5],[-5,0,5])
            plt.savefig(fileName+'.png', dpi=200)
            plt.clf()
        else:
            pylab.show()
        # plt.saveAs
    

def plot_comp_model(model, data, save_plot=None, fileName=None, labels=''):
    idx = -1
    label = ''
    label_full_model = ''
    for mod, frac in zip(model.pdfs, model.params.values()):
        idx += 1
        if labels != '':
            label = labels[idx]
            label_full_model = 'Total'
        if idx == 0: plot_model(mod, data, scale=frac, plot_data=False, label=label)
        else: plot_model(mod, data, scale=frac, plot_data=False, label=label, initial_plot = False)
    plot_model(model, data, plot_data=True, save_plot=save_plot, fileName=fileName, label=label_full_model, initial_plot = False)






def plot_model_combine_separate_run_periods(list_of_models, list_of_datas, scale=[1.,1.,1.], plot_data=True, save_plot=False, hidey=True, fileName=None, label='',initial_plot = True, xlabel=r'$m{K\ell\ell}$\;(MeV)'):  # we will use scale later on

    if initial_plot: 
        plt.figure(figsize=(13,11))
        ax_pull = plt.axes([0.1, 0.18, 0.8, 0.7]) #ALEX
    
    if scale == [1., 1., 1.]: scale = np.ones(len(list_of_models))

    nbins = 100
    for ii in range(0, len(list_of_datas)):
        if ii == 0:
            data_plot = zfit.run(z.unstack_x(list_of_datas[ii]))
        else:
            data_plot = np.concatenate((data_plot, zfit.run(z.unstack_x(list_of_datas[ii]))), axis=0)
    weights_set=None
    sumW=1.
    if list_of_datas[0].has_weights:
        for ii in range(0, len(list_of_datas)):
            if ii == 0:
                weights_set = list_of_datas[ii].weights.numpy()
            else:
                weights_set = np.concatenate((data_plot, list_of_datas[ii].weights.numpy()), axis=0)
        sumW=weights_set.sum()/len(weights_set)
    hist=np.histogram(data_plot, weights=weights_set, bins=nbins)[1]
    binw=hist[1]-hist[0]
    for ii in range(0, len(list_of_datas)):
        data = list_of_datas[ii]
        model = list_of_models[ii]
        data_plot = zfit.run(z.unstack_x(data))
        n_evts=len(data_plot)
        lower, upper = data.data_range.limit1d
        scale2=1.
        if(data.space.limits!=model.space.limits): scale2=model.integrate(data.space.limits)
        x = tf.linspace(lower, upper, num=1000)  
        y_i = model.pdf(x)* n_evts * binw * sumW /scale2
        if ii == 0:
            y = y_i*scale[ii]
        else:
             y += y_i*scale[ii]
    if label != '': plt.plot(x, y, label=label)
    else: plt.plot(x, y)

    if plot_data:
        for ii in range(0, len(list_of_datas)):
            if ii == 0:
                data_plot = zfit.run(z.unstack_x(list_of_datas[ii]))
            else:
                data_plot = np.concatenate((data_plot, zfit.run(z.unstack_x(list_of_datas[ii]))), axis=0)
        weights_set=None
        if list_of_datas[0].has_weights:
            for ii in range(0, len(list_of_datas)):
                if ii == 0:
                    weights_set = list_of_datas[ii].weights.numpy()
                else:
                    weights_set = np.concatenate((data_plot, list_of_datas[ii].weights.numpy()), axis=0)

        x_points, y_points, yerr_points, x_range_hist = plot_x_y_yerr(data_plot, weights_set, bins=nbins, x_range=[lower, upper])

        for ii in range(0, len(list_of_datas)):
            data = list_of_datas[ii]
            model = list_of_models[ii]
            data_plot = zfit.run(z.unstack_x(data))
            n_evts=len(data_plot)
            lower, upper = data.data_range.limit1d
            scale2=1.
            if(data.space.limits!=model.space.limits): scale2=model.integrate(data.space.limits)
            y_i = model.pdf(x_points)* n_evts * binw * sumW /scale2
            if ii == 0:
                y_fit = y_i*scale[ii]
            else:
                 y_fit += y_i*scale[ii]

        # y_fit = model.pdf(x_points)* n_evts * binw * sumW /scale2 
        pulls = (y_fit-y_points)/yerr_points
        plt.xlim(lower, upper) 
        if hidey:
            frame=pylab.gca()
        if save_plot:
            plt.ylim(ymin=0)
            if label != '': plt.legend()
            xlim = plt.gca().get_xlim() 
            plt.xlabel(xlabel, fontsize=8)

            ax_pull = plt.axes([0.1, 0.015, 0.8, 0.07])
            colors = np.asarray(['#737373' for i in x_points])
            for pull_idx, pull in enumerate(pulls):
                if pull > 3.:
                    colors[pull_idx] = '#f5a742'
                if pull > 5.:
                    colors[pull_idx] = '#f54242'

            plt.bar(x_points, pulls,color=colors,width=((x_range_hist[1]-x_range_hist[0])/nbins),align='center',alpha=1.)
            plt.axhline(y=0,c='k',linewidth=0.5)
            plt.xlim(xlim[0], xlim[1])
            plt.ylim(-5, 5)
            plt.xticks([],[])
            plt.yticks([-5,0,5],[-5,0,5])
            plt.savefig(fileName+'.png', dpi=200)
            plt.savefig(fileName+'.pdf', dpi=200)
            plt.clf()
        else:
            pylab.show()
        # plt.saveAs

def plot_comp_model_combine_separate_run_periods(list_of_models, list_of_datas, year_list, trigger_list, save_plot=None, fileName=None, labels='', xlabel=r'$m(K\ell\ell)$ (MeV)'):
    idx = -1
    label = ''
    label_full_model = ''
    full_list_of_mods = []
    full_list_of_fracs = []
    list_of_data_for_plot = []
    for data_key in list_of_datas:
        list_of_data_for_plot.append(list_of_datas[data_key])
    for model_key in list_of_models:
        list_of_mods = []
        list_of_fracs = []
        model = list_of_models[model_key]
        length = 0
        for model, frac in zip(model.pdfs, model.params.values()):
            list_of_mods.append(model)
            list_of_fracs.append(frac)
            length += 1
        full_list_of_mods.append(list_of_mods)
        full_list_of_fracs.append(list_of_fracs)
    for component in range(0, length):
        idx += 1
        if labels != '':
            label = labels[idx]
            label_full_model = 'Total'
        list_of_mods_for_plot = []
        list_of_fracs_for_plot = []
        for model_row in full_list_of_mods:
            list_of_mods_for_plot.append(model_row[component])
        for frac_row in full_list_of_fracs:
            list_of_fracs_for_plot.append(frac_row[component])
        if idx == 0: plot_model_combine_separate_run_periods(list_of_mods_for_plot, list_of_data_for_plot, scale=list_of_fracs_for_plot, plot_data=False, label=label)
        else: plot_model_combine_separate_run_periods(list_of_mods_for_plot, list_of_data_for_plot, scale=list_of_fracs_for_plot, plot_data=False, label=label, initial_plot = False)
    list_of_mods_for_plot = []
    for model_key in list_of_models:
        list_of_mods_for_plot.append(list_of_models[model_key])
    plot_model_combine_separate_run_periods(list_of_mods_for_plot, list_of_data_for_plot, plot_data=True, save_plot=save_plot, fileName=fileName, label=label_full_model, initial_plot = False, xlabel=xlabel)


def plot_model_combi(model, data, scale=1, plot_data=True, save_plot=False, fileName=None):  # we will use scale later on
    
    plt.figure(figsize=(13,11))
    ax_pull = plt.axes([0.1, 0.18, 0.8, 0.7]) #ALEX

    nbins = 100
    data_plot = zfit.run(z.unstack_x(data))
    
    hist=np.histogram(data_plot, bins=nbins)[1]
    binw=hist[1]-hist[0]
    
    scale=1./model.integrate(limits=(5400,6200))
    
    n_evts=len(data_plot)
    # for lim in data.data_range:
        # lower, upper = lim.limit1d
    lower = 4880
    upper = 6200
    x = tf.linspace(lower, upper, num=1000)  # np.linspace also works
    y = model.pdf(x)* n_evts * binw
    y *= scale
    plt.plot(x, y)
 
    if plot_data:
            # mplhep.histplot(plt.hist(data_plot, bins=nbins, facecolor="none"), 
            #     yerr=True, color='black', histtype='errorbar')

            x_points, y_points, yerr_points, x_range_hist = plot_x_y_yerr(data_plot, np.ones(np.shape(data_plot)), bins=nbins, x_range=[lower, upper])
            y_fit = model.pdf(x_points)* n_evts * binw
            y_fit *= scale  
            pulls = (y_fit-y_points)/yerr_points
            plt.xlim(lower, upper) 

            # frame = pylab.gca()
            # frame.axes.get_yaxis().set_ticks([])

            if save_plot:
                # plt.ylim(ymin=0)
                # plt.savefig(fileName, dpi=200, format='png')
                # plt.clf()
                plt.ylim(ymin=0)
                xlim = plt.gca().get_xlim() 
                ax_pull = plt.axes([0.1, 0.015, 0.8, 0.07])
                colors = np.asarray(['#737373' for i in x_points])
                for pull_idx, pull in enumerate(pulls):
                    if pull > 3.:
                        colors[pull_idx] = '#f5a742'
                    if pull > 5.:
                        colors[pull_idx] = '#f54242'
                plt.bar(x_points, pulls,color=colors,width=((x_range_hist[1]-x_range_hist[0])/nbins),align='center',alpha=1.)
                plt.axhline(y=0,c='k',linewidth=0.5)
                plt.xlim(xlim[0], xlim[1])
                plt.ylim(-5, 5)
                plt.xticks([],[])
                plt.yticks([-5,0,5],[-5,0,5])
                plt.savefig(fileName+'.png', dpi=200)
                plt.clf()
                
            else:
                pylab.show()

def plot_model_combi_nodata(model, scale=1, save_plot=False, fileName=None, xmin=5400, xmax=6200):  # we will use scale later on
    
    plt.figure(figsize=(13,11))
    ax_pull = plt.axes([0.1, 0.18, 0.8, 0.7]) #ALEX

    scale=1./model.integrate(limits=(xmin,xmax))
    
    x = tf.linspace(xmin, xmax, num=1000)  # np.linspace also works
    y = model.pdf(x)
    y *= scale
    plt.plot(x, y)
     
    plt.xlim(xmin, xmax) 

    if save_plot:

        plt.ylim(ymin=0)
        plt.savefig(fileName+'.png', dpi=200)
        plt.clf()
    else:
        pylab.show()


branches = branches = [
            "eventNumber",    
            "nSPDHits",
            "e_plus_PIDe",
            "e_minus_PIDe",
            "e_plus_P",
            "e_minus_P",
            "e_plus_PT",
            "e_minus_PT",
            "e_plus_IPCHI2_OWNPV",
            "e_minus_IPCHI2_OWNPV",
            "J_psi_1S_M",
            "B_plus_M",
            "B_plus_DTFM_M",
            "K_Kst_MC15TuneV1_ProbNNk",
            "K_Kst_PIDK",
            "K_Kst_TRACK_GhostProb",
            "e_plus_TRACK_GhostProb",
            "e_minus_TRACK_GhostProb",
            "e_plus_hasRich",
            "e_plus_hasCalo",
            "e_minus_hasRich",
            "e_minus_hasCalo",
            # "dataCondSPDWeight",
            "K_Kst_P",
            "K_Kst_PE",
            # "poissonWeight",
            # "nPoissonWeights",
            "e_minus_PT",
            "e_minus_P",
            "e_plus_PT",
            "e_plus_P",
            "*HasBremAdded*",
            "L0Data_Muon1_Pt",
            "Polarity",
            "*_TRACK_PT",
            "passTrigCat*",
            "J_psi_1S_TRACK_M",
            "*hasRich*",
            "*L0Calo_ECAL_*Projection",
            "*DLL*",
            "K_Kst_PIDe",
            "Kemu_MKl",
            "Kemu_TRACK_MKl_l2pi",
            "Kemu_MKl_l2pi",
            "B_plus_M02",
            "BDT*",
            "*KinWeight*",
            "B_plus_Hlt2Topo2*_TOS",
            "B_plus_Hlt2Topo3*_TOS",
            "*Hlt1TrackMVADecision_TOS",
            "*Hlt1TrackMVADecision_TOS",
            "*IPCHI2_OWNPV",
            "TCKCat",
            "*trigWeight*",
            "kpipi_correction",
            "*SwaveWeight*",
            "*BDT*",
            "PIDcalibeTOSWeight"
            ]





            