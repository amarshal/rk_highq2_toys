import uproot as ur
import zfit
from zfit import z
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import pylab

from utils.getPreselection import *
from utils.plotFunctions import *
from utils.config import *
from utils.definitions import *

import __main__




def print_toy_configuration():

	if truncate_trees: print('TREES WILL BE TRUNCATED (FOR TESTING)')
	if fix_signal_yield: print('SIGNAL YIELD WILL BE FIXED, to: %.2f'%fixed_signal_yield)
	if use_presel_tuples: print('Using pre-selected tuples to speed up initialisation of the toys...')

	print('Using the following trigger categories...')
	for trig_idx in use_trigger_categories:
		print('\t%s'%trigger_cat_names[trig_idx])

	if triggers == 'separate': print("Fitting trigger categories separately.")
	else: print("Fitting trigger categories together.")

	if years == 'separate': print("Fitting run periods separately.")
	elif years =='combined': print("Fitting run periods together.")
	else: print('Fitting only %s'%years)
	print('\nGenerating %d toy(s)...'%nToys)


def getEff(dataFrame, option, trigger='all'):

	if option == 'Kpipi': 
		try:
			totTruthed_weights=(dataFrame[__main__.kinematic_weight].to_numpy()*dataFrame[__main__.kpipi_correction].to_numpy()[:,1])
		except:
			totTruthed_weights=(dataFrame[__main__.kinematic_weight].to_numpy()*dataFrame[__main__.kpipi_correction].to_numpy())
	elif option == 'K*ee': 
		totTruthed_weights=(dataFrame[__main__.kinematic_weight].to_numpy()*dataFrame[__main__.SwaveWeight].to_numpy())
	else: 
		totTruthed_weights=(dataFrame[__main__.kinematic_weight].to_numpy())
	
	totTruthed = totTruthed_weights.sum()
	totTruthederr = np.sqrt(np.sum(totTruthed_weights**2))

	if __main__.use_kpipi_PIDcalib_weights:
		data_q2Presel = dataFrame.query(__main__.preselStringNoPid+" and "+__main__.q2Cut+" and "+__main__.massRangeCut+" and BDTGETOSRun3T>0.78")
	else:
		data_q2Presel = dataFrame.query(__main__.preselStringNoPid+" and "+__main__.q2Cut+" and "+__main__.massRangeCut+" and "+__main__.BDTCut)
		
	if trigger == 'all': trigger_weights = (data_q2Presel['passTrigCat0'].to_numpy()*data_q2Presel[__main__.trigger_weight_0].to_numpy()+data_q2Presel['passTrigCat1'].to_numpy()*data_q2Presel[__main__.trigger_weight_1].to_numpy()+data_q2Presel['passTrigCat2'].to_numpy()*data_q2Presel[__main__.trigger_weight_2].to_numpy())
	elif trigger == 'eTOS': trigger_weights = (data_q2Presel['passTrigCat0'].to_numpy()*data_q2Presel[__main__.trigger_weight_0].to_numpy())
	elif trigger == 'hTOS': trigger_weights = (data_q2Presel['passTrigCat1'].to_numpy()*data_q2Presel[__main__.trigger_weight_1].to_numpy())
	elif trigger == 'TIS': trigger_weights = (data_q2Presel['passTrigCat2'].to_numpy()*data_q2Presel[__main__.trigger_weight_2].to_numpy())

	if option == 'Kpipi': 
		if __main__.use_kpipi_PIDcalib_weights:
			try:
				totTrig0_weights = (data_q2Presel[__main__.PID_weight].to_numpy()*data_q2Presel[__main__.kinematic_weight].to_numpy()*data_q2Presel["PIDcalibeTOSWeight"].to_numpy()*data_q2Presel[__main__.kpipi_correction].to_numpy()[:,1])
			except:
				totTrig0_weights = (data_q2Presel[__main__.PID_weight].to_numpy()*data_q2Presel[__main__.kinematic_weight].to_numpy()*data_q2Presel["PIDcalibeTOSWeight"].to_numpy()*data_q2Presel[__main__.kpipi_correction].to_numpy())
		else:
			try:
				totTrig0_weights = (data_q2Presel[__main__.PID_weight].to_numpy()*data_q2Presel[__main__.kinematic_weight].to_numpy()*trigger_weights*data_q2Presel[__main__.kpipi_correction].to_numpy()[:,1])
			except:
				totTrig0_weights = (data_q2Presel[__main__.PID_weight].to_numpy()*data_q2Presel[__main__.kinematic_weight].to_numpy()*trigger_weights*data_q2Presel[__main__.kpipi_correction].to_numpy())
	elif option == 'K*ee': 
		totTrig0_weights = (data_q2Presel[__main__.PID_weight].to_numpy()*data_q2Presel[__main__.kinematic_weight].to_numpy()*trigger_weights*data_q2Presel[__main__.SwaveWeight].to_numpy())
	else: 
		totTrig0_weights = (data_q2Presel[__main__.PID_weight].to_numpy()*data_q2Presel[__main__.kinematic_weight].to_numpy()*trigger_weights)
	totTrig0= totTrig0_weights.sum()

	totTrig0err = np.sqrt(np.sum(totTrig0_weights**2))

	effTot = totTrig0/totTruthed

	effToterr = effTot * np.sqrt( (totTrig0err/totTrig0)**2 + (totTruthederr/totTruthed)**2 )


	return effTot*__main__.presel_eff[option], effToterr*__main__.presel_eff[option], totTrig0, totTrig0err

def share_params(mode, yearA, yearB):
	for parA in __main__.pars:
		if mode in parA and yearA in parA:
			try:
				parB = parA.replace(yearA, yearB)
				__main__.pars[parB].set_value(__main__.pars[parA].read_value().numpy())
			except:
				pass # 'ComposedParameter' object has no attribute 'set_value', no problem the other parameters change this automatically

def get_weights_from_df(df, mode, extra_weight, trigger):
	list_of_weights = [__main__.PID_weight, __main__.kinematic_weight, 'TRIGGER WEIGHTS']
	if mode == 'K*ee': list_of_weights.append(__main__.SwaveWeight)
	if mode == 'Kpipi' and __main__.use_kpipi_PIDcalib_weights: list_of_weights = [__main__.PID_weight, __main__.kinematic_weight, __main__.kpipi_correction, "PIDcalibeTOSWeight"] #list_of_weights.append(__main__.kpipi_correction)
	elif mode == 'Kpipi': list_of_weights.append(__main__.kpipi_correction)
	
	if trigger == 'all': trigger_weights = (df['passTrigCat0'].to_numpy()*df[__main__.trigger_weight_0].to_numpy()+df['passTrigCat1'].to_numpy()*df[__main__.trigger_weight_1].to_numpy()+df['passTrigCat2'].to_numpy()*df[__main__.trigger_weight_2].to_numpy())
	if trigger == 'eTOS': trigger_weights = (df['passTrigCat0'].to_numpy()*df[__main__.trigger_weight_0].to_numpy())
	if trigger == 'hTOS': trigger_weights = (df['passTrigCat1'].to_numpy()*df[__main__.trigger_weight_1].to_numpy())
	if trigger == 'TIS': trigger_weights = (df['passTrigCat2'].to_numpy()*df[__main__.trigger_weight_2].to_numpy())
	
	weights = 1.
	for weight in list_of_weights:
		if weight == 'TRIGGER WEIGHTS':
			weights *= trigger_weights
		else:
			try:
				weights *= df[weight].to_numpy()
			except:
				weights *= df[weight].to_numpy()[:,0]
	if 'Cas' in mode: weights = weights/np.sum(weights)
	return weights*extra_weight


def get_cas_extra_weight(mode, trigger):

	if 'Cas' not in mode: 
		return 1.

	effTotKee, err, countsTotKee, countserr = getEff(__main__.KeeMC, 'Kee', trigger)
	NKee=(__main__.NKee_bf_Run1*__main__.effStrip*0.66*effTotKee)+(__main__.NKee_bf_Run2*__main__.effStrip*effTotKee)

	if mode == 'Cas1':
		effTotCas1, err, countsTotCas1, countserr = getEff(__main__.Cas1MC,'Cas1', trigger)
		NCas1=NKee*(__main__.BFcas1*countsTotCas1*__main__.DecProd['Cas1']/__main__.Nsim['Cas1'])/(__main__.BFKee*countsTotKee*__main__.DecProd['Kee']/__main__.Nsim['Kee']);
		return NCas1

	if mode == 'Cas2':
		effTotCas2, err, countsTotCas2, countserr = getEff(__main__.Cas2MC,'Cas2', trigger)
		NCas2=NKee*(__main__.BFcas2*countsTotCas2*__main__.DecProd['Cas2']/__main__.Nsim['Cas2'])/(__main__.BFKee*countsTotKee*__main__.DecProd['Kee']/__main__.Nsim['Kee']);
		return NCas2

	if mode == 'Cas3':
		effTotCas3, err, countsTotCas3, countserr = getEff(__main__.Cas3MC,'Cas3', trigger)
		NCas3=NKee*(__main__.BFcas3*countsTotCas3*__main__.DecProd['Cas3']/__main__.Nsim['Cas3'])/(__main__.BFKee*countsTotKee*__main__.DecProd['Kee']/__main__.Nsim['Kee']);
		return NCas3

	if mode == 'Cas12':
		effTotCas12, err, countsTotCas12, countserr = getEff(__main__.Cas12MC,'Cas12', trigger)
		NCas12=NKee*(__main__.BFcas12*countsTotCas12*__main__.DecProd['Cas12']/__main__.Nsim['Cas12'])/(__main__.BFKee*countsTotKee*__main__.DecProd['Kee']/__main__.Nsim['Kee']);
		return NCas12

	if mode == 'Cas13':
		effTotCas13, err, countsTotCas13, countserr = getEff(__main__.Cas13MC,'Cas13', trigger)
		NCas13=NKee*(__main__.BFcas13*countsTotCas13*__main__.DecProd['Cas13']/__main__.Nsim['Cas13'])/(__main__.BFKee*countsTotKee*__main__.DecProd['Kee']/__main__.Nsim['Kee']);
		return NCas13


def get_MC_arrays(df, mode):

	# dictionary key convention: 'years_trigger_brem'
	# years: 0 (2012+11), 1 (2016), 2 (2017+18)
	# trigger: 0 (eTOS), 1 (hTOS), 2 (TIS)
	# brem: 0, 1, 2

	massRangeCut_i = __main__.massRangeCut
	if 'Cas' in mode or 'K*' in mode: massRangeCut_i = __main__.massRangeCutExt

	MC_dict = {}

	df_presel = df.query(__main__.preselStringNoPid)

	df_trigA = df_presel.query(__main__.q2Cut+" and "+massRangeCut_i+ " and "+__main__.BDTCut)
	MC_dict['all_all_all'] = [df_trigA['B_plus_M'].to_numpy(), get_weights_from_df(df_trigA, mode, get_cas_extra_weight(mode, 'all'), 'all')]

	if mode == 'Kee':
		for nPhotons in [0, 1, 2]:
			df_trigA_brem = df_trigA.query("(e_plus_HasBremAdded + e_minus_HasBremAdded) == %d"%nPhotons)
			MC_dict['all_all_%d'%nPhotons] = [df_trigA_brem['B_plus_M'].to_numpy(), get_weights_from_df(df_trigA_brem, mode, 1., 'all')]

	for trigger in use_trigger_categories:
		if mode == 'Kpipi' and __main__.use_kpipi_PIDcalib_weights: df_trigX = df_presel.query(__main__.q2Cut+" and "+massRangeCut_i+"and BDTGETOSRun3T>0.78")
		else: df_trigX = df_presel.query(__main__.q2Cut+" and "+massRangeCut_i+ " and "+__main__.BDTCut_trigger_cuts[trigger])
		MC_dict['all_%s_all'%trigger_cat_names[trigger]] = [df_trigX['B_plus_M'].to_numpy(), get_weights_from_df(df_trigX, mode, get_cas_extra_weight(mode, trigger_cat_names[trigger]), trigger_cat_names[trigger])]
		if mode == 'Kee':
			for nPhotons in [0, 1, 2]:
				df_trigX_brem = df_trigX.query("(e_plus_HasBremAdded + e_minus_HasBremAdded) == %d"%nPhotons)
				MC_dict['all_%s_%d'%(trigger_cat_names[trigger],nPhotons)] = [df_trigX_brem['B_plus_M'].to_numpy(), get_weights_from_df(df_trigX_brem, mode, 1., trigger_cat_names[trigger])]

	return MC_dict





def get_weights_from_df_kpipi_test(df, mode, extra_weight, trigger):
	list_of_weights = [__main__.PID_weight, __main__.kinematic_weight, 'TRIGGER WEIGHTS',__main__.kpipi_correction, 'BDT WEIGHTS']


	BDT_weight = np.zeros(np.shape(df['passTrigCat0'].to_numpy()))

	for i in range(0, np.shape(df['passTrigCat0'].to_numpy())[0]):

		if df['BDTGETOSRun3T'].to_numpy()[i] > 0.78:
			BDT_weight[i] = 1.


	if trigger == 'all': trigger_weights = (df['passTrigCat0'].to_numpy()*df[__main__.trigger_weight_0].to_numpy()+df['passTrigCat1'].to_numpy()*df[__main__.trigger_weight_1].to_numpy()+df['passTrigCat2'].to_numpy()*df[__main__.trigger_weight_2].to_numpy())
	if trigger == 'eTOS': trigger_weights = (df['passTrigCat0'].to_numpy()*df[__main__.trigger_weight_0].to_numpy())
	if trigger == 'hTOS': trigger_weights = (df['passTrigCat1'].to_numpy()*df[__main__.trigger_weight_1].to_numpy())
	if trigger == 'TIS': trigger_weights = (df['passTrigCat2'].to_numpy()*df[__main__.trigger_weight_2].to_numpy())
	
	if __main__.use_kpipi_PIDcalib_weights: trigger_weights = df['PIDcalibeTOSWeight'].to_numpy()

	weights = 1.
	for weight in list_of_weights:
		if weight == 'TRIGGER WEIGHTS':
			weights *= trigger_weights
		elif weight == 'BDT WEIGHTS':
			weights *= BDT_weight
		else:
			try:
				weights *= df[weight].to_numpy()
			except:
				weights *= df[weight].to_numpy()[:,0]
	if 'Cas' in mode: weights = weights/np.sum(weights)
	return weights*extra_weight, trigger_weights

def get_MC_arrays_kpipi_test(df, mode):

	massRangeCut_i = __main__.massRangeCut
	if 'Cas' in mode or 'K*' in mode: massRangeCut_i = __main__.massRangeCutExt

	MC_dict = {}

	df_presel = df.query(__main__.preselStringNoPid)

	df_trigA = df_presel.query(__main__.q2Cut+" and "+massRangeCut_i)

	full_weight, trigger_weight = get_weights_from_df_kpipi_test(df_trigA, mode, get_cas_extra_weight(mode, 'all'), 'all')
	MC_dict['all_all_all'] = [df_trigA['B_plus_M'].to_numpy(), full_weight, trigger_weight]

	# for trigger in use_trigger_categories:
	# 	df_trigX = df_presel.query(__main__.q2Cut+" and "+massRangeCut_i)
	# 	full_weight, trigger_weight = get_weights_from_df_kpipi_test(df_trigX, mode, get_cas_extra_weight(mode, trigger_cat_names[trigger]), trigger_cat_names[trigger])
	# 	MC_dict['all_%s_all'%trigger_cat_names[trigger]] = [df_trigX['B_plus_M'].to_numpy(), full_weight, trigger_weight]

	return MC_dict






def get_MC_arrays_data(df, mode):

	trigCut_list = ['passTrigCat0==1', 'passTrigCat1==1', 'passTrigCat2==1']
	Data_CommonPresel=df.query(__main__.preselString)
	MC_dict = {}

	for trigger in use_trigger_categories:
		if trigger_cat_names[trigger] == 'eTOS':
			trigCut= "(%s)"%trigCut_list[0]
		elif trigger_cat_names[trigger] == 'hTOS':
			trigCut= "(%s)"%trigCut_list[1]
		elif trigger_cat_names[trigger] == 'TIS':
			trigCut= "(%s)"%trigCut_list[2]
		Data_presel = Data_CommonPresel.query(__main__.q2Cut+" and "+trigCut+" and "+__main__.blindRangeCut+" and "+__main__.BDTCut_comb)
		MC_dict['all_%s_all'%trigger_cat_names[trigger]] = Data_presel['B_plus_M'].to_numpy()

	MC_dict['all_all_all'] = np.empty(0)
	for trigger in use_trigger_categories:
		MC_dict['all_all_all'] = np.append(MC_dict['all_all_all'], MC_dict['all_%s_all'%trigger_cat_names[trigger]])

	return MC_dict




