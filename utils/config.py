import argparse
# import time
import configparser



'''

	This script loads default options, updates them from mainConfig.txt and overrides them from anything passed as an --X argument.

	To add an option:

		- Add a default to the parser['DEFAULT'] dictionary.

		- Set up a variable name and point the parser to it: 
			example = str(parser.get("config", "example"))

		- Tell ArgumentParser() to look out for an argument to override the default/mainconfig:
			parser.add_argument('--example', action='store', dest='example', type=int)

		- Update the variable if found:
			if args.example is not None:
				example = args.example
				print('Override example to:', example)
'''

parser = configparser.ConfigParser()
parser['DEFAULT'] = {
'dir': 0, 
'nToys': 1, 
'RK':1.,
'seed':-1,

'batch': False, 

# Plotting
'makePlots':True,
'make_individual_PDF_plots':False,
'plot_all_toys':False,
'saveLocation':'/afs/cern.ch/user/m/marshall/RK/RK_toys/plotdir/',

'window': True, 
'kpipi_cuts': True, 
'casBDT': True, 
'prcBDT': True, 

'prcBDTcut': 0.471875,
'casBDTcut': 0.63, 

'processID':-99, 

# Fit configurations
'years':'combined', 
'triggers':'combined',
'use_trigger_categories':'0,2',

# Weight names
'kinematic_weight':'DataCondKinWeight_MUTOSLong', 
'trigger_weight_0':'trigWeightR_trigCat0', 
'trigger_weight_1':'trigWeightR_trigCat1',
'trigger_weight_2':'trigWeightR_trigCat2', 
'PID_weight':'PIDWeight_DLL4', 

# Boost stats
'boost_stats':1.,
'boost_eTOS':1.,
'boost_TIS':1.,

'truncate_trees':False,
'use_presel_tuples': True,

'fix_signal_yield': False,
'fixed_signal_yield': 800.,

# Mass window
'lower_limit_of_mass_window':4880.,
'extended_lower_limit_of_mass_window':4600.,

# New options for batch jobs
'floatArgus':True,
'usemklcut':False,
'fixcascades':False,
'fit_BFKee':False,
'base_Kee_yield_off_2018_Kuu':True,
'floatarguslambda':False,
'gitlab_CI':False
}

parser.read("mainConfig.txt")



convert_bool = {'false':False,'False':False,'f':False,'F':False,'true':True,'True':True,'t':True,'T':True, True:True, False:False}

foldernum = int(parser.get("mainConfig", "dir"))
nToys = int(parser.get("mainConfig", "nToys"))
cut_window = convert_bool[parser.get("mainConfig", "window")]
cut_kpipi_cuts = convert_bool[parser.get("mainConfig", "kpipi_cuts")]
cut_casBDT = convert_bool[parser.get("mainConfig", "casBDT")]
cut_prcBDT = convert_bool[parser.get("mainConfig", "prcBDT")]
cut_config_prcBDTcut = float(parser.get("mainConfig", "prcBDTcut"))
cut_config_casBDTcut = float(parser.get("mainConfig", "casBDTcut"))
processID = int(parser.get("mainConfig", "processID"))
saveLocation = str(parser.get("mainConfig", "saveLocation"))
batch = convert_bool[parser.get("mainConfig", "batch")]
years = str(parser.get("mainConfig", "years"))
years = years.replace(' ','')
years = [ x for x in years.split(',') ]
triggers = str(parser.get("mainConfig", "triggers"))
triggers = triggers.replace(' ','')
triggers = [ x for x in triggers.split(',') ]
makePlots = convert_bool[parser.get("mainConfig", "makePlots")]
kinematic_weight = str(parser.get("mainConfig", "kinematic_weight"))
trigger_weight_0 = str(parser.get("mainConfig", "trigger_weight_0"))
trigger_weight_1 = str(parser.get("mainConfig", "trigger_weight_1"))
trigger_weight_2 = str(parser.get("mainConfig", "trigger_weight_2"))
PID_weight = str(parser.get("mainConfig", "PID_weight"))
use_trigger_categories = str(parser.get("mainConfig", "use_trigger_categories"))
use_trigger_categories = use_trigger_categories.replace(' ','')
use_trigger_categories = [ int(x) for x in use_trigger_categories.split(',') ] 
boost_stats = float(parser.get("mainConfig", "boost_stats"))
boost_eTOS = float(parser.get("mainConfig", "boost_eTOS"))
boost_TIS = float(parser.get("mainConfig", "boost_TIS"))
plot_all_toys = convert_bool[parser.get("mainConfig", "plot_all_toys")]
truncate_trees = convert_bool[parser.get("mainConfig", "truncate_trees")]
fix_signal_yield = convert_bool[parser.get("mainConfig", "fix_signal_yield")]
fixed_signal_yield = float(parser.get("mainConfig", "fixed_signal_yield"))
use_presel_tuples = convert_bool[parser.get("mainConfig", "use_presel_tuples")]
lower_limit_of_mass_window = float(parser.get("mainConfig", "lower_limit_of_mass_window"))
extended_lower_limit_of_mass_window = float(parser.get("mainConfig", "extended_lower_limit_of_mass_window"))
make_individual_PDF_plots = convert_bool[parser.get("mainConfig", "make_individual_PDF_plots")]
RK = float(parser.get("mainConfig", "RK"))
seed = int(parser.get("mainConfig", "seed"))
floatArgus = convert_bool[parser.get("mainConfig", "floatArgus")]
usemklcut = convert_bool[parser.get("mainConfig", "usemklcut")]
fixcascades = convert_bool[parser.get("mainConfig", "fixcascades")]
fit_BFKee = convert_bool[parser.get("mainConfig", "fit_BFKee")]
base_Kee_yield_off_2018_Kuu = convert_bool[parser.get("mainConfig", "base_Kee_yield_off_2018_Kuu")]
floatarguslambda = convert_bool[parser.get("mainConfig", "floatarguslambda")]
gitlab_CI = convert_bool[parser.get("mainConfig", "gitlab_CI")]


parser = argparse.ArgumentParser()

# arguments for job configuration
parser.add_argument('--dir', action='store', dest='foldernum', type=int)
parser.add_argument('--nToys', action='store', dest='nToys', type=int)
parser.add_argument('--processID', action='store', dest='processID', type=int)
parser.add_argument('--saveLocation', action='store', dest='saveLocation', type=str)
parser.add_argument('--batch', action='store', dest='batch', type=str)
parser.add_argument('--makePlots', action='store', dest='makePlots', type=str)
parser.add_argument('--plot_all_toys', action='store', dest='plot_all_toys', type=str)
parser.add_argument('--make_individual_PDF_plots', action='store', dest='make_individual_PDF_plots', type=str)

# arguments for tuple loading
parser.add_argument('--use_presel_tuples', action='store', dest='use_presel_tuples', type=str)
parser.add_argument('--truncate_trees', action='store', dest='truncate_trees', type=str)

# arguments for signal yields
parser.add_argument('--fix_signal_yield', action='store', dest='fix_signal_yield', type=str)
parser.add_argument('--fixed_signal_yield', action='store', dest='fixed_signal_yield', type=float)
parser.add_argument('--RK', action='store', dest='RK', type=float)
parser.add_argument('--boost_stats', action='store', dest='boost_stats', type=float)
parser.add_argument('--boost_eTOS', action='store', dest='boost_eTOS', type=float)
parser.add_argument('--boost_TIS', action='store', dest='boost_TIS', type=float)

# arguments for configuration of cuts
parser.add_argument('--window', action='store', dest='cut_window', type=str)
parser.add_argument('--kpipi_cuts', action='store', dest='cut_kpipi_cuts', type=str)
parser.add_argument('--casBDT', action='store', dest='cut_casBDT', type=str)
parser.add_argument('--prcBDT', action='store', dest='cut_prcBDT', type=str)
parser.add_argument('--prcBDTcut', action='store', dest='cut_config_prcBDTcut', type=float)
parser.add_argument('--casBDTcut', action='store', dest='cut_config_casBDTcut', type=float)

# arguments for fit configurations to do
parser.add_argument('--years', action='store', dest='years', type=str)
parser.add_argument('--triggers', action='store', dest='triggers', type=str)

# arguments for weight branches
parser.add_argument('--kinematic_weight', action='store', dest='kinematic_weight', type=str)
parser.add_argument('--trigger_weight_0', action='store', dest='trigger_weight_0', type=str)
parser.add_argument('--trigger_weight_1', action='store', dest='trigger_weight_1', type=str)
parser.add_argument('--trigger_weight_2', action='store', dest='trigger_weight_2', type=str)
parser.add_argument('--PID_weight', action='store', dest='PID_weight', type=str)

parser.add_argument('--use_trigger_categories', action='store', dest='use_trigger_categories', type=str)

# arguments for mass windows
parser.add_argument('--lower_limit_of_mass_window', action='store', dest='lower_limit_of_mass_window', type=float)
parser.add_argument('--extended_lower_limit_of_mass_window', action='store', dest='extended_lower_limit_of_mass_window', type=float)

parser.add_argument('--seed', action='store', dest='seed', type=int)

parser.add_argument('--floatArgus', action='store', dest='floatArgus', type=str)
parser.add_argument('--usemklcut', action='store', dest='usemklcut', type=str)
parser.add_argument('--fixcascades', action='store', dest='fixcascades', type=str)
parser.add_argument('--fit_BFKee', action='store', dest='fit_BFKee', type=str)
parser.add_argument('--base_Kee_yield_off_2018_Kuu', action='store', dest='base_Kee_yield_off_2018_Kuu', type=str)
parser.add_argument('--floatarguslambda', action='store', dest='floatarguslambda', type=str)
parser.add_argument('--gitlab_CI', action='store', dest='gitlab_CI', type=str)

args = parser.parse_args()
# args, unknown = parser.parse_known_args()


if args.foldernum is not None:
	foldernum = args.foldernum
	print('Override foldernum to:', foldernum)
if args.nToys is not None:
	nToys = args.nToys
	print('Override nToys to:', nToys)
if args.cut_window is not None:
	cut_window = convert_bool[args.cut_window]
	print('Override cut_window to:', cut_window)
if args.cut_kpipi_cuts is not None:
	cut_kpipi_cuts = convert_bool[args.cut_kpipi_cuts]
	print('Override cut_kpipi_cuts to:', cut_kpipi_cuts)
if args.cut_casBDT is not None:
	cut_casBDT = convert_bool[args.cut_casBDT]
	print('Override cut_casBDT to:', cut_casBDT)
if args.cut_prcBDT is not None:
	cut_prcBDT = convert_bool[args.cut_prcBDT]
	print('Override cut_prcBDT to:', cut_prcBDT)
if args.cut_config_prcBDTcut is not None:
	cut_config_prcBDTcut = args.cut_config_prcBDTcut
	print('Override cut_config_prcBDTcut to:', cut_config_prcBDTcut)
if args.cut_config_casBDTcut is not None:
	cut_config_casBDTcut = args.cut_config_casBDTcut
	print('Override cut_config_casBDTcut to:', cut_config_casBDTcut)
if args.processID is not None:
	processID = args.processID
	print('Override processID to:', processID)
if args.saveLocation is not None:
	saveLocation = args.saveLocation
	print('Override saveLocation to:', saveLocation)
if args.batch is not None:
	batch = convert_bool[args.batch]
	print('Override batch to:', batch)
if args.years is not None:
	years = args.years
	print('Override years to:', years)
	years = years.replace(' ','')
	years = [ x for x in years.split(',') ]
if args.triggers is not None:
	triggers = args.triggers
	print('Override triggers to:', triggers)
	triggers = triggers.replace(' ','')
	triggers = [ x for x in triggers.split(',') ] 
if args.makePlots is not None:
	makePlots = convert_bool[args.makePlots]
	print('Override makePlots to:', makePlots)
if args.kinematic_weight is not None:
	kinematic_weight = args.kinematic_weight
	print('Override kinematic_weight to:', kinematic_weight)
if args.trigger_weight_0 is not None:
	trigger_weight_0 = args.trigger_weight_0
	print('Override trigger_weight_0 to:', trigger_weight_0)
if args.trigger_weight_1 is not None:
	trigger_weight_1 = args.trigger_weight_1
	print('Override trigger_weight_1 to:', trigger_weight_1)
if args.trigger_weight_2 is not None:
	trigger_weight_2 = args.trigger_weight_2
	print('Override trigger_weight_2 to:', trigger_weight_2)
if args.PID_weight is not None:
	PID_weight = args.PID_weight
	print('Override PID_weight to:', PID_weight)
if args.use_trigger_categories is not None:
	use_trigger_categories = args.use_trigger_categories
	print('Override use_trigger_categories to:', use_trigger_categories)
	use_trigger_categories = use_trigger_categories.replace(' ','')
	use_trigger_categories = [ int(x) for x in use_trigger_categories.split(',') ] 
if args.boost_eTOS is not None:
	boost_eTOS = args.boost_eTOS
	print('Override boost_eTOS to:', boost_eTOS)
if args.boost_stats is not None:
	boost_stats = args.boost_stats
	print('Override boost_stats to:', boost_stats)
if args.boost_TIS is not None:
	boost_TIS = args.boost_TIS
	print('Override boost_TIS to:', boost_TIS)
if args.plot_all_toys is not None:
	plot_all_toys = convert_bool[args.plot_all_toys]
	print('Override plot_all_toys to:', plot_all_toys)
if args.truncate_trees is not None:
	truncate_trees = convert_bool[args.truncate_trees]
	print('Override truncate_trees to:', truncate_trees)
if args.fix_signal_yield is not None:
	fix_signal_yield = convert_bool[args.fix_signal_yield]
	print('Override fix_signal_yield to:', fix_signal_yield)
if args.fixed_signal_yield is not None:
	fixed_signal_yield = args.fixed_signal_yield
	print('Override fixed_signal_yield to:', fixed_signal_yield)
if args.use_presel_tuples is not None:
	use_presel_tuples = convert_bool[args.use_presel_tuples]
	print('Override use_presel_tuples to:', use_presel_tuples)
if args.lower_limit_of_mass_window is not None:
	lower_limit_of_mass_window = args.lower_limit_of_mass_window
	print('Override lower_limit_of_mass_window to:', lower_limit_of_mass_window)
if args.extended_lower_limit_of_mass_window is not None:
	extended_lower_limit_of_mass_window = args.extended_lower_limit_of_mass_window
	print('Override extended_lower_limit_of_mass_window to:', extended_lower_limit_of_mass_window)
if args.make_individual_PDF_plots is not None:
	make_individual_PDF_plots = convert_bool[args.make_individual_PDF_plots]
	print('Override make_individual_PDF_plots to:', make_individual_PDF_plots)
if args.RK is not None:
	RK = args.RK
	print('Override RK to:', RK)
if args.seed is not None:
	seed = args.seed
	print('Override seed to:', seed)
if args.floatArgus is not None:
	floatArgus = convert_bool[args.floatArgus]
	print('Override floatArgus to:', floatArgus)
if args.usemklcut is not None:
	usemklcut = convert_bool[args.usemklcut]
	print('Override usemklcut to:', usemklcut)
if args.fixcascades is not None:
	fixcascades = convert_bool[args.fixcascades]
	print('Override fixcascades to:', fixcascades)
if args.fit_BFKee is not None:
	fit_BFKee = convert_bool[args.fit_BFKee]
	print('Override fit_BFKee to:', fit_BFKee)
if args.base_Kee_yield_off_2018_Kuu is not None:
	base_Kee_yield_off_2018_Kuu = convert_bool[args.base_Kee_yield_off_2018_Kuu]
	print('Override base_Kee_yield_off_2018_Kuu to:', base_Kee_yield_off_2018_Kuu)
if args.floatarguslambda is not None:
	floatarguslambda = convert_bool[args.floatarguslambda]
	print('Override floatarguslambda to:', floatarguslambda)
if args.gitlab_CI is not None:
	gitlab_CI = convert_bool[args.gitlab_CI]
	print('Override gitlab_CI to:', gitlab_CI)
	

for year_option in years:
	if year_option not in ['combined','separate']:
		print('Oops, triggers option must be in',['combined','separate'])
		quit()
for trigger_option in triggers:
	if trigger_option not in ['eTOS','combined', 'separate']:
		print('Oops, triggers option must be in',['combined', 'separate'])
		quit()


def configure_custom_cuts():

	# if batch:
	#     cut_casBDT_list = np.linspace(0.1,0.9,25)
	#     mklcut_value_list = np.linspace(1000,1885,25)
	#     processID_idx = -1
	#     for i in range(0,len(cut_casBDT_list)):
	#         for j in range(0,len(mklcut_value_list)):
	#             processID_idx += 1
	#             if processID == processID_idx:
	#                 cut_config_casBDTcut = cut_casBDT_list[i]
	#                 mklcut_value = mklcut_value_list[j]
	#                 break
	#     print(processID, cut_config_casBDTcut, mklcut_value)
	#     custom_cuts = {'window':cut_window, 'kpipi_cuts':cut_kpipi_cuts, 'casBDT':cut_casBDT, 
	#                     'prcBDT':cut_prcBDT, 'casBDTcut':cut_config_casBDTcut, 
	#                     'prcBDTcut':cut_config_prcBDTcut, 'mklcut': True, 'mklcut_value':mklcut_value}
	# else:
	if usemklcut:
		custom_cuts = {'window':cut_window, 'kpipi_cuts':cut_kpipi_cuts, 'casBDT':False, 
						'prcBDT':cut_prcBDT, 'casBDTcut':cut_config_casBDTcut, 
						'prcBDTcut':cut_config_prcBDTcut, 'mklcut': True, 'mklcut_value':1885.}
	else:
		custom_cuts = {'window':cut_window, 'kpipi_cuts':cut_kpipi_cuts, 'casBDT':cut_casBDT, 
						'prcBDT':cut_prcBDT, 'casBDTcut':cut_config_casBDTcut, 
						'prcBDTcut':cut_config_prcBDTcut, 'mklcut': False, 'mklcut_value':0.}

	return custom_cuts

custom_cuts = configure_custom_cuts()


trigger_list = ['eTOS','hTOS','TIS']

kpipi_correction = 'kpipi_correction'
SwaveWeight = 'SwaveWeightLASS'




