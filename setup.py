from setuptools import setup

setup(
   name='RK_highq2_toys',
   version='1.0',
   description='RK toys for high-q2',
   author='Alex Marshall',
   author_email='alex.marshall@cern.ch',
   install_requires=['uproot', 'zfit', 'zfit-physics','pandas','mplhep']
)