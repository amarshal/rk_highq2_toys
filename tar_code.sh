rm packaged_RK_toy.tar.gz

mkdir packaged_RK_toy
cp mainConfig.txt packaged_RK_toy/.
cp RK_toy.py packaged_RK_toy/.
cp -r number_storage packaged_RK_toy/.
mkdir packaged_RK_toy/plotdir
cp -r utils packaged_RK_toy/.

tar -zcvf packaged_RK_toy.tar.gz packaged_RK_toy

rm -r packaged_RK_toy/